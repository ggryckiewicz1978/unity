// This file is Shared between C++ and openCL code.

#ifndef __UNITY_CPP_AND_OPENCL_SHARED_HEADER_H__
#define __UNITY_CPP_AND_OPENCL_SHARED_HEADER_H__

// IMPORTANT --------------------------------------------------------------------------
// Bump the version below when changing OpenCL include files.
// Use current date in "YYYYMMDD0" (last digit normally zero, bump it up if you want to
// change the version more than once per day) format for the version.
// If conflicts while merging, just enter current date + '0' as the version.
//
// Details:
// The Nvidia OpenCL driver is caching compiled kernels on the harddisc, unfortunately
// it does not take include files into account. However it does hash macro sent to the
// preprocessor. So we do that in LoadOpenCLProgramInternal() to force a recompilation
// when the version number below changes.
// see https://stackoverflow.com/questions/31338520/opencl-clbuildprogram-caches-source-and-does-not-recompile-if-included-source
#define UNITY_KERNEL_INCLUDES_VERSION "202004030"

#if defined(UNITY_EDITOR)
#include "Runtime/Math/Color.h"
#include "Runtime/Math/Vector2.h"
#include "Runtime/Math/Vector4.h"
#define SHARED_float3(name) float name[3]
#define SHARED_float4(name) float name[4]
#define SHARED_float2_type Vector2f
#define SHARED_float3_type Vector3f
#define SHARED_float4_type Vector4f
#define SHARED_INLINE inline
#define SHARED_CONST const
#define SHARED_Max std::max
#define SHARED_Normalize Normalize
#define INDEX_SAFE(buffer, index) buffer[index]
#else
#define SHARED_float3(name) float3 name
#define SHARED_float4(name) float4 name
#define SHARED_float2_type float2
#define SHARED_float3_type float3
#define SHARED_float4_type float4
#define SHARED_INLINE
#define SHARED_CONST __constant
#define SHARED_Max max
#define SHARED_Normalize normalize
#endif // UNITY_EDITOR

#ifndef APPLE // These functions are defined on OSX already

SHARED_INLINE SHARED_float2_type make_float2(float x, float y)
{
    SHARED_float2_type res;
    res.x = x;
    res.y = y;
    return res;
}

SHARED_INLINE SHARED_float3_type make_float3(float x, float y, float z)
{
    SHARED_float3_type res;
    res.x = x;
    res.y = y;
    res.z = z;
    return res;
}

SHARED_INLINE SHARED_float4_type make_float4(float x, float y, float z, float w)
{
    SHARED_float4_type res;
    res.x = x;
    res.y = y;
    res.z = z;
    res.w = w;
    return res;
}

#endif  // #ifndef(APPLE)

//Maximum lightmap size is 4096 thus 16777216 texels, thus 65536 thread group for the convergence kernel.
#define CONVERGENCE_WORKGROUPSIZE 256
//Each thread group from the convergence kernel output a ratio of current sample against total sample. This ratio is expressed with
//the following precision. The precision need to be inferior to MAX_INT_32 / maxNumThreadGroup (ie 65536) to avoid overflow.
//This should go away when we will implement tiled lightmap baking (for memory reduction purposes).
#define CONVERGENCE_AVERAGES_PRECISION 65000.0f

#define INTERSECT_BVH_WORKGROUPSIZE 64
#define INTERSECT_BVH_MAXSTACKSIZE 48

#define LIGHTMAPPER_FALLOFF_TEXTURE_WIDTH (1024)

#ifndef PVR_MAX_ENVSAMPLES
#define PVR_MAX_ENVSAMPLES (16384) // from External\Wintermute\RLSL\Defines.rlsl
#endif // PVR_MAX_ENVSAMPLES

#define PLM_MAX_DIR_LIGHTS 8
#define PLM_MAX_NUM_SOBOL_DIMENSIONS_PER_BOUNCE 3
#define PLM_MAX_BOUNCE_FOR_CRANLEY_PATTERSON_ROTATION 4
#define PLM_MAX_NUM_CRANLEY_PATTERSON_ROTATION (PLM_MAX_NUM_SOBOL_DIMENSIONS_PER_BOUNCE * PLM_MAX_BOUNCE_FOR_CRANLEY_PATTERSON_ROTATION)
//Max size of the golden samples (for cranley patterson rotation) map
//this is a square map with PLM_MAX_NUM_CRANLEY_PATTERSON_ROTATION in each texel
#define PLM_MAX_SIZE_GOLDEN_SAMPLES_MAP 512

typedef enum OpenCLKernelAssertReason
{
    kOpenCLKernelAssertReason_None = 0,
    kOpenCLKernelAssertReason_BufferAccessedOutOfBound,
    kOpenCLKernelAssertReason_AssertionFailed,
    kOpenCLKernelAssertReason_Count
} OpenCLKernelAssertReason;

// If you modify this, also update GetBufferNameFromRadeonRaysBufferID in OpenCLHelpers.cpp
typedef enum radeonRaysBufferID
{
    // From OpenCLRenderBuffer
    kRRBuf_invalid = 0,
    kRRBuf_lightSamplesCompactedBuffer,
    kRRBuf_outputShadowmaskFromDirectBuffer,
    kRRBuf_shadowmaskExpandedBuffer,
    kRRBuf_pathRaysCompactedBuffer_0,
    kRRBuf_pathRaysCompactedBuffer_1,
    kRRBuf_pathIntersectionsCompactedBuffer,
    kRRBuf_transparentPathRayIndicesCompactedBuffer,
    kRRBuf_transparentPathRayIndicesCompactedCountBuffer,
    kRRBuf_pathThroughputExpandedBuffer,
    kRRBuf_pathLastPlaneNormalCompactedBuffer,
    kRRBuf_pathLastInterpNormalCompactedBuffer,
    kRRBuf_pathLastNormalFacingTheRayCompactedBuffer,
    kRRBuf_directSampleCountBuffer,
    kRRBuf_indirectSampleCountBuffer,
    kRRBuf_environmentSampleCountBuffer,
    kRRBuf_activePathCountBuffer_0,
    kRRBuf_activePathCountBuffer_1,
    kRRBuf_lightRayIndexToPathRayIndexCompactedBuffer,
    kRRBuf_totalRaysCastCountBuffer,
    kRRBuf_lightRaysCountBuffer,
    kRRBuf_sampleDescriptionsExpandedCountBuffer,
    kRRBuf_expandedTexelsBuffer,
    kRRBuf_expandedTexelsCountBuffer,
    kRRBuf_sampleDescriptionsExpandedBuffer,
    kRRBuf_lightRaysCompactedBuffer,
    kRRBuf_lightOcclusionCompactedBuffer,
    kRRBuf_positionsWSBuffer,
    kRRBuf_kernelDebugHelperExpandedBuffer,
    kRRBuf_kernelAssertHelperBuffer,
    kRRBuf_bufferIDToBufferSizeBuffer,
    kRRBuf_originalRaysExpandedBuffer,
    // From OpenCLRenderLightmapBuffers
    kRRBuf_outputDirectLightingBuffer,
    kRRBuf_outputIndirectLightingBuffer,
    kRRBuf_outputEnvironmentLightingBuffer,
    kRRBuf_lightingExpandedBuffer,
    kRRBuf_outputDirectionalFromDirectBuffer,
    kRRBuf_outputDirectionalFromGiBuffer,
    kRRBuf_directionalExpandedBuffer,
    kRRBuf_outputAoBuffer,
    kRRBuf_outputValidityBuffer,
    kRRBuf_cullingMapBuffer,
    kRRBuf_visibleTexelCountBuffer,
    kRRBuf_convergenceOutputDataBuffer,
    kRRBuf_interpNormalsWSBuffer,
    kRRBuf_planeNormalsWSBuffer,
    kRRBuf_chartIndexBuffer,
    kRRBuf_occupancyBuffer,
    // From OpenCLRenderLightProbeBuffers
    kRRBuf_outputProbeDirectSHDataBuffer,
    kRRBuf_outputProbeIndirectSHDataBuffer,
    kRRBuf_outputProbeOcclusionBuffer,
    kRRBuf_outputProbeValidityBuffer,
    kRRBuf_outputProbeDepthOctahedronBuffer,
    kRRBuf_inputLightIndicesBuffer,
    kRRBuf_probeSHExpandedBuffer,
    kRRBuf_probeDepthOctahedronExpandedBuffer,
    kRRBuf_probeOcclusionExpandedBuffer,

    // From OpenCLCommonBuffer
    kRRBuf_bvhStackBuffer,
    kRRBuf_goldenSample_buffer,
    kRRBuf_sobol_buffer,
    kRRBuf_distanceFalloffs_buffer,
    kRRBuf_angularFalloffLUT_buffer,
    kRRBuf_albedoTextures_buffer, // Albedo is stored in gamma space.
    kRRBuf_emissiveTextures_buffer,
    kRRBuf_transmissionTextures_buffer,
    kRRBuf_gbufferInstanceIdToReceiveShadowsBuffer,
    kRRBuf_lightCookiesBuffer,
    kRRBuf_instanceIdToLodInfoBuffer,

    // From RadeonRaysLightGrid
    kRRBuf_directLightsOffsetBuffer,  // Grid cell offset into index buffer.
    kRRBuf_directLightsIndexBuffer,   // Light index (direct)
    kRRBuf_directLightsBuffer,        // Light structs (direct)
    kRRBuf_directLightsCountPerCellBuffer,
    kRRBuf_indirectLightsOffsetBuffer,// Grid cell offset into index buffer.
    kRRBuf_indirectLightsIndexBuffer, // Light index (indirect)
    kRRBuf_indirectLightsBuffer,      // Light structs (indirect)
    kRRBuf_indirectLightDistributionOffsetBuffer,
    kRRBuf_indirectLightsDistributionBuffer,
    kRRBuf_usePowerSamplingBuffer,

    // From RadeonRaysMeshManager
    kRRBuf_instanceIdToAlbedoTextureProperties,
    kRRBuf_instanceIdToEmissiveTextureProperties,
    kRRBuf_instanceIdToTransmissionTextureProperties,
    kRRBuf_instanceIdToTransmissionTextureSTs,
    kRRBuf_geometryUV0sBuffer,
    kRRBuf_geometryUV1sBuffer,
    kRRBuf_geometryPositionsBuffer,
    kRRBuf_geometryNormalsBuffer,
    kRRBuf_geometryIndicesBuffer,
    kRRBuf_instanceIdToMeshDataOffsets,
    kRRBuf_instanceIdToInvTransposedMatrices,

    // From OpenCLEnvironmentBuffers
    kRRBuf_env_mipped_cube_texels_buffer,
    kRRBuf_env_mip_offsets_buffer,
    kRRBuf_envDirectionsBuffer,

    // Dynamic argument buffers, e.g. an argument that alternates between two buffers declared above
    kRRBuf_dynarg_begin,
    kRRBuf_dynarg_countBuffer,
    kRRBuf_dynarg_directionalityBuffer,     // shared between kRRBuf_outputDirectionalFromDirectBuffer and kRRBuf_outputDirectionalFromGiBuffer
    kRRBuf_dynarg_filterWeights,
    kRRBuf_w_dynarg_dstImage,
    kRRBuf_h_dynarg_dstImage,
    kRRBuf_w_dynarg_srcImage,
    kRRBuf_h_dynarg_srcImage,
    kRRBuf_w_dynarg_dstTile,
    kRRBuf_h_dynarg_dstTile,
    kRRBuf_w_dynarg_dstTile0,
    kRRBuf_h_dynarg_dstTile0,
    kRRBuf_w_dynarg_dstTile1,
    kRRBuf_h_dynarg_dstTile1,
    kRRBuf_w_dynarg_srcTile,
    kRRBuf_h_dynarg_srcTile,
    kRRBuf_w_dynarg_srcTile0,
    kRRBuf_h_dynarg_srcTile0,
    kRRBuf_w_dynarg_srcTile1,
    kRRBuf_h_dynarg_srcTile1,
    kRRBuf_w_dynarg_srcTile2,
    kRRBuf_h_dynarg_srcTile2,
    kRRBuf_w_dynarg_srcTile3,
    kRRBuf_h_dynarg_srcTile3,
    kRRBuf_dynarg_texture_buffer,

    // Denoising buffers
    kRRBuf_denoisedDirect,
    kRRBuf_denoisedShadowmask,
    kRRBuf_denoisedIndirect,
    kRRBuf_denoisedEnvironment,
    kRRBuf_denoisedAO,
    kRRBuf_denoisedTemporary,

    // Count
    kRRBuf_Count
} RadeonRaysBufferID;

typedef enum radeonRaysExpansionPass
{
    // From OpenCLRenderBuffer
    kRRExpansionPass_direct = 0,
    kRRExpansionPass_environment,
    kRRExpansionPass_indirect
}  RadeonRaysExpansionPass;

typedef enum angularFalloffType
{
    kAngularFalloffTypeLUT = 0,
    kAngularFalloffTypeAnalyticAndInnerAngle = 1
} AngularFalloffType;

typedef enum lightmappingSourceType
{
    kLightmappingSourceType_Lightmap = 0,
    kLightmappingSourceType_Probe,
    kLightmappingSourceType_CustomBake
} LightmappingSourceType;

typedef struct openCLKernelAssert
{
    int assertionValue;
    int lineNumber;
    int index;
    int bufferSize;
    RadeonRaysBufferID bufferID;
    int dibs;
    int threadId;
    int threadGroupSize;
} OpenCLKernelAssert;

typedef struct environment
{
    int         envDim;
    int         numMips;
    float       sMipOffs;
    int         lightmapWidth;
    int         numRaysIndirect;    // number of environment rays shot for indirect samples
    int         numSamples;         // number of entries in the precomputed MIS buffers
    float       envmapIntegral;     // integral of the current envmap when using MIS
    int         flags;              // various MIS related flags
} Environment;

typedef struct cookieHeader
{
    int width;
    int height;
    int offset;
} CookieHeader;

typedef struct areaLightData
{
    float areaHeight;
    float areaWidth;
    int   cookieIndex;
    float pad0;
    SHARED_float4(Normal);
    SHARED_float4(Tangent);
    SHARED_float4(Bitangent);
} AreaLightData;

typedef struct discLightData
{
    float radius;
    int   cookieIndex;
    float pad1;
    float pad2;
    SHARED_float4(Normal);
    SHARED_float4(Tangent);
} DiscLightData;

typedef struct spotLightData
{
    int LightFalloffIndex;
    int cookieIndex;
    float tanX;
    float tanY;
    float tanZ;
    float cosineConeAngle;
    float inverseCosineConeAngle;
    float cotanConeAngle;
    float cosineInnerAngle;
    float cosineOuterFrustumAngle;
    AngularFalloffType angularFalloffType;
} SpotLightData;

typedef struct pyramidLightData
{
    SHARED_float4(scaledTangent);
    SHARED_float4(scaledBitangent); // .w contains the falloffIdx
    int cookieIndex;
} PyramidLightData;

typedef struct boxLightData
{
    SHARED_float4(scaledTangent);
    SHARED_float4(scaledBitangent);
    int cookieIndex;
} BoxLightData;

typedef struct pointLightData
{
    float tanX;
    float tanY;
    float tanZ;
    float bitanX;
    float bitanY;
    float bitanZ;
    int   LightFalloffIndex;
    int   cookieIndex;
} PointLightData;

typedef struct directionalLightData
{
    int   cookieIndex;
    float cookieSizeXRcp;
    float cookieSizeYRcp;
    float tanX;
    float tanY;
    float tanZ;
    float pad0;
    float pad1;
} DirectionalLightData;

typedef union
{
    DirectionalLightData dirLightData;
    AreaLightData areaLightData;
    SpotLightData spotLightData;
    PointLightData pointLightData;
    DiscLightData discLightData;
    PyramidLightData pyramidLightData;
    BoxLightData     boxLightData;
} LightDataUnion;

typedef struct lightSample
{
    int lightIdx;
    float lightPdf;
} LightSample;

typedef struct expandedTexelInfo
{
    int firstRaysOffset;
    int numRays;
    int originalTexelIndex;
} ExpandedTexelInfo;

typedef struct sampleDescription
{
    int texelIndex;
    int currentSampleCount;
} SampleDescription;

typedef enum DirectBakeMode
{
    kDirectBakeMode_None = -3,
    kDirectBakeMode_Shaded = -2,
    kDirectBakeMode_Subtractive = -1,
    kDirectBakeMode_OcclusionChannel0 = 0,
    kDirectBakeMode_OcclusionChannel1 = 1,
    kDirectBakeMode_OcclusionChannel2 = 2,
    kDirectBakeMode_OcclusionChannel3 = 3
} DirectBakeMode;

typedef struct LightBuffer
{
#if defined(UNITY_EDITOR)
    LightBuffer()
    {
        memset(this, 0, sizeof(LightBuffer));
    }

    void SetPositionAndShadowAngle(float x, float y, float z, float shadowAngle)
    {
        pos[0] = x;
        pos[1] = y;
        pos[2] = z;
        pos[3] = shadowAngle;
    }

    void SetColor(float r, float g, float b, float a)
    {
        col[0] = r;
        col[1] = g;
        col[2] = b;
        col[3] = a;
    }

    void SetDirection(float x, float y, float z, float w)
    {
        dir[0] = x;
        dir[1] = y;
        dir[2] = z;
        dir[3] = w;
    }

#endif

    SHARED_float4(pos); // .rgb is position, .w is shadow angle
    SHARED_float4(col); // .rgb is color, .w is intensity
    SHARED_float4(dir); // .xyz is direction, .w is range

    int lightType;
    DirectBakeMode directBakeMode;
    int probeOcclusionLightIndex;
    int castShadow;

    LightDataUnion dataUnion;
} LightBuffer;

typedef struct meshDataOffsets
{
    int vertexOffset;
    int indexOffset;
} MeshDataOffsets;

typedef struct materialTextureProperties
{
    int textureOffset;
    int textureDimensions;
    int lodInfo;
    int materialProperties;
} MaterialTextureProperties;

typedef enum materialInstanceProperties
{
    kMaterialInstanceProperties_UseTransmission = 0,
    kMaterialInstanceProperties_WrapModeU_Clamp = 1,//Repeat is the default
    kMaterialInstanceProperties_WrapModeV_Clamp = 2,//Repeat is the default
    kMaterialInstanceProperties_FilerMode_Point = 3,//Linear is the default
    kMaterialInstanceProperties_CastShadows = 4,
    kMaterialInstanceProperties_DoubleSidedGI = 5,
    kMaterialInstanceProperties_OddNegativeScale = 6
} MaterialInstanceProperties;

SHARED_INLINE bool GetMaterialProperty(MaterialTextureProperties matTextureProperties, MaterialInstanceProperties selectedProperty)
{
    int materialProperties = matTextureProperties.materialProperties;
    return (bool)(materialProperties & 1 << selectedProperty);
}

SHARED_INLINE int GetMaterialPropertyTextureWidth(MaterialTextureProperties matTextureProperties)
{
    return matTextureProperties.textureDimensions & 0xffff;
}

SHARED_INLINE int GetMaterialPropertyTextureHeight(MaterialTextureProperties matTextureProperties)
{
    return (matTextureProperties.textureDimensions & 0xffff0000) >> 16;
}

SHARED_INLINE void BuildMaterialProperties(MaterialTextureProperties* matTextureProperties, MaterialInstanceProperties selectedProperty, bool value)
{
    int* materialProperties = &(matTextureProperties->materialProperties);
    int mask = 1 << selectedProperty;

    //clear the corresponding MaterialInstanceProperties slot
    (*materialProperties) &= ~(mask);

    //set it to 1 if value is true
    if (value)
    {
        (*materialProperties) |= mask;
    }
}

SHARED_INLINE void SetMaterialPropertyTextureDimension(MaterialTextureProperties *matTextureProperties, int width, int height)
{
#if defined(UNITY_EDITOR)
    const int kMaxTextureSize = 1 << 16;
    DebugAssert(width < kMaxTextureSize);
    DebugAssert(height < kMaxTextureSize);
#endif
    matTextureProperties->textureDimensions = (height << 16) | width;
}

SHARED_CONST unsigned int kMultiplyDeBruijnBitPosition[32] =
{
    // precomputed lookup table
    0,  1, 28,  2, 29, 14, 24,  3, 30, 22, 20, 15, 25, 17,  4,  8,
    31, 27, 13, 23, 21, 19, 16,  7, 26, 12, 18,  6, 11,  5, 10,  9
};

SHARED_INLINE unsigned int lowestBitSet(unsigned int x)
{
    // only lowest bit will remain 1, all others become 0
    x &= -((int)x);
    // DeBruijn constant
    x *= 0x077CB531;
    // the upper 5 bits are unique, skip the rest (32 - 5 = 27)
    x >>= 27;
    // convert to actual position
    return kMultiplyDeBruijnBitPosition[x];
}

SHARED_INLINE int ConvertLODMaskToRayLODLevel(int rayLodMask)
{
    return 1 << lowestBitSet(rayLodMask);
}

SHARED_INLINE int UnpackLODMask(int packedLodInfo)
{
    return (packedLodInfo & 0xff000000) >> 24;
}

SHARED_INLINE int UnpackLODGroup(int packedLodInfo)
{
    return (packedLodInfo & 0xffffff);
}

SHARED_INLINE int PackLODInfo(int lodMask, int lodGroup)
{
#if defined(UNITY_EDITOR)
    DebugAssert(lodMask < 256);
    DebugAssert(lodGroup < (1 << 24));
#endif
    return ((lodMask) << 24) | lodGroup;
}

#define LOD0_LEVEL 1 << 0
#define NO_LOD_GROUP 0x00ffffff // -1 represented in 24 bits.
#define NO_LOD_MASK  0x000000ff // -1 represented in  8 bits.

SHARED_INLINE bool IsInstanceHit(int instanceLODMask, int instanceLODGroup, int rayLODLevel, int rayLODGroup)
{
    // Ray originating from another LOD group should intersect LOD0 from a LOD group.
    const bool rayFromOtherLODGroupHitsLOD0 = (rayLODGroup == NO_LOD_GROUP || rayLODGroup != instanceLODGroup) && (instanceLODMask & LOD0_LEVEL);
    if (rayFromOtherLODGroupHitsLOD0)
        return true;

    // Ray originating from the same LODGroup, should only intersect instances appearing on the same level.
    const bool rayFromSameLODGroupHitsSameLODLevel = (rayLODGroup == instanceLODGroup) && (instanceLODMask & rayLODLevel);
    if (rayFromSameLODGroupHitsSameLODLevel)
        return true;

    return false;
}

SHARED_INLINE int GetTextureFetchOffset(const MaterialTextureProperties matProperty, int x, int y, bool gBufferFiltering)
{
    const int textureWidth  = GetMaterialPropertyTextureWidth(matProperty);
    const int textureHeight = GetMaterialPropertyTextureHeight(matProperty);

    //Clamp
    const int clampedWidth = clamp(x, 0, textureWidth - 1);
    const int clampedHeight = clamp(y, 0, textureHeight - 1);

    //Repeat
    int repeatedWidth = x % textureWidth;
    int repeatedHeight = y % textureHeight;
    repeatedWidth = (repeatedWidth >= 0) ? repeatedWidth : textureWidth + repeatedWidth;
    repeatedHeight = (repeatedHeight >= 0) ? repeatedHeight : textureHeight + repeatedHeight;

    //Select based on material properties
    const int usedWidth = (gBufferFiltering || GetMaterialProperty(matProperty, kMaterialInstanceProperties_WrapModeU_Clamp)) ? clampedWidth : repeatedWidth;
    const int usedHeight = (gBufferFiltering || GetMaterialProperty(matProperty, kMaterialInstanceProperties_WrapModeV_Clamp)) ? clampedHeight : repeatedHeight;
    const int fetchOffset = matProperty.textureOffset + (textureWidth * usedHeight) + usedWidth;
    return fetchOffset;
}

// Returns ±1
SHARED_INLINE SHARED_float2_type signNotZero(SHARED_float2_type v)
{
    return make_float2((v.x >= 0.0f) ? +1.0f : -1.0f, (v.y >= 0.0f) ? +1.0f : -1.0f);
}

// Assume normalized input. Output is on [-1, 1] for each component.
// The representation maps the octants of a sphere to the faces of an octahedron,
// which it then projects to the plane and unfolds into a unit square.
// Ref: A Survey of Efﬁcient Representations for Independent Unit Vectors
// http://jcgt.org/published/0003/02/01/paper.pdf
SHARED_INLINE SHARED_float2_type PackNormalOctQuadEncoded(const SHARED_float3_type v)
{
#if defined(UNITY_EDITOR)
    DebugAssert(IsNormalized(v));
#endif

    // Project the sphere onto the octahedron, and then onto the xy plane
    const SHARED_float2_type p = make_float2(v.x, v.y) * (1.0f / (fabs(v.x) + fabs(v.y) + fabs(v.z)));
    const SHARED_float2_type one = make_float2(1.f, 1.f);
    const SHARED_float2_type pyx = make_float2(fabs(p.y), fabs(p.x));

    // Reflect the folds of the lower hemisphere over the diagonals
    return (v.z < 0.0f) ? ((one - pyx) * signNotZero(p)) : p;
}

SHARED_INLINE SHARED_float3_type UnpackNormalOctQuadEncoded(const SHARED_float2_type e)
{
    SHARED_float3_type v = make_float3(e.x, e.y, 1.0f - fabs(e.x) - fabs(e.y));
    if (v.z < 0.f)
    {
        const SHARED_float2_type vyxAbs = make_float2(fabs(v.y), fabs(v.x));
        SHARED_float2_type vxy = make_float2(v.x, v.y);
        const SHARED_float2_type one = make_float2(1.f, 1.f);
        vxy = (one - vyxAbs) * signNotZero(vxy);
        v = make_float3(vxy.x, vxy.y, v.z);
    }
    return SHARED_Normalize(v);
}

SHARED_INLINE int GetOctQuadEncodedTexelFromPackedNormal(const SHARED_float2_type normalizedOctCoord, const int octWidth)
{
    SHARED_float2_type normalizedOctCoordZeroOne = (normalizedOctCoord + make_float2(1.0f, 1.0f)) * 0.5f;
    SHARED_float2_type octCoord = normalizedOctCoordZeroOne * octWidth;
    return (int)(octCoord.y * octWidth) + (int)(octCoord.x);
}

static float saturatev1(const float x)
{
    return x > 1.0f ? 1.0f : (x < 0.0f ? 0.0f : x);
}

static SHARED_float2_type saturate(const SHARED_float2_type in)
{
    return make_float2(saturatev1(in.x), saturatev1(in.y));
}

typedef struct packedNormalOctQuad
{
    // Warning: Using two ushorts instead of a uint here causes weird behavior on GPUs.
    unsigned int x;
#if defined(UNITY_EDITOR)
    void SetZero() { x = 0; }
#endif
} PackedNormalOctQuad;

// Pack float2 (each component quantized to 16 bits) into a uint.
// The input floats are quantized in this function.
SHARED_INLINE PackedNormalOctQuad PackFloat2ToUint(const SHARED_float2_type f)
{
    SHARED_float2_type scaled = (f * 65535.5f);  // 2^16 range minus a half.
    unsigned int ix = (unsigned int)scaled.x;
    unsigned int iy = (unsigned int)scaled.y;
    PackedNormalOctQuad out;
    out.x = (ix << 16) | iy;
    return out;
}

// Unpack 2 float of 16bit packed into a 32 bit uint
SHARED_INLINE SHARED_float2_type UnpackUintToFloat2(const PackedNormalOctQuad i)
{
    SHARED_float2_type cb;
    cb.x = (i.x >> 16) & 0xffff;
    cb.y = i.x & 0xffff;

    return cb / 65535.0f;
}

SHARED_INLINE PackedNormalOctQuad EncodeNormalToUint(const SHARED_float3_type normalIn)
{
    const SHARED_float2_type v2half = make_float2(0.5f, 0.5f);
    const SHARED_float2_type octNormal = PackNormalOctQuadEncoded(normalIn);
    return PackFloat2ToUint(saturate(octNormal * v2half + v2half));
}

SHARED_INLINE SHARED_float3_type DecodeNormal(const PackedNormalOctQuad packedNormal)
{
    const SHARED_float2_type octNormalUnpacked = UnpackUintToFloat2(packedNormal);
    const SHARED_float2_type two = make_float2(2.f, 2.f);
    const SHARED_float2_type one = make_float2(1.f, 1.f);
    SHARED_float3_type res = UnpackNormalOctQuadEncoded(octNormalUnpacked * two - one);
    // Fixup to make sure signs don't flip at singularities. Crucial for having consistent
    // z coordinates between GPU/CPU lightmappers as the sign is used to build
    // orthonormal coordinate frames.
    const float kEpsilon = 1.0f / 8192.0f;
    res.x = fabs(res.x) > kEpsilon ? res.x : 0.0f;
    res.y = fabs(res.y) > kEpsilon ? res.y : 0.0f;
    res.z = fabs(res.z) > kEpsilon ? res.z : 0.0f;
    return res;
}

// From https://github.com/Unity-Technologies/ScriptableRenderPipeline/blob/master/com.unity.render-pipelines.core/ShaderLibrary/Packing.hlsl
#if defined(UNITY_EDITOR)
SHARED_INLINE Vector4f Unpack8888ToFloat4(const ColorRGBA32 packed)
{
    const float x = (float)packed.r;
    const float y = (float)packed.g;
    const float z = (float)packed.b;
    const float w = (float)packed.a;
#else
float4 Unpack8888ToFloat4(const uchar4 packed)
{
    const float x = (float)packed.x;
    const float y = (float)packed.y;
    const float z = (float)packed.z;
    const float w = (float)packed.w;
#endif
    const float oneOver255 = 1.f / 255.f;
    const float fx = x * oneOver255;
    const float fy = y * oneOver255;
    const float fz = z * oneOver255;
    const float fw = w * oneOver255;
    return make_float4(fx, fy, fz, fw);
}

typedef struct convergenceOutputData
{
    unsigned int visibleConvergedDirectTexelCount;
    unsigned int visibleConvergedGITexelCount;
    unsigned int visibleConvergedEnvTexelCount;
    unsigned int visibleTexelCount;
    unsigned int convergedDirectTexelCount;
    unsigned int convergedGITexelCount;
    unsigned int convergedEnvTexelCount;
    unsigned int averageDirectSamplesPercent;
    unsigned int averageGISamplesPercent;
    unsigned int averageEnvSamplesPercent;
    int          minDirectSamples;
    int          minGISamples;
    int          minEnvSamples;
    int          maxDirectSamples;
    int          maxGISamples;
    int          maxEnvSamples;
} ConvergenceOutputData;

enum UsePowerSamplingBufferSlot
{
    UsePowerSamplingBufferSlot_PowerSampleEnabled = 0,
    UsePowerSamplingBufferSlot_LightHitCount = 1,
    UsePowerSamplingBufferSlot_LightRayCount = 2,
    UsePowerSamplingBufferSlot_BufferSize = 3
};

enum PrimaryBufferMode
{
    PrimaryBufferMode_DontGenerate = 0,
    PrimaryBufferMode_Generate = 1,
};

#if defined(UNITY_EDITOR)
#undef SHARED_float3
#undef SHARED_float4
#undef SHARED_float2_type
#undef SHARED_float3_type
#undef SHARED_float4_type
#undef SHARED_INLINE
#undef SHARED_Max
#undef SHARED_Normalize
#undef INDEX_SAFE
#endif // UNITY_EDITOR

typedef enum PVRLightType
{
    kPVRLightSpot,//A spot light with a circle shaped base
    kPVRLightDirectional,
    kPVRLightPoint,
    kPVRLightRectangle,
    kPVRLightDisc,
    // lightmapper exclusive types
    kPVRLightPyramid,
    kPVRLightBox,
    kPVRLightTypeLast = kPVRLightBox // keep this last
} PVRLightType;


#endif // __UNITY_CPP_AND_OPENCL_SHARED_HEADER_H__
