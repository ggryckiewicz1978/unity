#include "UnityPrefix.h"

#if ENABLE_UNIT_TESTS

#include "CPPsharedCLincludes.h"
#include "textureFetch.h"

#include "Runtime/Testing/Checks.h"
#include "Runtime/Testing/Testing.h"
#include "Runtime/Utilities/BitUtility.h"

UNIT_TEST_SUITE(CPPSharedIncludes)
{
    TEST(CPPSharedIncludes_PackAndUnpackFloat4ToRGBA8888Match)
    {
        // Emulate the packing that happens in OpenCLCommonBuffers::PrepareAlbedoAndEmissiveBuffers
        // and the unpacking that happens in FetchAlbedoTextureFromMaterialAndUVs.
        const ColorRGBAf albedoIn = ColorRGBAf(0.f, 0.25f, 0.5f, 1.0f);
        const ColorRGBA32 albedoPacked = albedoIn;
        const Vector4f unpacked = Unpack8888ToFloat4(albedoPacked);
        const float tolerance = 1.f / 256.f;
        CHECK_CLOSE_VECTOR4(albedoIn.GetVector4f(), unpacked, tolerance);
    }

    TEST(CPPSharedIncludes_PackFloat2ToUintAndUnpack)
    {
        const int kCount = 12;
        const Vector2f floats[kCount] =
        {
            Vector2f(1.f, 0.f),
            Vector2f(0.f, 1.f),
            Vector2f(0.f, 0.f),
            Vector2f(-1.f, 0.f),
            Vector2f(0.f, -1.f),
            Vector2f(0.f, 0.f),
            Vector2f(0.340799f,  0.647518f),
            Vector2f(0.340799f, -0.647518f),
            Vector2f(0.340799f,  0.647518f),
            Vector2f(-0.340799f, -0.647518f),
            Vector2f(-0.340799f, 0.647518f),
            Vector2f(-0.340799f, -0.647518f)
        };
        const float tolerance = 1.f / 256.f;
        const Vector2f half = make_float2(0.5f, 0.5f);
        const Vector2f two = make_float2(2.f, 2.f);
        const Vector2f one = make_float2(1.f, 1.f);
        for (int i = 0; i < kCount; ++i)
        {
            const Vector2f input = floats[i];
            const PackedNormalOctQuad packed = PackFloat2ToUint(input * half + half);
            const Vector2f unpacked = UnpackUintToFloat2(packed) * two - one;
            CHECK_CLOSE_VECTOR2(input, unpacked, tolerance);
        }
    }

    TEST(CPPSharedIncludes_PackNormalsToUintAndUnpack)
    {
        const int kNormalCount = 12;
        const Vector3f normals[kNormalCount] =
        {
            Vector3f(1.f, 0.f, 0.f),
            Vector3f(0.f, 1.f, 0.f),
            Vector3f(0.f, 0.f, 1.f),
            Vector3f(-1.f, 0.f, 0.f),
            Vector3f(0.f, -1.f, 0.f),
            Vector3f(0.f, 0.f, -1.f),
            Vector3f(0.340799f,  0.647518f,  0.681598f),
            Vector3f(0.340799f, -0.647518f,  0.681598f),
            Vector3f(0.340799f,  0.647518f, -0.681598f),
            Vector3f(-0.340799f, -0.647518f,  0.681598f),
            Vector3f(-0.340799f, 0.647518f, -0.681598f),
            Vector3f(-0.340799f, -0.647518f, -0.681598f),
        };

        const float tolerance = 1.f / 8192.f;
        for (int i = 0; i < kNormalCount; ++i)
        {
            const Vector3f& input = normals[i];
            const PackedNormalOctQuad packedNormal = EncodeNormalToUint(input);
            const Vector3f unpackedNormal = DecodeNormal(packedNormal);
            CHECK_CLOSE_VECTOR3(input, unpackedNormal, tolerance);
            // Additionally check whether encoding flips the sign of the coordinate.
            // This is especially crucial for z, as the sign is used to build
            // orthonormal coordinate frames in the lightmappers.
            for (int j = 0; j < 3; ++j)
            {
                float signIn = input[j] >= 0.f ? 1.f : -1.f;
                float signOut = unpackedNormal[j] >= 0.f ? 1.f : -1.f;
                CHECK_EQUAL(signIn, signOut);
            }
        }
    }

    TEST(CPPSharedIncludes_BuildMaterialProperties_GetterAndSetterMatch)
    {
        MaterialTextureProperties matProp;

        for (int i = kMaterialInstanceProperties_UseTransmission; i <= kMaterialInstanceProperties_OddNegativeScale; ++i)
        {
            matProp.materialProperties = 0;

            BuildMaterialProperties(&matProp, (MaterialInstanceProperties)i, true);
            CHECK_EQUAL(true, GetMaterialProperty(matProp, (MaterialInstanceProperties)i));
            CHECK_EQUAL(1 << i, matProp.materialProperties);

            BuildMaterialProperties(&matProp, (MaterialInstanceProperties)i, false);
            CHECK_EQUAL(false, GetMaterialProperty(matProp, (MaterialInstanceProperties)i));
            CHECK_EQUAL(0, matProp.materialProperties);
        }
    }

    // - LODGroupA -
    // LOD0: Building, Detail0
    // LOD1: Building, Detail1
    //
    // An instance appearing in LOD0 and LOD1 should affect instances in both LOD0 and LOD1 in the same LOD group.
    TEST(LightmappedLOD_InstanceInLOD0AndLOD1_ShouldAffectInstancesInLOD0AndLOD1)
    {
        int lodMaskBuildingInLOD0andLOD1 = 1 << 1 | 1 << 0;
        int lodMaskDetailOnlyInLOD0 = 1 << 0;
        int lodMaskDetailOnlyInLOD1 = 1 << 1;
        int lodGroupCommon = 20000;

        // Test a ray originating from a texel on detail objects.
        CHECK_EQUAL(IsInstanceHit(lodMaskBuildingInLOD0andLOD1, lodGroupCommon, ConvertLODMaskToRayLODLevel(lodMaskDetailOnlyInLOD0), lodGroupCommon), true);
        CHECK_EQUAL(IsInstanceHit(lodMaskBuildingInLOD0andLOD1, lodGroupCommon, ConvertLODMaskToRayLODLevel(lodMaskDetailOnlyInLOD1), lodGroupCommon), true);
    }

    // - LODGroupA -
    // LOD0: Building, Detail0
    // LOD1: Building, Detail1
    //
    // An instance appearing in LOD1 only should not affect an instance that exists both in LOD0 and LOD1 in the same LOD group.
    TEST(LightmappedLOD_InstanceInLOD1Only_ShouldNotAffectInstanceInLOD0AndLOD1)
    {
        int lodMaskBuildingInLOD0andLOD1 = 1 << 1 | 1 << 0;
        int lodMaskDetailOnlyInLOD0 = 1 << 0;
        int lodMaskDetailOnlyInLOD1 = 1 << 1;
        int lodGroupCommon = 20000;

        // Test a ray originating from a texel on the building.
        CHECK_EQUAL(IsInstanceHit(lodMaskDetailOnlyInLOD1, lodGroupCommon, ConvertLODMaskToRayLODLevel(lodMaskBuildingInLOD0andLOD1), lodGroupCommon), false);
    }

    // - LODGroupA -
    // LOD0: BuildingLOD0, Detail0
    // LOD1: BuildingLOD1, Detail1
    //
    // An instance appearing on a given LOD level should only affect instances on the same level in the same LOD group.
    TEST(LightmappedLOD_InstanceInGivenLODLevel_ShouldOnlyAffectInstancesOnSameLODLevelInSameLODGroup)
    {
        int lodMaskBuildingOnlyInLOD0 = 1 << 0;
        int lodMaskBuildingOnlyInLOD1 = 1 << 1;
        int lodMaskDetailOnlyInLOD0 = 1 << 0;
        int lodMaskDetailOnlyInLOD1 = 1 << 1;
        int lodGroupCommon = 20000;

        // Test a ray originating from a texel on the building against the detail object in the respective LOD level.
        CHECK_EQUAL(IsInstanceHit(lodMaskDetailOnlyInLOD0, lodGroupCommon, ConvertLODMaskToRayLODLevel(lodMaskBuildingOnlyInLOD0), lodGroupCommon), true);
        CHECK_EQUAL(IsInstanceHit(lodMaskDetailOnlyInLOD1, lodGroupCommon, ConvertLODMaskToRayLODLevel(lodMaskBuildingOnlyInLOD1), lodGroupCommon), true);

        // Test a ray originating from a texel on the building against the building and the detail object in the other LOD level.
        CHECK_EQUAL(IsInstanceHit(lodMaskBuildingOnlyInLOD1, lodGroupCommon, ConvertLODMaskToRayLODLevel(lodMaskBuildingOnlyInLOD0), lodGroupCommon), false);
        CHECK_EQUAL(IsInstanceHit(lodMaskDetailOnlyInLOD1, lodGroupCommon, ConvertLODMaskToRayLODLevel(lodMaskBuildingOnlyInLOD0), lodGroupCommon), false);
        CHECK_EQUAL(IsInstanceHit(lodMaskBuildingOnlyInLOD0, lodGroupCommon, ConvertLODMaskToRayLODLevel(lodMaskBuildingOnlyInLOD1), lodGroupCommon), false);
        CHECK_EQUAL(IsInstanceHit(lodMaskDetailOnlyInLOD0, lodGroupCommon, ConvertLODMaskToRayLODLevel(lodMaskBuildingOnlyInLOD1), lodGroupCommon), false);
    }

    // - LODGroupA -
    // LOD0: Sphere
    // LOD1: Cube
    //
    // Instance appearing on just one LOD level in an LOD group should affect itself.
    TEST(LightmappedLOD_InstanceInLODGroupOnOneLODLevel_ShouldAffectItself)
    {
        int lodMaskSphereOnlyInLOD0 = 1 << 0;
        int lodMaskCubeOnlyInLOD1 = 1 << 1;
        int lodGroupCommon = 20000;

        // Test a ray originating from a texel on an LOD0 object against itself.
        CHECK_EQUAL(IsInstanceHit(lodMaskSphereOnlyInLOD0, lodGroupCommon, ConvertLODMaskToRayLODLevel(lodMaskSphereOnlyInLOD0), lodGroupCommon), true);

        // Test a ray originating from a texel on an LOD1 object against itself.
        CHECK_EQUAL(IsInstanceHit(lodMaskCubeOnlyInLOD1, lodGroupCommon, ConvertLODMaskToRayLODLevel(lodMaskCubeOnlyInLOD1), lodGroupCommon), true);
    }

    // - LODGroupA -
    // LOD0: Sphere
    // LOD1: Sphere
    //
    // Instance appearing on multiple LOD levels in an LOD group should affect itself.
    TEST(LightmappedLOD_InstanceInLODGroupOnMultipleLODLevels_ShouldAffectItself)
    {
        int lodMaskSphereInLOD0AndLOD1 = 1 << 1 | 1 << 0;
        int lodGroupSphere = 20000;

        // Test a ray originating from a texel on an LOD0 object against itself.
        CHECK_EQUAL(IsInstanceHit(lodMaskSphereInLOD0AndLOD1, lodGroupSphere, ConvertLODMaskToRayLODLevel(lodMaskSphereInLOD0AndLOD1), lodGroupSphere), true);
    }

    // - LODGroupA -
    // LOD0: BuildingLOD0
    // LOD1: BuildingLOD1
    //
    // - LODGroupB -
    // LOD0: ShedLOD0
    // LOD1: ShedLOD1
    //
    // Only LOD0 from an LOD group should affect instances in other LOD groups. In those groups it should affect instances on all the LOD levels.
    TEST(LightmappedLOD_OnlyLOD0FromLODGroup_ShouldAffectOtherLODGroups)
    {
        int lodMaskBuildingInLOD0 = 1 << 0;
        int lodMaskBuildingInLOD1 = 1 << 1;
        int lodGroupBuilding = 20000;

        int lodMaskShedInLOD0 = 1 << 0;
        int lodMaskShedInLOD1 = 1 << 1;
        int lodGroupShed = 30000;

        // Test a ray originating from a texel in another LOD group against LOD0 on a hit.
        CHECK_EQUAL(IsInstanceHit(lodMaskBuildingInLOD0, lodGroupBuilding, ConvertLODMaskToRayLODLevel(lodMaskShedInLOD0), lodGroupShed), true);
        CHECK_EQUAL(IsInstanceHit(lodMaskBuildingInLOD0, lodGroupBuilding, ConvertLODMaskToRayLODLevel(lodMaskShedInLOD1), lodGroupShed), true);

        // Test a ray originating from a texel in another LOD group against LOD1 on a hit.
        CHECK_EQUAL(IsInstanceHit(lodMaskBuildingInLOD1, lodGroupBuilding, ConvertLODMaskToRayLODLevel(lodMaskShedInLOD0), lodGroupShed), false);
        CHECK_EQUAL(IsInstanceHit(lodMaskBuildingInLOD1, lodGroupBuilding, ConvertLODMaskToRayLODLevel(lodMaskShedInLOD1), lodGroupShed), false);
    }

    // - LODGroupA -
    // LOD0: BuildingLOD0
    // LOD1: BuildingLOD1
    //
    // - No LODGroup -
    // GroundPlane
    //
    // An instance without an LOD group should be affected by instances appearing in LOD groups.
    TEST(LightmappedLOD_NonLODInstance_ShouldBeAffectedByLOD0Instances)
    {
        int lodMaskBuildingInLOD0 = 1 << 0;
        int lodMaskBuildingInLOD1 = 1 << 1;
        int lodGroupBuildingInLOD0andLOD1 = 20000;

        int lodMaskGroundPlane = NO_LOD_MASK;
        int lodGroupGroundPlane = NO_LOD_GROUP;

        // Test a ray originating from a texel on a non-LOD object, hitting an LOD0 object in an LOD group.
        CHECK_EQUAL(IsInstanceHit(lodMaskBuildingInLOD0, lodGroupBuildingInLOD0andLOD1, ConvertLODMaskToRayLODLevel(lodMaskGroundPlane), lodGroupGroundPlane), true);

        // Test a ray originating from a texel on a non-LOD object, hitting an LOD1 object in an LOD group.
        CHECK_EQUAL(IsInstanceHit(lodMaskBuildingInLOD1, lodGroupBuildingInLOD0andLOD1, ConvertLODMaskToRayLODLevel(lodMaskGroundPlane), lodGroupGroundPlane), false);
    }

    // - LODGroupA -
    // LOD0: BuildingLOD0
    // LOD1: BuildingLOD1
    //
    // - No LODGroup -
    // GroundPlane
    //
    // An instance without an LOD group should affect instances appearing in LOD groups.
    TEST(LightmappedLOD_NonLODInstance_ShouldAffectLODInstances)
    {
        int lodMaskBuildingInLOD0 = 1 << 0;
        int lodMaskBuildingInLOD1 = 1 << 1;
        int lodGroupBuildingInLOD0andLOD1 = 20000;

        int lodMaskGroundPlane = NO_LOD_MASK;
        int lodGroupGroundPlane = NO_LOD_GROUP;

        // Test a ray originating from a texel in an LOD group, hitting an object without an LOD group.
        CHECK_EQUAL(IsInstanceHit(lodMaskGroundPlane, lodGroupGroundPlane, ConvertLODMaskToRayLODLevel(lodMaskBuildingInLOD0), lodGroupBuildingInLOD0andLOD1), true);
        CHECK_EQUAL(IsInstanceHit(lodMaskGroundPlane, lodGroupGroundPlane, ConvertLODMaskToRayLODLevel(lodMaskBuildingInLOD1), lodGroupBuildingInLOD0andLOD1), true);
    }

    // - No LODGroup -
    // Building
    // GroundPlane
    //
    // An instance without an LOD group should affect other instances without an LOD group.
    TEST(LightmappedLOD_NonLODInstance_ShouldAffectOtherNonLODInstances)
    {
        int lodMaskBuilding = NO_LOD_MASK;
        int lodGroupBuilding = NO_LOD_GROUP;

        int lodMaskGroundPlane = NO_LOD_MASK;
        int lodGroupGroundPlane = NO_LOD_GROUP;

        // Test a ray originating from a texel on a non-LOD object, hitting an object also without an LOD group.
        CHECK_EQUAL(IsInstanceHit(lodMaskGroundPlane, lodGroupGroundPlane, ConvertLODMaskToRayLODLevel(lodMaskBuilding), lodGroupBuilding), true);
    }

    TEST(LightmappedLOD_NonLODInstance_EncodeAndDecode)
    {
        const int lodMask = NO_LOD_MASK;
        const int lodGroup = NO_LOD_GROUP;
        const int packedNoLODInfo = PackLODInfo(lodMask, lodGroup);
        const int unpackedLODMask = UnpackLODMask(packedNoLODInfo);
        const int unpackedLODGroup = UnpackLODGroup(packedNoLODInfo);
        CHECK_EQUAL(lodMask, unpackedLODMask);
        CHECK_EQUAL(lodGroup, unpackedLODGroup);
    }

    TEST(CPPSharedIncludes_GetTextureFetchOffset_InsideBoundsReturnCorrectOffset)
    {
        MaterialTextureProperties matProp;
        matProp.textureOffset = 0;

        int textureWidth = 28;
        int textureHeight = 56;
        SetMaterialPropertyTextureDimension(&matProp, textureWidth, textureHeight);

        int offset = 0;

        BuildMaterialProperties(&matProp, kMaterialInstanceProperties_WrapModeU_Clamp, true);
        BuildMaterialProperties(&matProp, kMaterialInstanceProperties_WrapModeV_Clamp, true);
        offset = GetTextureFetchOffset(matProp, 5, 10, false);
        CHECK_EQUAL(textureWidth * 10 + 5, offset);
        offset = GetTextureFetchOffset(matProp, 5, 10, true);
        CHECK_EQUAL(textureWidth * 10 + 5, offset);

        BuildMaterialProperties(&matProp, kMaterialInstanceProperties_WrapModeU_Clamp, false);
        BuildMaterialProperties(&matProp, kMaterialInstanceProperties_WrapModeV_Clamp, false);
        offset = GetTextureFetchOffset(matProp, 5, 10, false);
        CHECK_EQUAL(textureWidth * 10 + 5, offset);
        offset = GetTextureFetchOffset(matProp, 5, 10, true);
        CHECK_EQUAL(textureWidth * 10 + 5, offset);

        BuildMaterialProperties(&matProp, kMaterialInstanceProperties_WrapModeU_Clamp, true);
        BuildMaterialProperties(&matProp, kMaterialInstanceProperties_WrapModeV_Clamp, false);
        offset = GetTextureFetchOffset(matProp, 5, 10, false);
        CHECK_EQUAL(textureWidth * 10 + 5, offset);
        offset = GetTextureFetchOffset(matProp, 5, 10, true);
        CHECK_EQUAL(textureWidth * 10 + 5, offset);

        BuildMaterialProperties(&matProp, kMaterialInstanceProperties_WrapModeU_Clamp, false);
        BuildMaterialProperties(&matProp, kMaterialInstanceProperties_WrapModeV_Clamp, true);
        offset = GetTextureFetchOffset(matProp, 5, 10, false);
        CHECK_EQUAL(textureWidth * 10 + 5, offset);
        offset = GetTextureFetchOffset(matProp, 5, 10, true);
        CHECK_EQUAL(textureWidth * 10 + 5, offset);
    }

    TEST(CPPSharedIncludes_GetTextureFetchOffset_GlobalTextureOffsetIsAdded)
    {
        MaterialTextureProperties matProp;
        matProp.textureOffset = 0;
        SetMaterialPropertyTextureDimension(&matProp, 28, 56);
        matProp.materialProperties = 0;

        const int beforeOffset = GetTextureFetchOffset(matProp, 5, 10, false);

        matProp.textureOffset = 102;
        const int afterOffset = GetTextureFetchOffset(matProp, 5, 10, false);

        CHECK_EQUAL(beforeOffset + 102, afterOffset);
    }

    TEST(CPPSharedIncludes_GetTextureFetchOffset_WrapAccordingToMaterialParamInU)
    {
        MaterialTextureProperties matProp;
        matProp.textureOffset = 0;
        SetMaterialPropertyTextureDimension(&matProp, 10, 1);

        int offset = 0;

        //Clamp
        BuildMaterialProperties(&matProp, kMaterialInstanceProperties_WrapModeU_Clamp, true);
        offset = GetTextureFetchOffset(matProp, 5, 1, false);
        CHECK_EQUAL(5, offset);
        offset = GetTextureFetchOffset(matProp, 5, 12, false);
        CHECK_EQUAL(5, offset);

        offset = GetTextureFetchOffset(matProp, 15, 1, false);
        CHECK_EQUAL(9, offset);
        offset = GetTextureFetchOffset(matProp, 15, 12, false);
        CHECK_EQUAL(9, offset);

        offset = GetTextureFetchOffset(matProp, -5, 1, false);
        CHECK_EQUAL(0, offset);
        offset = GetTextureFetchOffset(matProp, -5, 12, false);
        CHECK_EQUAL(0, offset);

        //Repeat
        BuildMaterialProperties(&matProp, kMaterialInstanceProperties_WrapModeU_Clamp, false);
        offset = GetTextureFetchOffset(matProp, 5, 1, false);
        CHECK_EQUAL(5, offset);
        offset = GetTextureFetchOffset(matProp, 5, 12, false);
        CHECK_EQUAL(5, offset);

        offset = GetTextureFetchOffset(matProp, 15, 1, false);
        CHECK_EQUAL(5, offset);
        offset = GetTextureFetchOffset(matProp, 15, 12, false);
        CHECK_EQUAL(5, offset);

        offset = GetTextureFetchOffset(matProp, -5, 1, false);
        CHECK_EQUAL(5, offset);
        offset = GetTextureFetchOffset(matProp, -5, 12, false);
        CHECK_EQUAL(5, offset);

        //GBuffer filtering (clamp even if repeat mode is set)
        BuildMaterialProperties(&matProp, kMaterialInstanceProperties_WrapModeU_Clamp, false);
        offset = GetTextureFetchOffset(matProp, 5, 1, true);
        CHECK_EQUAL(5, offset);
        offset = GetTextureFetchOffset(matProp, 5, 12, true);
        CHECK_EQUAL(5, offset);

        offset = GetTextureFetchOffset(matProp, 15, 1, true);
        CHECK_EQUAL(9, offset);
        offset = GetTextureFetchOffset(matProp, 15, 12, true);
        CHECK_EQUAL(9, offset);

        offset = GetTextureFetchOffset(matProp, -5, 1, true);
        CHECK_EQUAL(0, offset);
        offset = GetTextureFetchOffset(matProp, -5, 12, true);
        CHECK_EQUAL(0, offset);
    }

    TEST(CPPSharedIncludes_GetTextureFetchOffset_WrapAccordingToMaterialParamInV)
    {
        MaterialTextureProperties matProp;
        matProp.textureOffset = 0;
        SetMaterialPropertyTextureDimension(&matProp, 1, 10);

        int offset = 0;

        //Clamp
        BuildMaterialProperties(&matProp, kMaterialInstanceProperties_WrapModeV_Clamp, true);
        offset = GetTextureFetchOffset(matProp, 1, 5, false);
        CHECK_EQUAL(5, offset);
        offset = GetTextureFetchOffset(matProp, 12, 5, false);
        CHECK_EQUAL(5, offset);

        offset = GetTextureFetchOffset(matProp, 1, 15, false);
        CHECK_EQUAL(9, offset);
        offset = GetTextureFetchOffset(matProp, 12, 15, false);
        CHECK_EQUAL(9, offset);

        offset = GetTextureFetchOffset(matProp, 1, -5, false);
        CHECK_EQUAL(0, offset);
        offset = GetTextureFetchOffset(matProp, 12, -5, false);
        CHECK_EQUAL(0, offset);

        //Repeat
        BuildMaterialProperties(&matProp, kMaterialInstanceProperties_WrapModeV_Clamp, false);
        offset = GetTextureFetchOffset(matProp, 1, 5, false);
        CHECK_EQUAL(5, offset);
        offset = GetTextureFetchOffset(matProp, 12, 5, false);
        CHECK_EQUAL(5, offset);

        offset = GetTextureFetchOffset(matProp, 1, 15, false);
        CHECK_EQUAL(5, offset);
        offset = GetTextureFetchOffset(matProp, 12, 15, false);
        CHECK_EQUAL(5, offset);

        offset = GetTextureFetchOffset(matProp, 1, -5, false);
        CHECK_EQUAL(5, offset);
        offset = GetTextureFetchOffset(matProp, 12, -5, false);
        CHECK_EQUAL(5, offset);

        //GBuffer (same as clamp) even if release mode is set
        BuildMaterialProperties(&matProp, kMaterialInstanceProperties_WrapModeV_Clamp, false);
        offset = GetTextureFetchOffset(matProp, 1, 5, true);
        CHECK_EQUAL(5, offset);
        offset = GetTextureFetchOffset(matProp, 12, 5, true);
        CHECK_EQUAL(5, offset);

        offset = GetTextureFetchOffset(matProp, 1, 15, true);
        CHECK_EQUAL(9, offset);
        offset = GetTextureFetchOffset(matProp, 12, 15, true);
        CHECK_EQUAL(9, offset);

        offset = GetTextureFetchOffset(matProp, 1, -5, true);
        CHECK_EQUAL(0, offset);
        offset = GetTextureFetchOffset(matProp, 12, -5, true);
        CHECK_EQUAL(0, offset);
    }

    struct SetTextureFixture
    {
        SetTextureFixture() : tex(buffer + padding)
        {
            Assert((2 * padding) < bufferSize);

            memset(buffer, 0, sizeof(Vector4f) * bufferSize);
            //Set padding alpha channel to 1;
            for (int i = 0; i < padding; ++i)
            {
                buffer[i].w = 1.0f;
                buffer[bufferSize - 1 - i].w = 1.0f;
            }

            //Add red lines
            for (int u = 0; u < textureSize; u += 2)
            {
                for (int v = 0; v < textureSize; ++v)
                {
                    tex[v * textureSize + u].x = 1.0f;
                }
            }

            //Material property
            matProp.textureOffset = 0;
            SetMaterialPropertyTextureDimension(&matProp, textureSize, textureSize);

            matProp.materialProperties = 0;
        }

        static const int textureSize = 10;
        Vector4f* const tex;
        MaterialTextureProperties matProp;

    private:
        //'buffer' is the actual memory while 'tex' is a pointer to a subpart of it.
        static const int padding = 20;
        static const int bufferSize = textureSize * textureSize + 2 * padding;
        Vector4f buffer[bufferSize];
    };

    TEST_FIXTURE(SetTextureFixture, CPPSharedIncludes_GetBilinearFilteredPixelColor_DoNotMixChannelNorFetchOutOfBounds)
    {
        const float tolerance = 0.001f;

        for (float u = -1.0f; u < 2.0f; u += 0.1f)
        {
            for (float v = -1.0f; v < 2.0f; v += 0.1f)
            {
                Vector4f filteringColor = GetBilinearFilteredPixelColor(tex, Vector2f(u, v), matProp, true);
                CHECK(filteringColor.x >= -tolerance);
                CHECK(filteringColor.x <= (1.0f + tolerance));
                CHECK_EQUAL(0.0f, filteringColor.y);
                CHECK_EQUAL(0.0f, filteringColor.z);
                CHECK_EQUAL(0.0f, filteringColor.w);
            }
        }
    }

    TEST_FIXTURE(SetTextureFixture, CPPSharedIncludes_GetBilinearFilteredPixelColor_NoBlendingAtTexelCenter)
    {
        BuildMaterialProperties(&matProp, kMaterialInstanceProperties_WrapModeU_Clamp, false);
        BuildMaterialProperties(&matProp, kMaterialInstanceProperties_WrapModeV_Clamp, false);
        const float tolerance = 0.001f;

        for (int i = -textureSize; i < (2 * textureSize); ++i)
        {
            for (int j = -textureSize; j < (2 * textureSize); ++j)
            {
                Vector2f uvs((float)i, (float)j);
                uvs.x += 0.5f;
                uvs.y += 0.5f;
                uvs /= textureSize;
                Vector4f filteringColor = GetBilinearFilteredPixelColor(tex, uvs, matProp, false);
                if (i % 2)
                    CHECK_CLOSE(0.0f, filteringColor.x, tolerance);
                else
                    CHECK_CLOSE(1.0f, filteringColor.x, tolerance);
                CHECK_EQUAL(0.0f, filteringColor.y);
                CHECK_EQUAL(0.0f, filteringColor.z);
                CHECK_EQUAL(0.0f, filteringColor.w);
            }
        }
    }

    TEST_FIXTURE(SetTextureFixture, CPPSharedIncludes_GetBilinearFilteredPixelColor_BilinearBlending)
    {
        BuildMaterialProperties(&matProp, kMaterialInstanceProperties_WrapModeU_Clamp, false);
        BuildMaterialProperties(&matProp, kMaterialInstanceProperties_WrapModeV_Clamp, false);
        const float tolerance = 0.001f;

        for (int i = -textureSize; i < (2 * textureSize); ++i)
        {
            for (int j = -textureSize; j < (2 * textureSize); ++j)
            {
                Vector2f uvs((float)i, (float)j);
                uvs /= textureSize;
                Vector4f filteringColor = GetBilinearFilteredPixelColor(tex, uvs, matProp, false);
                CHECK_CLOSE(0.5f, filteringColor.x, tolerance);
                CHECK_EQUAL(0.0f, filteringColor.y);
                CHECK_EQUAL(0.0f, filteringColor.z);
                CHECK_EQUAL(0.0f, filteringColor.w);
            }
        }
    }

    TEST_FIXTURE(SetTextureFixture, CPPSharedIncludes_GetNearestPixelColor_NoFetchOutOfBound_NoBlending)
    {
        for (float u = -1.0f; u < 2.0f; u += 0.1f)
        {
            for (float v = -1.0f; v < 2.0f; v += 0.1f)
            {
                Vector4f filteringColor = GetNearestPixelColor(tex, Vector2f(u, v), matProp, true);
                CHECK(filteringColor.x == 1.0f || filteringColor.x == 0.0f);
                CHECK_EQUAL(0.0f, filteringColor.y);
                CHECK_EQUAL(0.0f, filteringColor.z);
                CHECK_EQUAL(0.0f, filteringColor.w);
            }
        }
    }
}


#endif // ENABLE_UNIT_TESTS
