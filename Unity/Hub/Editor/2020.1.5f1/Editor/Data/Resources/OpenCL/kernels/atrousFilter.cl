/* Edge-Avoiding A-Trous Wavelet Filter
   see: https://jo.dreggn.org/home/2010_atrous.pdf
*/

#include "commonCL.h"

#if !defined(BLOCK_SIZE)
#define BLOCK_SIZE 8
#endif

// ----------------------------------------------------------------------------------------------------

#define REFLECT(x, max) \
   if (x < 0) x = - x - 1; \
   if (x >= max) x =  2 * max - x - 1;

// ----------------------------------------------------------------------------------------------------

#define FILL_LOCAL_BUFFER(source, srcBufferSize, coord, localBuff, halfWindow, localStart) \
do \
{ \
   int localX = get_local_id(0); \
   int localY = get_local_id(1); \
   int beg = 0, end = 0; \
   int windowSize = (BLOCK_SIZE + halfWindow*2)*(BLOCK_SIZE + halfWindow*2); \
   int block = ceil((float)(windowSize) / (BLOCK_SIZE * BLOCK_SIZE)); \
   if (localX + localY*BLOCK_SIZE <= ceil((float)(windowSize) / block)) \
   { \
      beg = (localX + localY*BLOCK_SIZE)*block; \
      end = beg + block; \
      end = clamp(end, 0, windowSize); \
   } \
   localStart = (int2)((coord.x & (~(BLOCK_SIZE - 1))) - halfWindow, (coord.y & (~(BLOCK_SIZE - 1))) - halfWindow); \
   localStart = clamp(localStart, (int2)0, srcBufferSize - (int2)1); \
   for (int i = beg; i < end; ++i) \
   { \
      int2 xy = (int2)(localStart.x + (i % (BLOCK_SIZE + halfWindow*2)), localStart.y + (i / (BLOCK_SIZE + halfWindow*2))); \
      xy = clamp(xy, 0, srcBufferSize - (int2)1); \
      localBuff[i] = read_imagef(source, kSamplerClampNearestUnormCoords, xy); \
   } \
   barrier(CLK_LOCAL_MEM_FENCE); \
} \
while(0);

// ----------------------------------------------------------------------------------------------------

#define DERIVATE(buffer, bufferSize, coord, halfWindow, localStart, dFdX, dFdY) \
do \
{ \
   int left = clamp(coord.x - 1, 0, bufferSize.x - 1); \
   int right = clamp(coord.x + 1, 0, bufferSize.x - 1); \
   int top = clamp(coord.y - 1, 0, bufferSize.y - 1); \
   int bottom = clamp(coord.y + 1, 0, bufferSize.y - 1); \
   dFdX = (buffer[(left - localStart.x) + (BLOCK_SIZE + halfWindow*2)*(top - localStart.y)]  \
        - buffer[(right - localStart.x) + (BLOCK_SIZE + halfWindow*2)*(top - localStart.y)] \
        + 2 * (buffer[(left - localStart.x) + (BLOCK_SIZE + halfWindow*2)*(coord.y - localStart.y)]  \
               - buffer[(right - localStart.x) + (BLOCK_SIZE + halfWindow*2)*(coord.y - localStart.y)]) \
        + buffer[(left - localStart.x) + (BLOCK_SIZE + halfWindow*2)*(bottom - localStart.y)] \
        - buffer[(right - localStart.x) + (BLOCK_SIZE + halfWindow*2)*(bottom - localStart.y)]); \
   dFdY = (buffer[(left - localStart.x) + (BLOCK_SIZE + halfWindow*2)*(top - localStart.y)]  \
        - buffer[(left - localStart.x) + (BLOCK_SIZE + halfWindow*2)*(bottom - localStart.y)] \
        + 2 * (buffer[(coord.x - localStart.x) + (BLOCK_SIZE + halfWindow*2)*(top - localStart.y)]  \
               - buffer[(coord.x - localStart.x) + (BLOCK_SIZE + halfWindow*2)*(bottom - localStart.y)]) \
        + buffer[(right - localStart.x) + (BLOCK_SIZE + halfWindow*2)*(top - localStart.y)] \
        - buffer[(right - localStart.x) + (BLOCK_SIZE + halfWindow*2)*(bottom - localStart.y)]); \
} \
while(0);


// ----------------------------------------------------------------------------------------------------
// Similarity function
static inline float C(float3 x1, float3 x2, float sigma)
{
   float3 distance = x1 - x2;
   float a = fast_length(convert_float3(distance)) / sigma;
   return native_exp(-a);
}

// ----------------------------------------------------------------------------------------------------
// Depth similarity function
static inline float dW(float x1, float x2, float sigma)
{
   float a = fabs(x1 - x2) / sigma;
   return native_exp(-a);
}

// ----------------------------------------------------------------------------------------------------
// Normals similarity function
static inline float nW(float3 x1, float3 x2, float sigma)
{
   x1 = normalize(x1 + make_float3(0.01f, 0.01f, 0.01f));
   x2 = normalize(x2 + make_float3(0.01f, 0.01f, 0.01f));
   float a = fmax((float)0.0f, dot(x1, x2));

   return pow(a, (float)1.0f / sigma);
}

// ----------------------------------------------------------------------------------------------------
static inline float4 SampleGauss3x3F(__read_only image2d_t buffer, int2 buffer_size, int2 coord, __local float4* window)
{
   int2 tl = clamp(coord - (int2)1, (int2)0, buffer_size - (int2)1);
   int2 br = clamp(coord + (int2)1, (int2)0, buffer_size - (int2)1);
   int2 localStart;

   FILL_LOCAL_BUFFER(buffer, buffer_size, coord, window, 1, localStart);

   float4 bluredVal = 0.077847f * (
      window[(tl.x - localStart.x) + (BLOCK_SIZE + 2)*(tl.y - localStart.y)]
      + window[(br.x - localStart.x) + (BLOCK_SIZE + 2)*(tl.y - localStart.y)]
      + window[(tl.x - localStart.x) + (BLOCK_SIZE + 2)*(br.y - localStart.y)]
      + window[(br.x - localStart.x) + (BLOCK_SIZE + 2)*(br.y - localStart.y)] );

   bluredVal += 0.123317f * (
      window[(coord.x - localStart.x) + (BLOCK_SIZE + 2)*(tl.y - localStart.y)]
      + window[(br.x - localStart.x) + (BLOCK_SIZE + 2)*(coord.y - localStart.y)]
      + window[(tl.x - localStart.x) + (BLOCK_SIZE + 2)*(coord.y - localStart.y)]
      + window[(coord.x - localStart.x) + (BLOCK_SIZE + 2)*(br.y - localStart.y)] );

   bluredVal += 0.195346f * window[(coord.x - localStart.x) + (BLOCK_SIZE + 2)*(coord.y - localStart.y)];
   return bluredVal;
}

// ----------------------------------------------------------------------------------------------------
__kernel void ATrousKernel(
    __read_only image2d_t dynarg_srcTile0, // Source buffer
    __read_only image2d_t dynarg_srcTile1, // Normals/ChartId buffer
    __read_only image2d_t dynarg_srcTile2, // Prev variance buffer
   __write_only image2d_t dynarg_dstTile , // Dest buffer
   __write_only image2d_t dynarg_dstTile1, // Dest variance buffer
    INPUT_VALUE( 5, int2  ,   imageSize),
    INPUT_VALUE( 6, float4,   sigma ),
    INPUT_VALUE( 7, int   ,   coordOffset )
    KERNEL_VALIDATOR_BUFFERS_DEF)
{
   int2 coord = (int2)(get_global_id(0), get_global_id(1));

   float2 gradDpt = 0.0f;
   __local float4 window[(BLOCK_SIZE + 2)*(BLOCK_SIZE + 2)];
   float4 dFdX, dFdY;
   int2 localStart;
   FILL_LOCAL_BUFFER(dynarg_srcTile1, imageSize, coord, window, 1, localStart);
   DERIVATE(window, imageSize, coord, 1, localStart, dFdX, dFdY);
   barrier(CLK_LOCAL_MEM_FENCE);

   gradDpt.x = dFdX.w;
   gradDpt.y = dFdY.w;

   // color variance value
#ifdef FIRST_PASS
   float colVar = fast_length(convert_float3(SampleGauss3x3F(dynarg_srcTile0, imageSize, coord, window).xyz));
#else
   float colVar = READ_IMAGEF_SAFE(dynarg_srcTile2, kSamplerClampNearestUnormCoords, coord).x;
#endif

   //B3 spline
   const float kernl[] =
   {
      1.0f / 256, 1.0f / 64, 3.0f / 128, 1.0f / 64, 1.0f / 256,
      1.0f / 64, 1.0f / 16, 3.0f / 32, 1.0f / 16, 1.0f / 64,
      3.0f / 128, 3.0f / 32, 9.0f / 64, 3.0f / 32, 3.0f / 128,
      1.0f / 64, 1.0f / 16, 3.0f / 32, 1.0f / 16, 1.0f / 64,
      1.0f / 256, 1.0f / 64, 3.0f / 128, 1.0f / 64, 1.0f / 256
   };

   // color value at the center of the window
   float4 temp = READ_IMAGEF_SAFE(dynarg_srcTile0, kSamplerClampNearestUnormCoords, coord);
   float3 qcol = temp.xyz;
   const float srcAlpha = temp.w;

   // normal/depth value at the center of the window
   temp = READ_IMAGEF_SAFE(dynarg_srcTile1, kSamplerClampNearestUnormCoords, coord);
   float3 qnorm = temp.xyz;
   float qdpt   = temp.w;

   float4 out = 0.0f;
   float sum  = 0.0f;
   float vsum = 0.0f;

   const float colSigma   = sigma.x;
   const float normSigma  = sigma.y;
   const float depthSigma = sigma.z;

   for (int i = -2; i <= 2; ++i)
      for (int j = -2; j <= 2; ++j)
      {
         int2 offsetUV;
         offsetUV.x = coord.x + i * coordOffset;
         offsetUV.y = coord.y + j * coordOffset;

         REFLECT(offsetUV.x, imageSize.x)
         REFLECT(offsetUV.y, imageSize.y)
         offsetUV.x = clamp(offsetUV.x, (int)0, (int)imageSize.x-1);
         offsetUV.y = clamp(offsetUV.y, (int)0, (int)imageSize.y-1);

         float coeff = kernl[i + 2 + (j + 2) * 5];

         float4 temp;
         float3 c;
         float multiplier;

         temp = READ_IMAGEF_SAFE(dynarg_srcTile0, kSamplerClampNearestUnormCoords, offsetUV);
         c = temp.xyz;

         multiplier = C(c, qcol, colSigma * sqrt(colVar) + 1.0e-5f);
         coeff *= multiplier > 0.0f ? multiplier : 0.0f;

         // Normal edge stopping
         temp = READ_IMAGEF_SAFE(dynarg_srcTile1, kSamplerClampNearestUnormCoords, offsetUV);
         multiplier = nW(temp.xyz, qnorm, normSigma);
         coeff *= multiplier > 0.0f ? multiplier : 0.0f;

         // Depth edge stopping
         multiplier = dW(temp.w, qdpt, depthSigma * fabs(dot(gradDpt, make_float2(i * coordOffset, j * coordOffset))) + 1.0e-3f);
         coeff *= multiplier > 0.0f ? multiplier : 0.0f;

         //temp = ReadPixelTyped(transBuff, cx, cy);
         //multiplier = C(temp.xyz / temp.w, qtrans, transSigma);
         //coeff *= multiplier > 0.0f ? multiplier : 0.0f;

         out.xyz += c * coeff;

#ifdef FIRST_PASS
         vsum += fast_length(c) * coeff * coeff;
#else
         vsum += READ_IMAGEF_SAFE(dynarg_srcTile2, kSamplerClampNearestUnormCoords, offsetUV).x * coeff * coeff;
#endif
         sum += coeff;
      }

   out.w   = srcAlpha;
   out.xyz = sum > make_float3(0.0f, 0.0f, 0.0f) ? out.xyz / sum : make_float3(0.0f, 0.0f, 0.0f);

#if !defined(LAST_PASS)
   vsum /= sum * sum;

   //Back prop variance
   WRITE_IMAGEF_SAFE(dynarg_dstTile1, coord, make_float4(vsum,vsum,vsum,vsum));
#endif

   WRITE_IMAGEF_SAFE(dynarg_dstTile, coord, out);
}
