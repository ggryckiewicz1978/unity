#include "commonCL.h"
#include "colorSpace.h"
#include "rgbmEncoding.h"

__constant float4 kZero = (float4)(0.0f, 0.0f, 0.0f, 0.0f);
__constant float4 kOne = (float4)(1.0f, 1.0f, 1.0f, 1.0f);
__constant float4 kHalf = (float4)(0.5, 0.5f, 0.5f, 0.5f);

// ----------------------------------------------------------------------------------------------------
static uint ConvertLightmapCoordinatesToIndex(int2 lightmapCoords, int lightmapSize)
{
    const int2 minval = (int2)(0, 0);
    const int2 maxval = (int2)(lightmapSize - 1, lightmapSize - 1);
    const int2 clampedCoords = clamp(lightmapCoords, minval, maxval);
    return clampedCoords.y * lightmapSize + clampedCoords.x;
}

// ----------------------------------------------------------------------------------------------------
static int ReadChartId(
    INPUT_BUFFER(0, int,    chartIndexBuffer),
    INPUT_VALUE(1, int,     lightmapSize),
    INPUT_VALUE(2, int2,    lightmapCoords)
    KERNEL_VALIDATOR_BUFFERS_DEF)
{
    const int index = ConvertLightmapCoordinatesToIndex(lightmapCoords, lightmapSize);
    return INDEX_SAFE(chartIndexBuffer, index);
}


// ----------------------------------------------------------------------------------------------------
__kernel void compositingBlit(
    __write_only image2d_t   dynarg_dstImage,
    __read_only image2d_t    dynarg_srcImage
    KERNEL_VALIDATOR_BUFFERS_DEF)
{
    // Image coordinates
    int2 coords = (int2)(get_global_id(0), get_global_id(1));

    float4 srcColor = READ_IMAGEF_SAFE(dynarg_srcImage, kSamplerClampNearestUnormCoords, coords);
    WRITE_IMAGEF_SAFE(dynarg_dstImage, coords, srcColor);
}

// ----------------------------------------------------------------------------------------------------
__kernel void compositingMarkupInvalidTexels(
    __write_only image2d_t      dynarg_dstTile,
    __read_only image2d_t       dynarg_srcTile,
    INPUT_BUFFER(2, int,        indirectSampleCountBuffer),
    INPUT_BUFFER(3, float,      outputValidityBuffer),
    INPUT_BUFFER(4, unsigned char, occupancyBuffer),
    INPUT_VALUE( 5, float,      backfaceTolerance),
    INPUT_VALUE( 6, int2,       tileCoordinates),
    INPUT_VALUE( 7, int,        lightmapSize)
    KERNEL_VALIDATOR_BUFFERS_DEF)
{
    // Coordinates in tile space
    int2 tileThreadId = (int2)(get_global_id(0), get_global_id(1));

    // Coordinates in lightmap space
    int2 lightmapCoords = tileThreadId + tileCoordinates;

    uint index = ConvertLightmapCoordinatesToIndex(lightmapCoords, lightmapSize);

    const int sampleCount = INDEX_SAFE(indirectSampleCountBuffer, index);

    const float validityValue    = INDEX_SAFE(outputValidityBuffer, index);
    float4 value                 = READ_IMAGEF_SAFE(dynarg_srcTile, kSamplerClampNearestUnormCoords, tileThreadId);

    const bool backfaceInvalid = sampleCount <= 0 ? false : ((validityValue / sampleCount) > (1.f - backfaceTolerance));
    if (backfaceInvalid)
    {
        value.w = 0.0f;
    }
    else
    {
        const int occupiedSamplesWithinTexel = INDEX_SAFE(occupancyBuffer, index);
        value.w = ((occupiedSamplesWithinTexel > 0) ? 1.f : 0.f);
    }
    WRITE_IMAGEF_SAFE(dynarg_dstTile, tileThreadId, value);
}

// ----------------------------------------------------------------------------------------------------
__kernel void compositingDirect(
    __write_only image2d_t  dynarg_dstTile,
    INPUT_BUFFER(1, float4,  outputDirectLightingBuffer),
    INPUT_BUFFER(2, int,     directSampleCountBuffer),
    INPUT_VALUE( 3, int2,    tileCoordinates),
    INPUT_VALUE( 4, int,     lightmapSize)
    KERNEL_VALIDATOR_BUFFERS_DEF)
{
    // Coordinates in tile space
    int2 tileThreadId = (int2)(get_global_id(0), get_global_id(1));

    // Coordinates in lightmap space
    int2 lightmapCoords = tileThreadId + tileCoordinates;

    const uint index = ConvertLightmapCoordinatesToIndex(lightmapCoords, lightmapSize);

    const int currentDirectSampleCount = INDEX_SAFE(directSampleCountBuffer, index);
    if (currentDirectSampleCount <= 0)
        return;

    const float3 lightingValue = INDEX_SAFE(outputDirectLightingBuffer, index).xyz;

    float4 result;
    result.xyz = lightingValue / currentDirectSampleCount;
    result.w = 1.f;

    WRITE_IMAGEF_SAFE(dynarg_dstTile, tileThreadId, result);
}

// ----------------------------------------------------------------------------------------------------
__kernel void compositingShadowMask(
    __write_only image2d_t      dynarg_dstTile,
    INPUT_BUFFER(1, int,        directSampleCountBuffer),
    INPUT_BUFFER(2, float4,     outputShadowmaskFromDirectBuffer),
    INPUT_VALUE( 3, int2,       tileCoordinates),
    INPUT_VALUE( 4, int,        lightmapSize)
    KERNEL_VALIDATOR_BUFFERS_DEF)
{
    // Coordinates in tile space
    int2 tileThreadId = (int2)(get_global_id(0), get_global_id(1));

    // Coordinates in lightmap space
    int2 lightmapCoords = tileThreadId + tileCoordinates;

    uint index = ConvertLightmapCoordinatesToIndex(lightmapCoords, lightmapSize);

    const int currentDirectSampleCount = INDEX_SAFE(directSampleCountBuffer, index);
    if (currentDirectSampleCount <= 0)
        return;

    KERNEL_ASSERT(currentDirectSampleCount > 0);
    const float4 shadowMaskValue = INDEX_SAFE(outputShadowmaskFromDirectBuffer, index);
    float4 result = shadowMaskValue / currentDirectSampleCount;
    result = saturate4(result);

    WRITE_IMAGEF_SAFE(dynarg_dstTile, tileThreadId, result);
}

// ----------------------------------------------------------------------------------------------------
__kernel void compositingIndirect(
    __write_only image2d_t  dynarg_dstTile,
    INPUT_BUFFER(1, float4, outputIndirectLightingBuffer),
    INPUT_BUFFER(2, int,    indirectSampleCountBuffer),
    INPUT_BUFFER(3, float4, outputEnvironmentLightingBuffer),
    INPUT_BUFFER(4, int,    environmentSampleCountBuffer),
    INPUT_VALUE( 5, float,  indirectIntensity),
    INPUT_VALUE( 6, int2,   tileCoordinates),
    INPUT_VALUE( 7, int,    lightmapSize)
    KERNEL_VALIDATOR_BUFFERS_DEF)
{
    // Coordinates in tile space
    int2 tileThreadId = (int2)(get_global_id(0), get_global_id(1));

    // Coordinates in lightmap space
    int2 lightmapCoords = tileThreadId + tileCoordinates;

    uint index = ConvertLightmapCoordinatesToIndex(lightmapCoords, lightmapSize);

    const int sampleCount = INDEX_SAFE(indirectSampleCountBuffer, index);
    if (sampleCount == 0)
        return;

    const int envSampleCount = INDEX_SAFE(environmentSampleCountBuffer, index);
    if (envSampleCount == 0)
        return;

    KERNEL_ASSERT(sampleCount > 0);
    KERNEL_ASSERT(envSampleCount > 0);
    float4 indirectLightValue = INDEX_SAFE(outputIndirectLightingBuffer, index);
    float4 environmentValue   = INDEX_SAFE(outputEnvironmentLightingBuffer, index);

    float4 result = indirectIntensity * (indirectLightValue / sampleCount + environmentValue / envSampleCount);
    result.w = 1.0f;

    WRITE_IMAGEF_SAFE(dynarg_dstTile, tileThreadId, result);
}

// ----------------------------------------------------------------------------------------------------
__kernel void compositingAO(
    __write_only image2d_t      dynarg_dstTile,
    INPUT_BUFFER(1, float,      outputAoBuffer),
    INPUT_BUFFER(2, int,        indirectSampleCountBuffer),
    INPUT_VALUE( 3, int2,       tileCoordinates),
    INPUT_VALUE( 4, float,      aoExponent),
    INPUT_VALUE( 5, int,        lightmapSize)
    KERNEL_VALIDATOR_BUFFERS_DEF)
{
    // Coordinates in tile space
    int2 tileThreadId = (int2)(get_global_id(0), get_global_id(1));

    // Coordinates in lightmap space
    int2 lightmapCoords = tileThreadId + tileCoordinates;

    uint index = ConvertLightmapCoordinatesToIndex(lightmapCoords, lightmapSize);

    const int sampleCount = INDEX_SAFE(indirectSampleCountBuffer, index);

    if (sampleCount == 0)
        return;

    float aoValue = INDEX_SAFE(outputAoBuffer, index);
    KERNEL_ASSERT(sampleCount > 0);
    aoValue = aoValue / (float)sampleCount;

    aoValue = pow(aoValue, aoExponent);

    float4 result = (float4)(aoValue, aoValue, aoValue, 1.0f);

    WRITE_IMAGEF_SAFE(dynarg_dstTile, tileThreadId, result);
}

// ----------------------------------------------------------------------------------------------------
__kernel void compositingAddLighting(
    __write_only image2d_t      dynarg_dstTile,
    __read_only image2d_t       dynarg_srcTile0,    // directLightingImage
    __read_only image2d_t       dynarg_srcTile1     // indirectLightingImage
    KERNEL_VALIDATOR_BUFFERS_DEF)
{
    // Coordinates in tile space
    int2 tileThreadId = (int2)(get_global_id(0), get_global_id(1));

    float4 directLightingValue = READ_IMAGEF_SAFE(dynarg_srcTile0, kSamplerClampNearestUnormCoords, tileThreadId);
    float4 indirectLightingValue = READ_IMAGEF_SAFE(dynarg_srcTile1, kSamplerClampNearestUnormCoords, tileThreadId);

    float4 result = directLightingValue + indirectLightingValue;
    result.w = saturate1(directLightingValue.w * indirectLightingValue.w);

    WRITE_IMAGEF_SAFE(dynarg_dstTile, tileThreadId, result);
}

// ----------------------------------------------------------------------------------------------------
__kernel void compositingDilate(
    __write_only image2d_t          dynarg_dstTile,
    __read_only image2d_t           dynarg_srcTile,
    INPUT_BUFFER(2, unsigned char,  occupancyBuffer),
    INPUT_VALUE( 3, int,            useOccupancy),
    INPUT_VALUE( 4, int2,           tileCoordinates),
    INPUT_VALUE( 5, int,            lightmapSize)
    KERNEL_VALIDATOR_BUFFERS_DEF)
{
    // Coordinates in tile space
    int2 tileThreadId = (int2)(get_global_id(0), get_global_id(1));

    float4 inputValue = READ_IMAGEF_SAFE(dynarg_srcTile, kSamplerClampNearestUnormCoords, tileThreadId);

    // The texel is valid -> just write it to the output
    if (inputValue.w > 0)
    {
        WRITE_IMAGEF_SAFE(dynarg_dstTile, tileThreadId, inputValue);
        return;
    }

    if (useOccupancy) // Internal dilation
    {
        // Coordinates in lightmap space
        int2 lightmapCoords = tileThreadId + tileCoordinates;

        int index = ConvertLightmapCoordinatesToIndex(lightmapCoords, lightmapSize);

        const int occupiedSamplesWithinTexel = INDEX_SAFE(occupancyBuffer, index);

        // A non-occupied texel, just copy when doing internal dilation.
        if (occupiedSamplesWithinTexel == 0)
        {
            WRITE_IMAGEF_SAFE(dynarg_dstTile, tileThreadId, inputValue);
            return;
        }
    }

    float4 dilated = kZero;
    float weightCount = 0.0f;

    // Note: not using READ_IMAGEF_SAFE below as those samples are expected to read just outside of the tile boundary, they will get safely clamped though.

    // Upper row
    float4 value0 = read_imagef(dynarg_srcTile, kSamplerClampNearestUnormCoords, tileThreadId + (int2)(-1, -1));
    float4 value1 = read_imagef(dynarg_srcTile, kSamplerClampNearestUnormCoords, tileThreadId + (int2)(0, -1));
    float4 value2 = read_imagef(dynarg_srcTile, kSamplerClampNearestUnormCoords, tileThreadId + (int2)(1, -1));

    // Side values
    float4 value3 = read_imagef(dynarg_srcTile, kSamplerClampNearestUnormCoords, tileThreadId + (int2)(-1, 0));
    float4 value4 = read_imagef(dynarg_srcTile, kSamplerClampNearestUnormCoords, tileThreadId + (int2)(1, 0));

    // Bottom row
    float4 value5 = read_imagef(dynarg_srcTile, kSamplerClampNearestUnormCoords, tileThreadId + (int2)(-1, 1));
    float4 value6 = read_imagef(dynarg_srcTile, kSamplerClampNearestUnormCoords, tileThreadId + (int2)(0, 1));
    float4 value7 = read_imagef(dynarg_srcTile, kSamplerClampNearestUnormCoords, tileThreadId + (int2)(1, 1));

    dilated = value0.w * value0;
    dilated += value1.w * value1;
    dilated += value2.w * value2;
    dilated += value3.w * value3;
    dilated += value4.w * value4;
    dilated += value5.w * value5;
    dilated += value6.w * value6;
    dilated += value7.w * value7;

    weightCount = value0.w;
    weightCount += value1.w;
    weightCount += value2.w;
    weightCount += value3.w;
    weightCount += value4.w;
    weightCount += value5.w;
    weightCount += value6.w;
    weightCount += value7.w;

    dilated *= 1.0f / max(1.0f, weightCount);

    dilated.w = saturate1(weightCount);

    WRITE_IMAGEF_SAFE(dynarg_dstTile, tileThreadId, dilated);
}

// ----------------------------------------------------------------------------------------------------
__kernel void compositingNormalizeDirectionality(
    __write_only image2d_t               dynarg_dstTile,
    INPUT_BUFFER(1, float4,              dynarg_directionalityBuffer),
    INPUT_BUFFER(2, PackedNormalOctQuad, interpNormalsWSBuffer),
    INPUT_VALUE( 3, int2,                tileCoordinates),
    INPUT_VALUE( 4, int,                 lightmapSize),
    INPUT_VALUE( 5, int,                 superSamplingMultiplier)
    KERNEL_VALIDATOR_BUFFERS_DEF)
{
    int2 tileThreadId = (int2)(get_global_id(0), get_global_id(1));

    int2 lightmapCoords = tileThreadId + tileCoordinates;

    int index = ConvertLightmapCoordinatesToIndex(lightmapCoords, lightmapSize);

    float4 dir = INDEX_SAFE(dynarg_directionalityBuffer, index);
    dir = dir / max(0.001f, dir.w);

    float3 normalWS = CalculateSuperSampledInterpolatedNormal(index, superSamplingMultiplier, interpNormalsWSBuffer KERNEL_VALIDATOR_BUFFERS);

    // Compute rebalancing coefficients
    dir.w = dot(normalWS.xyz, dir.xyz);

    dir = dir * kHalf + kHalf;

    WRITE_IMAGEF_SAFE(dynarg_dstTile, tileThreadId, dir);
}

// ----------------------------------------------------------------------------------------------------
__kernel void compositingDecodeNormalsWS(
    __write_only image2d_t               dynarg_dstTile,
    INPUT_BUFFER(1, PackedNormalOctQuad, interpNormalsWSBuffer),
    INPUT_BUFFER(2, int,                 chartIndexBuffer),
    INPUT_VALUE( 3, int2,                tileCoordinates),
    INPUT_VALUE( 4, int,                 lightmapSize),
    INPUT_VALUE( 5, int,                 superSamplingMultiplier)
    KERNEL_VALIDATOR_BUFFERS_DEF)
{
    int2 tileThreadId = (int2)(get_global_id(0), get_global_id(1));

    // Coordinates in lightmap space
    int2 lightmapCoords = tileThreadId + tileCoordinates;
    int index = ConvertLightmapCoordinatesToIndex(lightmapCoords, lightmapSize);

    int centerChartId = ReadChartId(chartIndexBuffer, lightmapSize, lightmapCoords KERNEL_VALIDATOR_BUFFERS);

    float4 dir;

    dir.xyz = CalculateSuperSampledInterpolatedNormal(index, superSamplingMultiplier, interpNormalsWSBuffer KERNEL_VALIDATOR_BUFFERS);
    dir.w   = (float)centerChartId;

    WRITE_IMAGEF_SAFE(dynarg_dstTile, tileThreadId, dir);
}

// ----------------------------------------------------------------------------------------------------
__kernel void compositingCombineDirectionality(
    __write_only image2d_t      dynarg_dstTile,
    __read_only image2d_t       dynarg_srcTile0, // directLightingImage
    __read_only image2d_t       dynarg_srcTile1, // indirectLightingImage
    __read_only image2d_t       dynarg_srcTile2, // directionalityFromDirectImage
    __read_only image2d_t       dynarg_srcTile3, // directionalityFromIndirectImage
    INPUT_VALUE(5, float,       indirectScale)
    KERNEL_VALIDATOR_BUFFERS_DEF)
{
    int2 tileThreadId = (int2)(get_global_id(0), get_global_id(1));

    float4 directLighting               = READ_IMAGEF_SAFE(dynarg_srcTile0, kSamplerClampNearestUnormCoords, tileThreadId);
    float4 indirectLighting             = READ_IMAGEF_SAFE(dynarg_srcTile1, kSamplerClampNearestUnormCoords, tileThreadId);
    float4 directionalityFromDirect     = READ_IMAGEF_SAFE(dynarg_srcTile2, kSamplerClampNearestUnormCoords, tileThreadId);
    float4 directionalityFromIndirect   = READ_IMAGEF_SAFE(dynarg_srcTile3, kSamplerClampNearestUnormCoords, tileThreadId);

    float directWeight      = Luminance(directLighting.xyz) * length(directionalityFromDirect.xyz);
    float indirectWeight    = Luminance(indirectLighting.xyz) * length(directionalityFromIndirect.xyz) * indirectScale;

    float normalizationWeight = directWeight + indirectWeight;

    directWeight = directWeight / max(0.0001f, normalizationWeight);

    float4 output = select(directionalityFromDirect, lerp4(directionalityFromIndirect, directionalityFromDirect, (float4)directWeight), (int4)(-(indirectLighting.w > 0.0f)));

    WRITE_IMAGEF_SAFE(dynarg_dstTile, tileThreadId, output);
}

// ----------------------------------------------------------------------------------------------------
__kernel void compositingSplitRGBA(
    __write_only image2d_t      dynarg_dstTile0,    // outRGBImage
    __write_only image2d_t      dynarg_dstTile1,    // outAlphaImage
    __read_only image2d_t       dynarg_srcTile0,    // directionalLightmap
    __read_only image2d_t       dynarg_srcTile1     // lightmap
    KERNEL_VALIDATOR_BUFFERS_DEF)
{
    int2 tileCoordinates = (int2)(get_global_id(0), get_global_id(1));

    float4 directionalValue = READ_IMAGEF_SAFE(dynarg_srcTile0, kSamplerClampNearestUnormCoords, tileCoordinates);
    float4 lightmapValue    = READ_IMAGEF_SAFE(dynarg_srcTile1, kSamplerClampNearestUnormCoords, tileCoordinates);

    float4 rgbValue         = (float4)(directionalValue.xyz, lightmapValue.w);
    float4 alphaValue       = (float4)(directionalValue.www, lightmapValue.w);

    WRITE_IMAGEF_SAFE(dynarg_dstTile0, tileCoordinates, rgbValue);
    WRITE_IMAGEF_SAFE(dynarg_dstTile1, tileCoordinates, alphaValue);
}

// ----------------------------------------------------------------------------------------------------
__kernel void compositingMergeRGBA(
    __write_only image2d_t      dynarg_dstTile,
    __read_only image2d_t       dynarg_srcTile0,    // dirRGBDilatedImage
    __read_only image2d_t       dynarg_srcTile1,    // dirAlphaDilatedImage
    INPUT_VALUE(3, uint,        tileBorderWidth)
    KERNEL_VALIDATOR_BUFFERS_DEF)
{
    // Coordinates in tile space
    int2 tileCoords = (int2)(get_global_id(0), get_global_id(1));

    // Discard tile border(output is smaller than the input)
    int2 sampleCoords = (int2)(tileCoords.x + tileBorderWidth, tileCoords.y + tileBorderWidth);

    float4 dirRGBValue      = READ_IMAGEF_SAFE(dynarg_srcTile0, kSamplerClampNearestUnormCoords, sampleCoords);
    float4 dirAlphaValue    = READ_IMAGEF_SAFE(dynarg_srcTile1, kSamplerClampNearestUnormCoords, sampleCoords);

    float4 result = (float4)(dirRGBValue.xyz, dirAlphaValue.x);

    WRITE_IMAGEF_SAFE(dynarg_dstTile, tileCoords, result);
}

// ----------------------------------------------------------------------------------------------------
__kernel void compositingNormalizeWithSampleCount(
    OUTPUT_BUFFER(0, float4, outputDirectLightingBuffer),   // dest buffer
    INPUT_BUFFER( 1, float4, outputIndirectLightingBuffer), // source buffer (named like this because of kernel asserts to work, but can be used not only for indirect)
    INPUT_BUFFER( 2, int,    indirectSampleCountBuffer),    // buffer with sample count
    INPUT_VALUE(  3, int2,   imageSize)
    KERNEL_VALIDATOR_BUFFERS_DEF)
{
    uint index = get_global_id(0) + get_global_id(1) * imageSize.y;

    const int sampleCount = INDEX_SAFE(indirectSampleCountBuffer, index);
    const float norm = sampleCount > 0 ? 1.0 / (float)sampleCount : 1.0;

    float4 color = INDEX_SAFE(outputIndirectLightingBuffer,index);

    INDEX_SAFE(outputDirectLightingBuffer, index) = color * norm;
}

// ----------------------------------------------------------------------------------------------------
__kernel void compositingNormalizeWithSampleCountAO(
    OUTPUT_BUFFER(0, float,  outputDirectLightingBuffer),
    INPUT_BUFFER( 1, float,  outputAoBuffer),
    INPUT_BUFFER( 2, int,    indirectSampleCountBuffer),
    INPUT_VALUE(  3, int2,   imageSize)
    KERNEL_VALIDATOR_BUFFERS_DEF)
{
    uint index = get_global_id(0) + get_global_id(1) * imageSize.y;

    const int sampleCount = INDEX_SAFE(indirectSampleCountBuffer, index);
    const float norm = sampleCount > 0 ? 1.0 / (float)sampleCount : 1.0;

    float color = INDEX_SAFE(outputAoBuffer,index);

    INDEX_SAFE(outputDirectLightingBuffer, index) = color * norm;
}

// ----------------------------------------------------------------------------------------------------
__kernel void compositingMultiplyWithSampleCount(
    OUTPUT_BUFFER(0, float4, outputDirectLightingBuffer),
    INPUT_BUFFER( 1, float4, outputIndirectLightingBuffer),
    INPUT_BUFFER( 2, int,    indirectSampleCountBuffer),
    INPUT_VALUE(  3, int2,   imageSize)
    KERNEL_VALIDATOR_BUFFERS_DEF)
{
    uint index = get_global_id(0) + get_global_id(1) * imageSize.y;

    const int sampleCount = INDEX_SAFE(indirectSampleCountBuffer, index);
    float4 color = INDEX_SAFE(outputIndirectLightingBuffer,index);

    INDEX_SAFE(outputDirectLightingBuffer, index) = color * (float)sampleCount;
}

// ----------------------------------------------------------------------------------------------------
__kernel void compositingMultiplyWithSampleCountAO(
    OUTPUT_BUFFER(0, float,  outputIndirectLightingBuffer),
    INPUT_BUFFER( 1, float,  outputAoBuffer),
    INPUT_BUFFER( 2, int,    indirectSampleCountBuffer),
    INPUT_VALUE(  3, int2,   imageSize)
    KERNEL_VALIDATOR_BUFFERS_DEF)
{
    uint index = get_global_id(0) + get_global_id(1) * imageSize.y;

    const int sampleCount = INDEX_SAFE(indirectSampleCountBuffer, index);
    float color = INDEX_SAFE(outputAoBuffer,index);

    INDEX_SAFE(outputIndirectLightingBuffer, index) = color * (float)sampleCount;
}

// ----------------------------------------------------------------------------------------------------
__kernel void compositingRGBMEncode(
    __write_only image2d_t      dynarg_dstTile,
    __read_only image2d_t       dynarg_srcTile,
    INPUT_VALUE(2, float,       rgbmRange),
    INPUT_VALUE(3, float,       lowerThreshold)
    KERNEL_VALIDATOR_BUFFERS_DEF)
{
    int2 tileThreadId = (int2)(get_global_id(0), get_global_id(1));

    float4 linearSpaceColor = READ_IMAGEF_SAFE(dynarg_srcTile, kSamplerClampNearestUnormCoords, tileThreadId);

    float4 rgbmValue = RGBMEncode(linearSpaceColor, rgbmRange, lowerThreshold);

    float4 gammaSpaceColor = LinearToGammaSpace01(rgbmValue);

    WRITE_IMAGEF_SAFE(dynarg_dstTile, tileThreadId, gammaSpaceColor);
}

// ----------------------------------------------------------------------------------------------------
__kernel void compositingDLDREncode(
    __write_only image2d_t  dynarg_dstTile,
    __read_only image2d_t   dynarg_srcTile
    KERNEL_VALIDATOR_BUFFERS_DEF)
{
    int2 tileThreadId = (int2)(get_global_id(0), get_global_id(1));

    float4 linearSpaceColor = READ_IMAGEF_SAFE(dynarg_srcTile, kSamplerClampNearestUnormCoords, tileThreadId);

    float4 gammaSpaceColor = (float4)(LinearToGammaSpace(linearSpaceColor.x), LinearToGammaSpace(linearSpaceColor.y), LinearToGammaSpace(linearSpaceColor.z), linearSpaceColor.w);

    gammaSpaceColor = min(gammaSpaceColor * 0.5f, kOne);

    WRITE_IMAGEF_SAFE(dynarg_dstTile, tileThreadId, gammaSpaceColor);
}

// ----------------------------------------------------------------------------------------------------
__kernel void compositingLinearToGamma(
    __write_only image2d_t      dynarg_dstTile,
    __read_only image2d_t       dynarg_srcTile
    KERNEL_VALIDATOR_BUFFERS_DEF)
{
    int2 tileThreadId = (int2)(get_global_id(0), get_global_id(1));

    float4 linearSpaceColor = READ_IMAGEF_SAFE(dynarg_srcTile, kSamplerClampNearestUnormCoords, tileThreadId);

    float4 gammaSpaceColor = (float4)(LinearToGammaSpace(linearSpaceColor.x), LinearToGammaSpace(linearSpaceColor.y), LinearToGammaSpace(linearSpaceColor.z), linearSpaceColor.w);

    WRITE_IMAGEF_SAFE(dynarg_dstTile, tileThreadId, gammaSpaceColor);
}

// ----------------------------------------------------------------------------------------------------
__kernel void compositingClampValues(
    __write_only image2d_t      dynarg_dstTile,
    __read_only image2d_t       dynarg_srcTile,
    INPUT_VALUE(2, float,       min),
    INPUT_VALUE(3, float,       max)
    KERNEL_VALIDATOR_BUFFERS_DEF)
{
    const float4 vMin = (float4)(min, min, min, min);
    const float4 vMax = (float4)(max, max, max, max);

    int2 tileThreadId = (int2)(get_global_id(0), get_global_id(1));

    float4 value = READ_IMAGEF_SAFE(dynarg_srcTile, kSamplerClampNearestUnormCoords, tileThreadId);

    value = clamp(value, vMin, vMax);

    WRITE_IMAGEF_SAFE(dynarg_dstTile, tileThreadId, value);
}

// ----------------------------------------------------------------------------------------------------
__kernel void compositingMultiplyImages(
    __write_only image2d_t      dynarg_dstTile,
    __read_only image2d_t       dynarg_srcTile0,
    __read_only image2d_t       dynarg_srcTile1
    KERNEL_VALIDATOR_BUFFERS_DEF)
{
    int2 tileThreadId = (int2)(get_global_id(0), get_global_id(1));

    float4 value1 = READ_IMAGEF_SAFE(dynarg_srcTile0, kSamplerClampNearestUnormCoords, tileThreadId);
    float4 value2 = READ_IMAGEF_SAFE(dynarg_srcTile1, kSamplerClampNearestUnormCoords, tileThreadId);

    float4 result = value1 * value2;

    WRITE_IMAGEF_SAFE(dynarg_dstTile, tileThreadId, result);
}

// ----------------------------------------------------------------------------------------------------
__kernel void compositingBlitTile(
    __write_only image2d_t      dynarg_dstTile,
    __read_only image2d_t       dynarg_srcTile,
    INPUT_VALUE(2, int2,        tileCoordinates)
    KERNEL_VALIDATOR_BUFFERS_DEF)
{
    int2 tileThreadId = (int2)(get_global_id(0), get_global_id(1));

    float4 value = READ_IMAGEF_SAFE(dynarg_srcTile, kSamplerClampNearestUnormCoords, tileThreadId);

    int2 lightmapCoords = tileThreadId + tileCoordinates;

    // write_imagef does appropriate data format conversion to the target image format
    WRITE_IMAGEF_SAFE(dynarg_dstTile, lightmapCoords, value);
}

// ----------------------------------------------------------------------------------------------------
// Filters horizontally or vertically depending on filterDirection - (1, 0) or (0, 1)
__kernel void compositingGaussFilter(
    __write_only image2d_t      dynarg_dstTile,
    __read_only image2d_t       dynarg_srcTile,
    INPUT_BUFFER(2, float,      dynarg_filterWeights),
    INPUT_BUFFER(3, int,        chartIndexBuffer),
    INPUT_VALUE( 4, int,        kernelWidth),
    INPUT_VALUE( 5, int2,       filterDirection),
    INPUT_VALUE( 6, int2,       halfKernelWidth),
    INPUT_VALUE( 7, int2,       tileCoordinates),
    INPUT_VALUE( 8, int,        lightmapSize)
    KERNEL_VALIDATOR_BUFFERS_DEF)
{
    // Coordinates in tile space
    int2 tileThreadId = (int2)(get_global_id(0), get_global_id(1));

    // Coordinates in lightmap space
    int2 lightmapCoords = tileThreadId + tileCoordinates;

    int centerChartId = ReadChartId(chartIndexBuffer, lightmapSize, lightmapCoords KERNEL_VALIDATOR_BUFFERS);

    float4 centerValue = READ_IMAGEF_SAFE(dynarg_srcTile, kSamplerClampNearestUnormCoords, tileThreadId);

    if (centerChartId == -1 || centerValue.w == 0.0f)
        return;

    float4 filtered     = kZero;
    float  weightSum    = 0.0f;
    float  weightCount  = 0.0f;

    int2 startOffset = tileThreadId - halfKernelWidth * filterDirection;
    for (int s = 0; s < kernelWidth; s++)
    {
        int2    sampleCoords    = startOffset + s * filterDirection;

        // Note: not using READ_IMAGEF_SAFE below as those samples are expected to read just outside of the tile boundary, they will get safely clamped though.
        // The srcTile and dstTile are of the same size, so iterating over dstTile texels means trying to sample by halfKernelWidth outside of srcTile at the edges.
        // We are using a separable Gaussian blur, first the vertical one and then the horizontal. The second pass depends on being able to read the results
        // stored in the border area from the first pass. Since we simply swap srcTile and dstTile, it's the easiest to keep them of the same size instead of doing
        // the tileSize vs expanded tileSize logic.

        float4  sampleValue     = read_imagef(dynarg_srcTile, kSamplerClampNearestUnormCoords, sampleCoords);
        int     sampleChartId   = ReadChartId(chartIndexBuffer, lightmapSize, sampleCoords + tileCoordinates KERNEL_VALIDATOR_BUFFERS);

        float weight = sampleValue.w * INDEX_SAFE(dynarg_filterWeights, s);

        weight *= sampleChartId == centerChartId ? 1.0f : 0.0f;

        weightSum   += weight;
        weightCount += sampleValue.w;
        filtered    += weight * sampleValue;
    }

    filtered *= 1.0f / lerp1(1.0f, weightSum, clamp(weightCount, 0.0f, 1.0f));
    filtered.w = 1.0f;

    WRITE_IMAGEF_SAFE(dynarg_dstTile, tileThreadId, filtered);
}
