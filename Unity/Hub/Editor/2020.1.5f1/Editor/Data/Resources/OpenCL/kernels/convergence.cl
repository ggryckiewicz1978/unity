#include "commonCL.h"

__constant ConvergenceOutputData g_clearedConvergenceOutputData = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, INT_MAX, INT_MAX, INT_MAX, INT_MIN, INT_MIN, INT_MIN};

__kernel void clearConvergenceData(
    OUTPUT_BUFFER(00, ConvergenceOutputData, convergenceOutputDataBuffer)
    KERNEL_VALIDATOR_BUFFERS_DEF
)
{
    INDEX_SAFE(convergenceOutputDataBuffer, 0) = g_clearedConvergenceOutputData;
}

__attribute__((reqd_work_group_size(CONVERGENCE_WORKGROUPSIZE, 1, 1)))
__kernel void calculateConvergenceMap(
    INPUT_BUFFER( 00, float4,                positionsWSBuffer),
    INPUT_BUFFER( 01, unsigned char,         cullingMapBuffer),
    INPUT_BUFFER( 02, int,                   directSampleCountBuffer),
    INPUT_BUFFER( 03, int,                   indirectSampleCountBuffer),
    INPUT_BUFFER( 04, int,                   environmentSampleCountBuffer),
    INPUT_VALUE(  05, int,                   maxDirectSamplesPerPixel),
    INPUT_VALUE(  06, int,                   maxGISamplesPerPixel),
    INPUT_VALUE(  07, int,                   maxEnvSamplesPerPixel),
    INPUT_BUFFER( 08, unsigned char,         occupancyBuffer),
    INPUT_VALUE(  09, int,                   occupiedTexelCount),
    OUTPUT_BUFFER(10, ConvergenceOutputData, convergenceOutputDataBuffer) //Should be cleared properly before kernel is running
    KERNEL_VALIDATOR_BUFFERS_DEF
)
{
    __local ConvergenceOutputData dataShared;
    __local unsigned int totalDirectSamples;
    __local unsigned int totalGISamples;
    __local unsigned int totalEnvSamples;

    int idx = get_global_id(0);

    if (get_local_id(0) == 0)
    {
        dataShared = g_clearedConvergenceOutputData;
        totalDirectSamples = 0;
        totalGISamples = 0;
        totalEnvSamples = 0;
    }


    barrier(CLK_LOCAL_MEM_FENCE);

    const int occupiedSamplesWithinTexel = INDEX_SAFE(occupancyBuffer, idx);

    if (occupiedSamplesWithinTexel != 0)
    {
        const bool isTexelVisible = !IsCulled(INDEX_SAFE(cullingMapBuffer, idx));
        if (isTexelVisible)
            atomic_inc(&(dataShared.visibleTexelCount));

        const int directSampleCount = INDEX_SAFE(directSampleCountBuffer, idx);
        atomic_min(&(dataShared.minDirectSamples), directSampleCount);
        atomic_max(&(dataShared.maxDirectSamples), directSampleCount);
        atomic_add(&totalDirectSamples, directSampleCount);

        const int giSampleCount = INDEX_SAFE(indirectSampleCountBuffer, idx);
        atomic_min(&(dataShared.minGISamples), giSampleCount);
        atomic_max(&(dataShared.maxGISamples), giSampleCount);
        atomic_add(&totalGISamples, giSampleCount);

        const int envSampleCount = INDEX_SAFE(environmentSampleCountBuffer, idx);
        atomic_min(&(dataShared.minEnvSamples), envSampleCount);
        atomic_max(&(dataShared.maxEnvSamples), envSampleCount);
        atomic_add(&totalEnvSamples, envSampleCount);

        if (IsGIConverged(giSampleCount, maxGISamplesPerPixel))
        {
            atomic_inc(&(dataShared.convergedGITexelCount));

            if (isTexelVisible)
                atomic_inc(&(dataShared.visibleConvergedGITexelCount));
        }

        if (IsDirectConverged(directSampleCount, maxDirectSamplesPerPixel))
        {
            atomic_inc(&(dataShared.convergedDirectTexelCount));

            if (isTexelVisible)
                atomic_inc(&(dataShared.visibleConvergedDirectTexelCount));
        }

        if (IsEnvironmentConverged(envSampleCount, maxEnvSamplesPerPixel))
        {
            atomic_inc(&(dataShared.convergedEnvTexelCount));

            if (isTexelVisible)
                atomic_inc(&(dataShared.visibleConvergedEnvTexelCount));
        }

    }

    barrier(CLK_LOCAL_MEM_FENCE);
    if (get_local_id(0) == 0)
    {
        const float maxTotalDirectSamples = (float)occupiedTexelCount * (float)maxDirectSamplesPerPixel;
        const float maxTotalGISamples     = (float)occupiedTexelCount * (float)maxGISamplesPerPixel;
        const float maxTotalEnvSamples    = (float)occupiedTexelCount * (float)maxEnvSamplesPerPixel;
        const unsigned int averageDirectSamplesRatio = (int)ceil(CONVERGENCE_AVERAGES_PRECISION * (float)totalDirectSamples / maxTotalDirectSamples);
        const unsigned int averageGISamplesRatio     = (int)ceil(CONVERGENCE_AVERAGES_PRECISION * (float)totalGISamples     / maxTotalGISamples);
        const unsigned int averageEnvSamplesRatio    = (int)ceil(CONVERGENCE_AVERAGES_PRECISION * (float)totalEnvSamples    / maxTotalEnvSamples);

        atomic_add(&(INDEX_SAFE(convergenceOutputDataBuffer, 0).visibleTexelCount), dataShared.visibleTexelCount);
        atomic_min(&(INDEX_SAFE(convergenceOutputDataBuffer, 0).minDirectSamples), dataShared.minDirectSamples);
        atomic_max(&(INDEX_SAFE(convergenceOutputDataBuffer, 0).maxDirectSamples), dataShared.maxDirectSamples);
        atomic_add(&(INDEX_SAFE(convergenceOutputDataBuffer, 0).averageDirectSamplesPercent), averageDirectSamplesRatio);
        atomic_min(&(INDEX_SAFE(convergenceOutputDataBuffer, 0).minGISamples), dataShared.minGISamples);
        atomic_max(&(INDEX_SAFE(convergenceOutputDataBuffer, 0).maxGISamples), dataShared.maxGISamples);
        atomic_add(&(INDEX_SAFE(convergenceOutputDataBuffer, 0).averageGISamplesPercent), averageGISamplesRatio);
        atomic_min(&(INDEX_SAFE(convergenceOutputDataBuffer, 0).minEnvSamples), dataShared.minEnvSamples);
        atomic_max(&(INDEX_SAFE(convergenceOutputDataBuffer, 0).maxEnvSamples), dataShared.maxEnvSamples);
        atomic_add(&(INDEX_SAFE(convergenceOutputDataBuffer, 0).averageEnvSamplesPercent), averageEnvSamplesRatio);
        atomic_add(&(INDEX_SAFE(convergenceOutputDataBuffer, 0).convergedGITexelCount), dataShared.convergedGITexelCount);
        atomic_add(&(INDEX_SAFE(convergenceOutputDataBuffer, 0).visibleConvergedGITexelCount), dataShared.visibleConvergedGITexelCount);
        atomic_add(&(INDEX_SAFE(convergenceOutputDataBuffer, 0).convergedDirectTexelCount), dataShared.convergedDirectTexelCount);
        atomic_add(&(INDEX_SAFE(convergenceOutputDataBuffer, 0).visibleConvergedDirectTexelCount), dataShared.visibleConvergedDirectTexelCount);
        atomic_add(&(INDEX_SAFE(convergenceOutputDataBuffer, 0).convergedEnvTexelCount), dataShared.convergedEnvTexelCount);
        atomic_add(&(INDEX_SAFE(convergenceOutputDataBuffer, 0).visibleConvergedEnvTexelCount), dataShared.visibleConvergedEnvTexelCount);
    }
}
