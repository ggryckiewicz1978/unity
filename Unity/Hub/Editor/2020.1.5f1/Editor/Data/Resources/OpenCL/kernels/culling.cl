#include "commonCL.h"

__kernel void clearLightmapCulling(
    OUTPUT_BUFFER(00, unsigned char, cullingMapBuffer)
    KERNEL_VALIDATOR_BUFFERS_DEF
)
{
    int idx = get_global_id(0);
    INDEX_SAFE(cullingMapBuffer, idx) = 255;
}

__kernel void prepareLightmapCulling(
    INPUT_BUFFER( 00, unsigned char,       occupancyBuffer),
    INPUT_BUFFER( 01, float4,              positionsWSBuffer),
    INPUT_BUFFER( 02, PackedNormalOctQuad, interpNormalsWSBuffer),
    INPUT_VALUE(  03, Matrix4x4,           worldToClip),
    INPUT_VALUE(  04, float4,              cameraPosition),
    INPUT_VALUE(  05, int,                 superSamplingMultiplier),
    OUTPUT_BUFFER(06, ray,                 lightRaysCompactedBuffer),
    OUTPUT_BUFFER(07, uint,                lightRaysCountBuffer),
    INPUT_BUFFER( 08, uint,                instanceIdToLodInfoBuffer)
    KERNEL_VALIDATOR_BUFFERS_DEF
)
{
    ray r;  // prepare ray in private memory
    int idx = get_global_id(0);

    __local int numRayPreparedSharedMem;
    if (get_local_id(0) == 0)
        numRayPreparedSharedMem = 0;
    barrier(CLK_LOCAL_MEM_FENCE);
    atomic_inc(&numRayPreparedSharedMem);

    //TODO(RadeonRays) on spot compaction (guillaume v1 style)

    const int occupiedSamplesWithinTexel = INDEX_SAFE(occupancyBuffer, idx);
    if (occupiedSamplesWithinTexel == 0) // Reject texels that are invalid.
    {
        Ray_SetInactive(&r);
    }
    else
    {
        // Just fetch one sample, we know the position is within an occupied texel.
        int ssIdx = idx * superSamplingMultiplier * superSamplingMultiplier;
        float4 position = INDEX_SAFE(positionsWSBuffer, ssIdx);

        //Clip space position
        float4 clipPos = transform_point(position.xyz, worldToClip);
        clipPos.xyz /= clipPos.w;

        //Camera to texel
        //float3 camToPos = (position.xyz - cameraPosition.xyz);

        //Normal
        float3 normal = CalculateSuperSampledInterpolatedNormal(idx, superSamplingMultiplier, interpNormalsWSBuffer KERNEL_VALIDATOR_BUFFERS);
        //float normalDotCamToPos = dot(normal, camToPos);

        //Is the texel visible?
        if (clipPos.x >= -1.0f && clipPos.x <= 1.0f &&
            clipPos.y >= -1.0f && clipPos.y <= 1.0f &&
            clipPos.z >= 0.0f && clipPos.z <= 1.0f)
            //TODO(RadeonRays) understand why this does not work.
            //&& normalDotCamToPos < 0.0f)
        {
            const float kMinPushOffDistance = 0.001f;
            float3 targetPos = position.xyz + normal * kMinPushOffDistance;
            float3 camToTarget = (targetPos - cameraPosition.xyz);
            float camToTargetDist = length(camToTarget);
            if (camToTargetDist > 0)
            {
                const int instanceId = (int)(floor(position.w));
                const int instanceLodInfo = INDEX_SAFE(instanceIdToLodInfoBuffer, instanceId);
                Ray_Init(&r, cameraPosition.xyz, camToTarget/ camToTargetDist, camToTargetDist, 0.f, instanceLodInfo);
            }
            else
            {
                Ray_SetInactive(&r);
            }
        }
        else
        {
            Ray_SetInactive(&r);
        }
    }

    barrier(CLK_LOCAL_MEM_FENCE);
    if (get_local_id(0) == 0)
    {
        atomic_add(GET_PTR_SAFE(lightRaysCountBuffer, 0), numRayPreparedSharedMem);
    }
    INDEX_SAFE(lightRaysCompactedBuffer, idx) = r;
}

__kernel void processLightmapCulling(
    INPUT_BUFFER( 00, ray,           lightRaysCompactedBuffer),
    INPUT_BUFFER( 01, float4,        lightOcclusionCompactedBuffer),
    OUTPUT_BUFFER(02, unsigned char, cullingMapBuffer),
    OUTPUT_BUFFER(03, unsigned int,  visibleTexelCountBuffer) //Need to have been cleared to 0 before the kernel is called.
    KERNEL_VALIDATOR_BUFFERS_DEF
)
{
    __local int visibleTexelCountSharedMem;
    int idx = get_global_id(0);
    if (get_local_id(0) == 0)
        visibleTexelCountSharedMem = 0;
    barrier(CLK_LOCAL_MEM_FENCE);

    const bool rayActive = !Ray_IsInactive(GET_PTR_SAFE(lightRaysCompactedBuffer, idx));//TODO(RadeonRays) on spot compaction (guillaume v1 style) see same comment above
    const bool hit = rayActive && INDEX_SAFE(lightOcclusionCompactedBuffer, idx).w < TRANSMISSION_THRESHOLD;
    const bool texelVisible = rayActive && !hit;

    if (texelVisible)
    {
        INDEX_SAFE(cullingMapBuffer, idx) = 255;
    }
    else
    {
        INDEX_SAFE(cullingMapBuffer, idx) = 0;
    }

    // nvidia+macOS hack (atomic operation in the if above break the write to cullingMapBuffer!).
    int intTexelVisible = texelVisible?1:0;
    atomic_add(&visibleTexelCountSharedMem,intTexelVisible);

    barrier(CLK_LOCAL_MEM_FENCE);
    if (get_local_id(0) == 0)
        atomic_add(GET_PTR_SAFE(visibleTexelCountBuffer, 0), visibleTexelCountSharedMem);
}
