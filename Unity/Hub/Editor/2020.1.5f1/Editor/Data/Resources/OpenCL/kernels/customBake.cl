#include "commonCL.h"

__kernel void prepareCustomBake(
    OUTPUT_BUFFER(00, ray, pathRaysCompactedBuffer_0),
    OUTPUT_BUFFER(01, uint, activePathCountBuffer_0),
    OUTPUT_BUFFER(02, uint, totalRaysCastCountBuffer),
    OUTPUT_BUFFER(03, float4, originalRaysExpandedBuffer),
    INPUT_BUFFER( 04, float4, positionsWSBuffer),
    INPUT_BUFFER( 05, uint, sobol_buffer),
    INPUT_BUFFER( 06, float, goldenSample_buffer),
    INPUT_VALUE(  07, int, numGoldenSample),
    INPUT_VALUE(  08, int, fakedLightmapResolution), // position count
    INPUT_BUFFER( 09, SampleDescription, sampleDescriptionsExpandedBuffer),
    INPUT_BUFFER( 10, uint, sampleDescriptionsExpandedCountBuffer)
    KERNEL_VALIDATOR_BUFFERS_DEF
)
{
    __local uint numRayPreparedSharedMem;
    if (get_local_id(0) == 0)
        numRayPreparedSharedMem = 0;
    barrier(CLK_LOCAL_MEM_FENCE);

    // Prepare ray in private memory
    ray r;
    Ray_SetInactive(&r);

    int expandedPathRayIdx = get_global_id(0), local_idx;
    const uint sampleDescriptionsExpandedCount = INDEX_SAFE(sampleDescriptionsExpandedCountBuffer, 0);
    if (expandedPathRayIdx < sampleDescriptionsExpandedCount)
    {
        const SampleDescription sampleDescription = INDEX_SAFE(sampleDescriptionsExpandedBuffer, expandedPathRayIdx);
#if DISALLOW_RAY_EXPANSION
        if (sampleDescription.texelIndex >= 0)
        {
#endif
            const int ssIdx = sampleDescription.texelIndex;
            const float4 position = INDEX_SAFE(positionsWSBuffer, ssIdx);
            const float rayOffset = position.w;

            // Skip unused texels.
            if (rayOffset >= 0.0)
            {
                AssertPositionIsOccupied(position KERNEL_VALIDATOR_BUFFERS);

                // Get random numbers
                float3 sample3D;
                sample3D.x = SobolSample(sampleDescription.currentSampleCount, 0, sobol_buffer KERNEL_VALIDATOR_BUFFERS);
                sample3D.y = SobolSample(sampleDescription.currentSampleCount, 1, sobol_buffer KERNEL_VALIDATOR_BUFFERS);
                sample3D.z = SobolSample(sampleDescription.currentSampleCount, 2, sobol_buffer KERNEL_VALIDATOR_BUFFERS);

                int texel_x = sampleDescription.texelIndex % fakedLightmapResolution;
                int texel_y = sampleDescription.texelIndex / fakedLightmapResolution;
                sample3D = ApplyCranleyPattersonRotation3D(sample3D, texel_x, texel_y, fakedLightmapResolution, 0, goldenSample_buffer KERNEL_VALIDATOR_BUFFERS);

                // We don't want the full sphere, we only want the upper hemisphere.
                float3 direction = SphereSample(sample3D.xy);
                if (direction.y < 0.0f)
                    direction = make_float3(direction.x, -direction.y, direction.z);

                const float randOffset = 0.1f * rayOffset + 0.9f * rayOffset * sample3D.z;
                const float3 origin = position.xyz + direction * randOffset;
                const float kMaxt = 1000000.0f;
                const int instanceLodInfo = PackLODInfo(NO_LOD_MASK, NO_LOD_GROUP);
                Ray_Init(&r, origin, direction, kMaxt, 0.f, instanceLodInfo);

                // Set the index so we can map to the originating texel/probe
                Ray_SetSourceIndex(&r, sampleDescription.texelIndex);
            }
#if DISALLOW_RAY_EXPANSION
        }
#endif
    }

    // Threads synchronization for compaction
    if (Ray_IsActive_Private(&r))
    {
        local_idx = atomic_inc(&numRayPreparedSharedMem);
    }
    barrier(CLK_LOCAL_MEM_FENCE);
    if (get_local_id(0) == 0)
    {
        atomic_add(GET_PTR_SAFE(totalRaysCastCountBuffer, 0), numRayPreparedSharedMem);
        int numRayToAdd = numRayPreparedSharedMem;
#if DISALLOW_PATH_RAYS_COMPACTION
        numRayToAdd = get_local_size(0);
#endif
        numRayPreparedSharedMem = atomic_add(GET_PTR_SAFE(activePathCountBuffer_0, 0), numRayToAdd);
    }
    barrier(CLK_LOCAL_MEM_FENCE);

    int compactedPathRayIndex = numRayPreparedSharedMem + local_idx;
#if DISALLOW_PATH_RAYS_COMPACTION
    compactedPathRayIndex = expandedPathRayIdx;
#else
    // Write the ray out to memory
    if (Ray_IsActive_Private(&r))
#endif
    {
        INDEX_SAFE(originalRaysExpandedBuffer, expandedPathRayIdx) = (float4)(r.d.x, r.d.y, r.d.z, 0);
        Ray_SetSampleDescriptionIndex(&r, expandedPathRayIdx);
        INDEX_SAFE(pathRaysCompactedBuffer_0, compactedPathRayIndex) = r;
    }
}

__kernel void processCustomBake(
    //*** input ***
    INPUT_BUFFER( 00, ray,    pathRaysCompactedBuffer_0),
    INPUT_BUFFER( 01, uint,   activePathCountBuffer_0),
    INPUT_BUFFER( 02, float4, pathThroughputExpandedBuffer),
    INPUT_VALUE(  03, int,    totalSampleCount),
    INPUT_BUFFER( 04, float4, shadowmaskExpandedBuffer), //Used to store validity in .y
    INPUT_BUFFER( 05, Intersection, pathIntersectionsCompactedBuffer),
    //*** output ***
    OUTPUT_BUFFER(06, float4, probeOcclusionExpandedBuffer)
    KERNEL_VALIDATOR_BUFFERS_DEF)
{
    const uint compactedRayIdx = get_global_id(0);
    if (compactedRayIdx >= INDEX_SAFE(activePathCountBuffer_0, 0))
        return;

#if DISALLOW_LIGHT_RAYS_COMPACTION
    if (Ray_IsInactive(GET_PTR_SAFE(lightRaysCompactedBuffer, compactedRayIdx)))
        return;
#endif

    KERNEL_ASSERT(Ray_IsActive(GET_PTR_SAFE(pathRaysCompactedBuffer_0, compactedRayIdx)));
    const int sampleDescriptionIdx = Ray_GetSampleDescriptionIndex(GET_PTR_SAFE(pathRaysCompactedBuffer_0, compactedRayIdx));

    const bool pathRayHitSomething = INDEX_SAFE(pathIntersectionsCompactedBuffer, compactedRayIdx).shapeid > 0;
    const float4 throughput = INDEX_SAFE(pathThroughputExpandedBuffer, sampleDescriptionIdx);
    float3 color = throughput.xyz;

    if (pathRayHitSomething)
        color = make_float3(0,0,0);

    // accumulate sky occlusion.
    const float backfacing = INDEX_SAFE(shadowmaskExpandedBuffer, sampleDescriptionIdx).y;
    INDEX_SAFE(probeOcclusionExpandedBuffer, sampleDescriptionIdx) += make_float4(color.x, color.y, color.z, backfacing);
    KERNEL_ASSERT(totalSampleCount > 0);
}
