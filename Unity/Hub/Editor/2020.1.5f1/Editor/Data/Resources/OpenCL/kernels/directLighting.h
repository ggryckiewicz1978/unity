#ifndef DIRECT_LIGHTING_H
#define DIRECT_LIGHTING_H

#include "commonCL.h"

CookieHeader LoadCookieHeaderWithTiling(const uint cookieIndex, bool* tileCookie, __global int const* cookiesBuffer KERNEL_VALIDATOR_BUFFERS_DEF)
{
    int width  = cookiesBuffer[cookieIndex * (sizeof(CookieHeader) / sizeof(int)) + 0];
    int height = cookiesBuffer[cookieIndex * (sizeof(CookieHeader) / sizeof(int)) + 1];
    int offset = cookiesBuffer[cookieIndex * (sizeof(CookieHeader) / sizeof(int)) + 2];
    *tileCookie = width < 0;
    CookieHeader ch = { abs(width), height, offset };
    return ch;
}

// helper, as tiling info is only relevant to directional lights
CookieHeader LoadCookieHeader(const uint cookieIndex, __global int const* cookiesBuffer KERNEL_VALIDATOR_BUFFERS_DEF)
{
    bool tileCookie;
    return LoadCookieHeaderWithTiling(cookieIndex, &tileCookie, cookiesBuffer KERNEL_VALIDATOR_BUFFERS);
}

float3 LoadCookieData(int offset, __global int const* cookiesBuffer KERNEL_VALIDATOR_BUFFERS_DEF)
{
    int rawval = cookiesBuffer[offset];
    float a = ((rawval >> 24) & 0xff) / 255.f;
    float b = ((rawval >> 16) & 0xff) / 255.f;
    float g = ((rawval >>  8) & 0xff) / 255.f;
    float r = ((rawval >>  0) & 0xff) / 255.f;
    return (float3)(r, g, b);
}

float3 GetCookieAttenuation(const CookieHeader ch, bool tileCookie, int slice, float2 uvs, __global int const* cookiesBuffer KERNEL_VALIDATOR_BUFFERS_DEF)
{
    // bilinear texfetch
    float2 fres       = { (float)(ch.width), (float)(ch.height) };
    float2 scaled_uvs = uvs * fres;
    int2   res        = (int2)(ch.width, ch.height);
    int2   xy00       = (int2)(scaled_uvs.x, scaled_uvs.y);
    int2   xy10       = xy00 + (int2)(1, 0);
    int2   xy01       = xy00 + (int2)(0, 1);
    int2   xy11       = xy00 + (int2)(1, 1);

    int2 res1 = res - (int2)(1, 1);
    if (tileCookie)
    {
        xy00 = xy00 >= 0 ? (xy00 % res) : (res1 - (-xy00 % res));
        xy10 = xy10 >= 0 ? (xy10 % res) : (res1 - (-xy10 % res));
        xy01 = xy01 >= 0 ? (xy01 % res) : (res1 - (-xy01 % res));
        xy11 = xy11 >= 0 ? (xy11 % res) : (res1 - (-xy11 % res));
    }
    else
    {
        xy00 = clamp(xy00, (int2)(0, 0), res1);
        xy10 = clamp(xy10, (int2)(0, 0), res1);
        xy01 = clamp(xy01, (int2)(0, 0), res1);
        xy11 = clamp(xy11, (int2)(0, 0), res1);
    }

    int offset = ch.offset + ch.width * ch.height * slice;

    float3 col00 = LoadCookieData(offset + xy00.y * ch.width + xy00.x, cookiesBuffer KERNEL_VALIDATOR_BUFFERS);
    float3 col10 = LoadCookieData(offset + xy10.y * ch.width + xy10.x, cookiesBuffer KERNEL_VALIDATOR_BUFFERS);
    float3 col01 = LoadCookieData(offset + xy01.y * ch.width + xy01.x, cookiesBuffer KERNEL_VALIDATOR_BUFFERS);
    float3 col11 = LoadCookieData(offset + xy11.y * ch.width + xy11.x, cookiesBuffer KERNEL_VALIDATOR_BUFFERS);

    float2 fract_out;
    float2 bilin = fract(scaled_uvs, &fract_out);

    return mix(mix(col00, col10, bilin.x), mix(col01, col11, bilin.x), bilin.y);
}

float3 GetCubemapCookieAttenuation(LightBuffer light, float3 dir, __global int const* cookiesBuffer KERNEL_VALIDATOR_BUFFERS_DEF)
{
    float3   e0 = (float3)(light.dataUnion.pointLightData.tanX  , light.dataUnion.pointLightData.tanY  , light.dataUnion.pointLightData.tanZ);
    float3   e1 = (float3)(light.dataUnion.pointLightData.bitanX, light.dataUnion.pointLightData.bitanY, light.dataUnion.pointLightData.bitanZ);
    float3   e2 = (float3)(light.dir.x, light.dir.y, light.dir.z);

    // rotate light vector into cubemap frame (transposing here instead of inverse, as rot is orthonormal)
    dir = (float3)(dot(dir, e0), dot(dir, e1), dot(dir, e2));

    // find slice
    int slice = 0;

    float2 uvs;
    float3 absdir = fabs(dir);
    if (absdir.x >= absdir.y && absdir.x >= absdir.z)
    {
        slice = dir.x >= 0.0f ? 0 : 1;
        uvs = (float2)(-dir.z / dir.x, -dir.y / absdir.x);
    }
    else if (absdir.y >= absdir.z)
    {
        slice = dir.y >= 0.0f ? 2 : 3;
        uvs = (float2)(dir.x / absdir.y, dir.z / dir.y);
    }
    else
    {
        slice = dir.z >= 0.0f ? 4 : 5;
        uvs = (float2)(dir.x / dir.z, -dir.y / absdir.z);
    }

    uvs = uvs * 0.5f + 0.5f;

    // pull header info first
    CookieHeader ch = LoadCookieHeader(light.dataUnion.pointLightData.cookieIndex, cookiesBuffer KERNEL_VALIDATOR_BUFFERS);
    return GetCookieAttenuation(ch, false, slice, uvs, cookiesBuffer KERNEL_VALIDATOR_BUFFERS);
}

float3 GetJitteredLightVec(float shadowRadius, float2 rnd, float3 lightOffset, Matrix4x4 lightBasis)
{
    float2 diskSample = MapSquareToDisk(rnd);
    float3 jitterOffset = (float3)(shadowRadius * diskSample.x, shadowRadius * diskSample.y, 0.0f);
    jitterOffset = transform_vector(jitterOffset, lightBasis);
    float3 jitteredLightOffset = lightOffset + jitterOffset;
    return normalize(jitteredLightOffset);
}

static float3 CalculateJitteredLightVec(bool forceHardShadow, LightBuffer light, float3 lightVec, float maxT, float2 rnd)
{
    int lightType = light.lightType;

    bool shouldSkipJittering = (forceHardShadow || lightType == kPVRLightRectangle || lightType == kPVRLightDisc || lightType == kPVRLightBox);
    if (shouldSkipJittering)
        return lightVec;

    float lightDist = (lightType == kPVRLightDirectional) ? 1.0f : maxT;
    float3 lightOffset = lightVec * lightDist;
    float shadowRadius = light.pos.w;

    // Construct basis
    float3 b1;
    float3 b2;
    CreateOrthoNormalBasis(lightVec, &b1, &b2);

    Matrix4x4 lightBasis;
    lightBasis.m0.xyz = b1;
    lightBasis.m1.xyz = b2;
    lightBasis.m2.xyz = lightVec;
    return GetJitteredLightVec(shadowRadius, rnd, lightOffset, lightBasis);
}

static void PrepareShadowRay(
    const LightBuffer       light,
    const float2            sample2D,
    const float3            surfacePosition,
    const float3            surfaceNormal,
    float                   pushOff,
    bool                    forceHardShadow,
    __private ray*          dstRay,
    const int               rayLodInfo
)
{
#ifdef PROBES
    const float3 pushOffPos = surfacePosition;
#else
    const float3 pushOffPos = surfacePosition + surfaceNormal * pushOff;
#endif

    // Limit ray length to remove length checking in process (for all but kPVRLightDirectional)
    const float lightRange = light.dir.w;
    const float maxt = (light.lightType == kPVRLightDirectional) ? DEFAULT_RAY_LENGTH : length(light.pos.xyz - pushOffPos);

    if (light.lightType != kPVRLightDirectional && light.lightType != kPVRLightBox)
    {
        if (maxt > lightRange)
        {
            Ray_SetInactive(dstRay);
            return;
        }

#ifndef PROBES
        const float surfToLightDist = length(light.pos.xyz - surfacePosition);
        if (pushOff > surfToLightDist)
        {
            Ray_SetInactive(dstRay);
            return;
        }
#endif
    }

    if (light.lightType == kPVRLightSpot)
    {
        // Shoot rays against spot light cone.
        float3 rayDir = normalize(light.pos.xyz - surfacePosition);
        float nDotL = 1.0f;
        const float cosConeAng = (light.dataUnion.spotLightData.angularFalloffType != kAngularFalloffTypeLUT || light.dataUnion.spotLightData.cookieIndex < 0) ?
            light.dataUnion.spotLightData.cosineConeAngle : light.dataUnion.spotLightData.cosineOuterFrustumAngle;
        float dval = dot(rayDir, -light.dir.xyz);
        if (dval < cosConeAng)
        {
            Ray_SetInactive(dstRay);
        }
        else
        {
#ifndef PROBES
            nDotL = IsNormalValid(surfaceNormal) ? dot(rayDir, surfaceNormal) : 1.0f;
            bool shouldCancelRay = nDotL < 0.0f || any(isnan(nDotL));
            if (shouldCancelRay)
            {
                Ray_SetInactive(dstRay);
            }
            else
#endif
            {
                rayDir = CalculateJitteredLightVec(forceHardShadow, light, rayDir, maxt, sample2D);
                Ray_Init(dstRay, pushOffPos, rayDir, maxt, nDotL, rayLodInfo);
            }
        }
    }
    else if (light.lightType == kPVRLightDirectional)
    {
        float3 rayDir = -normalize(light.dir.xyz);
        float nDotL = 1.0f;
#ifndef PROBES
        nDotL = IsNormalValid(surfaceNormal) ? dot(rayDir, surfaceNormal) : 1.0f;
        bool shouldCancelRay = nDotL < 0.0f || any(isnan(nDotL));
        if (shouldCancelRay)
        {
            Ray_SetInactive(dstRay);
        }
        else
#endif
        {
            rayDir = CalculateJitteredLightVec(forceHardShadow, light, rayDir, maxt, sample2D);
            Ray_Init(dstRay, pushOffPos, rayDir, maxt, nDotL, rayLodInfo);
        }
    }
    else if (light.lightType == kPVRLightPoint)
    {
        float3 rayDir = normalize(light.pos.xyz - surfacePosition);
        float nDotL = 1.0f;
#ifndef PROBES
        nDotL = IsNormalValid(surfaceNormal) ? dot(rayDir, surfaceNormal) : 1.0f;
        bool shouldCancelRay = nDotL < 0.0f || any(isnan(nDotL));
        if (shouldCancelRay)
        {
            Ray_SetInactive(dstRay);
        }
        else
#endif
        {
            rayDir = CalculateJitteredLightVec(forceHardShadow, light, rayDir, maxt, sample2D);
            Ray_Init(dstRay, pushOffPos, rayDir, maxt, nDotL, rayLodInfo);
        }
    }
    else if (light.lightType == kPVRLightRectangle)
    {
        const float3 lightDir = normalize(light.dir.xyz);
        const float3 lightPos = light.pos.xyz;
        // light backfacing ?
        if (dot(lightDir, surfacePosition.xyz - lightPos) < 0.f)
        {
            Ray_SetInactive(dstRay);
        }
        else
        {
            const float width = light.dataUnion.areaLightData.areaWidth;
            const float height = light.dataUnion.areaLightData.areaHeight;
            const float2 sq = sample2D;
            const float3 lightTan = light.dataUnion.areaLightData.Tangent.xyz;
            const float3 lightBitan = light.dataUnion.areaLightData.Bitangent.xyz;
            const float3 s = lightPos - (.5f * width * lightBitan) - (.5f * height * lightTan);
            float solidAngle;
            float nDotL = 1.0f;
            float3 rayDir = SphQuadSample(s, lightTan * height, lightBitan * width, pushOffPos, sq.x, sq.y, &solidAngle) - pushOffPos;

            if (isnan(solidAngle) || solidAngle <= 0.0f)
            {
                Ray_SetInactive(dstRay);
            }
            else
            {
                const float maxT = length(rayDir);
                rayDir /= maxT;
#ifndef PROBES
                // light ray backfacing ?
                nDotL = IsNormalValid(surfaceNormal) ? dot(rayDir, surfaceNormal) : 1.0f;
                bool shouldCancelRay = nDotL < 0.0f || any(isnan(nDotL));
                if (shouldCancelRay)
                {
                    Ray_SetInactive(dstRay);
                }
                else
#endif
                {
                    Ray_Init(dstRay, pushOffPos, rayDir, maxT, solidAngle * nDotL, rayLodInfo);
                }
            }
        }
    }
    else if (light.lightType == kPVRLightDisc)
    {
        const float3 lightDir = normalize(light.dir.xyz);
        const float3 lightPos = light.pos.xyz;

        // is light backfacing?
        if (dot(lightDir, surfacePosition.xyz - lightPos) < 0.f)
        {
            Ray_SetInactive(dstRay);
        }
        else
        {
            // Sample uniformly on 2d disc area
            const float radius = light.dataUnion.discLightData.radius;
            float2 sq = sample2D;
            float rLocal = sqrt(sq.x);
            float thetaLocal = 2.0f * FLT_PI * sq.y;
            float2 samplePointLocal = make_float2(cos(thetaLocal), sin(thetaLocal)) * rLocal * radius;

            // Convert sample point to world space
            float3 lightTan = -normalize(light.dataUnion.discLightData.Tangent.xyz);
            float3 lineCross = cross(lightDir, lightTan);
            float3 samplePointWorld = lightPos + samplePointLocal.x * lightTan + samplePointLocal.y * lineCross;

            // Limit ray length to remove length checking in process.
            float3 rayDir = samplePointWorld - pushOffPos;
            const float maxT = length(rayDir);
            float nDotL = 1.0f;
            rayDir /= maxT;
#ifndef PROBES
            // light ray backfacing ?
            nDotL = IsNormalValid(surfaceNormal) ? dot(rayDir, surfaceNormal) : 1.0f;
            bool shouldCancelRay = nDotL < 0.0f || any(isnan(nDotL));
            if (shouldCancelRay)
            {
                Ray_SetInactive(dstRay);
            }
            else
#endif
            {
                Ray_Init(dstRay, pushOffPos, rayDir, maxT, nDotL, rayLodInfo);
            }
        }
    }
    else if (light.lightType == kPVRLightPyramid)
    {
        bool   hasNormal   = IsNormalValid(surfaceNormal);
        float3 lightPos    = light.pos.xyz;
        float3 lightDir_NZ = normalize(light.dir.xyz);
        float3 toLight     = lightPos - surfacePosition;
        float  range       = lightRange;

        float  dist       = maxt;
        float3 toLight_NZ = toLight / dist;

        // wrong side
        float dotVal = hasNormal ? dot(surfaceNormal, toLight_NZ) : 1.0f;
        float projectedDist = dot(-toLight, lightDir_NZ);
        if (dist > range || dotVal <= 0.0 || isnan(dotVal) || projectedDist <= 0.0)
        {
            Ray_SetInactive(dstRay);
        }
        else
        {
            float3 pos2 = lightPos + lightDir_NZ;
            float3 pos3 = lightPos - (toLight / projectedDist);
            float3 edge = pos3 - pos2;

            float3 lightTan   = light.dataUnion.pyramidLightData.scaledTangent.xyz;
            float3 lightBitan = light.dataUnion.pyramidLightData.scaledBitangent.xyz;
            float  width      = fabs(dot(edge, lightBitan));
            float  height     = fabs(dot(edge, lightTan));

            if (width > 1.0f || height > 1.0f)
            {
                Ray_SetInactive(dstRay);
            }
            else
            {
                Ray_Init(dstRay, pushOffPos, toLight_NZ, dist, dotVal, rayLodInfo);
            }
        }
    }
    else if (light.lightType == kPVRLightBox)
    {
        bool   hasNormal     = IsNormalValid(surfaceNormal);
        float3 toLight_NZ    = normalize(-light.dir.xyz);
        float3 lightPos      = light.pos.xyz;

        float  dotVal        = hasNormal ? dot(surfaceNormal, toLight_NZ) : 1.0f;
        float  range         = lightRange;
        float  projectedDist = dot(toLight_NZ, lightPos - surfacePosition);

        if (dotVal <= 0.0 || isnan(dotVal) || projectedDist < 0.0 || projectedDist > range)
        {
            Ray_SetInactive(dstRay);
        }
        else
        {
            float3 lightTan   = light.dataUnion.pyramidLightData.scaledTangent.xyz;
            float3 lightBitan = light.dataUnion.pyramidLightData.scaledBitangent.xyz;
            float3 pos2       = lightPos - toLight_NZ * projectedDist;
            float3 edge       = surfacePosition - pos2;
            float  width      = fabs(dot(edge, lightBitan));
            float  height     = fabs(dot(edge, lightTan));

            if (width > 1.0f || height > 1.0f)
            {
                Ray_SetInactive(dstRay);
            }
            else
            {
                Ray_Init(dstRay, pushOffPos, toLight_NZ, projectedDist, dotVal, rayLodInfo);
            }
        }
    }
}

static float SampleFalloff(const int falloffIndex, const float normalizedSamplePosition, __global const float* restrict distanceFalloffs_buffer KERNEL_VALIDATOR_BUFFERS_DEF)
{
    const int sampleCount = LIGHTMAPPER_FALLOFF_TEXTURE_WIDTH;
    float index = normalizedSamplePosition * (float)sampleCount;

    // compute the index pair
    int loIndex = min((int)index, (sampleCount - 1));
    int hiIndex = min((int)index + 1, (sampleCount - 1));
    float hiFraction = (index - (float)loIndex);

    const int offset = falloffIndex * LIGHTMAPPER_FALLOFF_TEXTURE_WIDTH;
    const float sampleLo = INDEX_SAFE(distanceFalloffs_buffer, offset + loIndex);
    const float sampleHi = INDEX_SAFE(distanceFalloffs_buffer, offset + hiIndex);

    // do the lookup
    return (1.0 - hiFraction) * sampleLo + hiFraction * sampleHi;
}

static float3 ShadeDirectionalLight(
    const LightBuffer        light,
    const ray                surfaceToLightRay,
    float3                   surfacePosition,
    __global const int*  restrict cookiesBuffer
    KERNEL_VALIDATOR_BUFFERS_DEF
)
{
    const float nDotL = surfaceToLightRay.d.w;

    float3 dirColor = nDotL * light.col.xyz;

    int cookieIndex = light.dataUnion.dirLightData.cookieIndex;
    if (cookieIndex >= 0)
    {
        float3 lightVecOut   = surfaceToLightRay.d.xyz;
        float3 tan_NZ        = { light.dataUnion.dirLightData.tanX, light.dataUnion.dirLightData.tanY, light.dataUnion.dirLightData.tanZ };
        float3 position      = surfacePosition;
        float3 lightPos      = light.pos.xyz;
        float3 toLight       = position - lightPos;
        float  dist          = dot(lightVecOut, toLight);
        float3 projected     = (position + dist * lightVecOut) - lightPos;
        float3 lightBitan_NZ = cross(-light.dir.xyz, tan_NZ);
        float2 scales        = (float2)(light.dataUnion.dirLightData.cookieSizeXRcp, light.dataUnion.dirLightData.cookieSizeYRcp);
        float2 uvs           = (float2)(dot(projected, scales.x * lightBitan_NZ),
            dot(projected, scales.y * tan_NZ));
        uvs           = uvs * 0.5f + 0.5f;

        // pull header info first
        bool tileCookie;
        CookieHeader ch = LoadCookieHeaderWithTiling(cookieIndex, &tileCookie, cookiesBuffer KERNEL_VALIDATOR_BUFFERS);

        if (tileCookie || (uvs.x >= 0.0f && uvs.y >= 0.0f && uvs.x <= 1.0f && uvs.y <= 1.0f))
            dirColor *= GetCookieAttenuation(ch, tileCookie, 0, uvs, cookiesBuffer KERNEL_VALIDATOR_BUFFERS);
    }

    return dirColor;
}

static float3 ShadeRectangularAreaLight(
    const LightBuffer       light,
    const ray               surfaceToLightRay,
    float3                  surfacePosition,
    __global const int*  restrict cookiesBuffer
    KERNEL_VALIDATOR_BUFFERS_DEF
)
{
    int    cookieIndex       = light.dataUnion.areaLightData.cookieIndex;
    float3 cookieAttenuation = (float3)(1.0f, 1.0f, 1.0f);

    if (cookieIndex >= 0)
    {
        float  maxT   = surfaceToLightRay.o.w;
        float3 edge   = surfacePosition + surfaceToLightRay.d.xyz * maxT - light.pos.xyz;
        float3 e0     = light.dataUnion.areaLightData.Bitangent.xyz;
        float3 e1     = light.dataUnion.areaLightData.Tangent.xyz;
        float  width  = light.dataUnion.areaLightData.areaWidth;
        float  height = light.dataUnion.areaLightData.areaHeight;
        float2 uvs    = (float2)(2.0f * dot(edge, e0) / width, 2.0f * dot(edge, e1) / height);

        uvs = uvs * 0.5f + 0.5f;

        // pull header info first
        CookieHeader ch = LoadCookieHeader(cookieIndex, cookiesBuffer KERNEL_VALIDATOR_BUFFERS);
        cookieAttenuation = GetCookieAttenuation(ch, false, 0, uvs, cookiesBuffer KERNEL_VALIDATOR_BUFFERS);
    }

    const float nDotLAndSolidAngle = surfaceToLightRay.d.w;
    return nDotLAndSolidAngle / FLT_PI * light.col.xyz * cookieAttenuation;
}

static float3 ShadeDiscLight(
    const LightBuffer       light,
    const ray               surfaceToLightRay,
    float3                  surfacePosition,
    __global const int*  restrict cookiesBuffer
    KERNEL_VALIDATOR_BUFFERS_DEF
)
{
    const float radius = light.dataUnion.discLightData.radius;
    const float maxTOut = surfaceToLightRay.o.w;
    const float3 lightDir = normalize(light.dataUnion.discLightData.Normal.xyz);
    const float3 lightVecOut = surfaceToLightRay.d.xyz;
    const float nDotL = surfaceToLightRay.d.w;

    int    cookieIndex       = light.dataUnion.discLightData.cookieIndex;
    float3 cookieAttenuation = (float3)(1.0f, 1.0f, 1.0f);

    if (cookieIndex >= 0)
    {
        float3 edge   = surfacePosition + surfaceToLightRay.d.xyz * maxTOut - light.pos.xyz;
        float3 e1     = light.dataUnion.discLightData.Tangent.xyz;
        float3 e0     = cross(lightDir, e1);
        float  scale  = 1.0f / (radius * 0.5f);
        float2 uvs    = (float2)(dot(edge, e0) * scale, dot(edge, e1) * scale);

        uvs = uvs * 0.5f + 0.5f;

        // pull header info first
        CookieHeader ch = LoadCookieHeader(cookieIndex, cookiesBuffer KERNEL_VALIDATOR_BUFFERS);
        cookieAttenuation = GetCookieAttenuation(ch, false, 0, uvs, cookiesBuffer KERNEL_VALIDATOR_BUFFERS);
    }

    // * (Pi / Pi) removed from the expression below as it cancels out.
    return (nDotL * radius * radius * dot(lightDir, -lightVecOut)) / (maxTOut * maxTOut) * light.col.xyz * cookieAttenuation;
}

// This code must be kept in sync with FalloffLUT.cpp::LookupFalloffLUT
static float LookupAngularFalloffLUT(float angularScale, __global const float* restrict angularFalloffLUT_buffer KERNEL_VALIDATOR_BUFFERS_DEF)
{
    const int gAngularFalloffTableLength = 128; // keep in sync with Editor\Src\GI\Progressive\RadeonRays\RRBakeTechnique.cpp
    const int sampleCount = gAngularFalloffTableLength;

    //======================================
    // light distance falloff lookup:
    //   d = Max(0, distance - m_Radius) / (m_CutOff - m_Radius)
    //   index = (g_SampleCount - 1) / (1 + d * d * (g_SampleCount - 2))
    float tableDist = max(angularScale, 0.0f);
    float index = (float)(sampleCount - 1) / (1.0f + tableDist * tableDist * (float)(sampleCount - 2));

    // compute the index pair
    int loIndex = min((int)(index), (sampleCount - 1));
    int hiIndex = min((int)(index) + 1, (sampleCount - 1));
    float hiFraction = (index - (float)(loIndex));

    // do the lookup
    return (1.0 - hiFraction) * INDEX_SAFE(angularFalloffLUT_buffer, loIndex) + hiFraction * INDEX_SAFE(angularFalloffLUT_buffer, hiIndex);
}

static float3 ShadePointLight(
    const LightBuffer        light,
    const ray                surfaceToLightRay,
    const bool               evalCookie,
    const int                falloffIndex,
    __global const float* restrict distanceFalloffs_buffer,
    __global const int*   restrict cookiesBuffer
    KERNEL_VALIDATOR_BUFFERS_DEF
)
{
    const float distance = surfaceToLightRay.o.w;
    const float lightRange = light.dir.w;
    const float distScale = distance / lightRange;
    if (distScale > 1.0f)
        return make_float3(0.0f, 0.0f, 0.0f);

    int cookieIndex = evalCookie ? light.dataUnion.pointLightData.cookieIndex : -1;
    float3 cookieAttenuation = (float3)(1.0f, 1.0f, 1.0f);
    if (cookieIndex >= 0)
        cookieAttenuation = GetCubemapCookieAttenuation(light, -surfaceToLightRay.d.xyz, cookiesBuffer KERNEL_VALIDATOR_BUFFERS);

    const float nDotL = surfaceToLightRay.d.w;
    const float falloff = SampleFalloff(falloffIndex, distScale, distanceFalloffs_buffer KERNEL_VALIDATOR_BUFFERS);

    return falloff * nDotL * light.col.xyz * cookieAttenuation;
}

//Angle attenuation code from HDRP CommonLighting.hlsl
// Square the result to smoothen the function.
static float HDRPSmoothAngleAttenuation(
    const float cosFwd,
    const float lightAngleScale,
    const float lightAngleOffset)
{
    float attenuation = clamp(cosFwd * lightAngleScale + lightAngleOffset, 0.0f, 1.0f);
    return attenuation * attenuation;
}

static float3 ShadeSpotLight(
    const LightBuffer        light,
    const ray                surfaceToLightConeRay,
    float3                   surfacePosition,
    __global const float* restrict angularFalloffLUT_buffer,
    __global const float* restrict distanceFalloffs_buffer,
    __global const int*   restrict cookiesBuffer
    KERNEL_VALIDATOR_BUFFERS_DEF
)
{
    float3 pointColor = ShadePointLight(light, surfaceToLightConeRay, false, light.dataUnion.spotLightData.LightFalloffIndex, distanceFalloffs_buffer, cookiesBuffer KERNEL_VALIDATOR_BUFFERS);

    const float cosConeAng = light.dataUnion.spotLightData.cosineConeAngle;
    const float invCosConeAng = light.dataUnion.spotLightData.inverseCosineConeAngle;
    const float3 unJitteredDirToLight = normalize(light.pos.xyz - surfacePosition);
    const float dval = dot(unJitteredDirToLight, -light.dir.xyz);
    const float angScale = (dval - cosConeAng) / invCosConeAng;
    float angFalloff;

    int cookieIndex = light.dataUnion.spotLightData.cookieIndex;
    if (cookieIndex >= 0)
    {
        float3 dir_NZ        = -light.dir.xyz;
        float3 tan_NZ        = { light.dataUnion.spotLightData.tanX, light.dataUnion.spotLightData.tanY, light.dataUnion.spotLightData.tanZ };
        float3 projected     = dir_NZ - unJitteredDirToLight / dval;
        float3 lightBitan_NZ = cross(dir_NZ, tan_NZ);
        float scale          = cosConeAng / sqrt(1.0f - cosConeAng * cosConeAng);
        float2 uvs           = make_float2(dot(projected, scale * lightBitan_NZ), dot(projected, scale * tan_NZ));
        uvs           = uvs * 0.5f + 0.5f;

        if (uvs.x >= 0.0f && uvs.x <= 1.0f && uvs.y >= 0.0f && uvs.y <= 1.0f)
        {
            // pull header info first
            CookieHeader ch = LoadCookieHeader(cookieIndex, cookiesBuffer KERNEL_VALIDATOR_BUFFERS);
            pointColor *= GetCookieAttenuation(ch, false, 0, uvs, cookiesBuffer KERNEL_VALIDATOR_BUFFERS);
        }
        else
            pointColor = (float3)(0.0f, 0.0f, 0.f);
    }

    if (light.dataUnion.spotLightData.angularFalloffType == kAngularFalloffTypeLUT)
    {
        // builtin cookies completely control angular falloff
        if (light.dataUnion.spotLightData.cookieIndex < 0)
            angFalloff = 1.0f - LookupAngularFalloffLUT(angScale, angularFalloffLUT_buffer KERNEL_VALIDATOR_BUFFERS);
        else
            angFalloff = 1.0f;
    }
    else//inner angle support AND Analyticfalloff
    {
        const float cosInnerAngle = light.dataUnion.spotLightData.cosineInnerAngle;

        //There is no attenuation inside inner angle
        if (dval >= cosInnerAngle)
        {
            angFalloff = 1.0f;
        }
        else//Otherwise match HDRP analytic formula for attenuation
        {
            float angleScale = 1.0f / max(0.0001f, (cosInnerAngle - cosConeAng));
            float lightAngleOffset = -cosConeAng * angleScale;
            angFalloff = HDRPSmoothAngleAttenuation(dval, angleScale, lightAngleOffset);
        }
    }

    return pointColor * angFalloff;
}

static float3 ShadePyramidLight(
    const LightBuffer               light,
    const ray                       surfaceToLightRay,
    float3                          surfacePosition,
    const int                       falloffIndex,
    __global const float* restrict  distanceFalloffs_buffer,
    __global const int*  restrict   cookiesBuffer
    KERNEL_VALIDATOR_BUFFERS_DEF
)
{
    const float distance   = surfaceToLightRay.o.w;
    const float lightRange = light.dir.w;
    const float distScale  = distance / lightRange;
    if (distScale > 1.0f)
        return make_float3(0.0f, 0.0f, 0.0f);

    const float distFalloff = SampleFalloff(falloffIndex, distScale, distanceFalloffs_buffer KERNEL_VALIDATOR_BUFFERS);
    const float nDotL = surfaceToLightRay.d.w;

    int    cookieIndex       = light.dataUnion.pyramidLightData.cookieIndex;
    float3 cookieAttenuation = (float3)(1.0f, 1.0f, 1.0f);

    if (cookieIndex >= 0)
    {
        float3 toLight = light.pos.xyz - surfacePosition;
        float  projectedDist = dot(-toLight, light.dir.xyz);
        float3 pos2 = light.pos.xyz + light.dir.xyz;
        float3 pos3 = light.pos.xyz - (toLight / projectedDist);
        float3 edge = pos3 - pos2;
        float3 e0   = light.dataUnion.pyramidLightData.scaledTangent.xyz;
        float3 e1   = light.dataUnion.pyramidLightData.scaledBitangent.xyz;
        float2 uvs  = (float2)(dot(edge, e0), dot(edge, e1));

        uvs = uvs * 0.5f + 0.5f;
        // pull header info first
        CookieHeader ch = LoadCookieHeader(cookieIndex, cookiesBuffer KERNEL_VALIDATOR_BUFFERS);
        cookieAttenuation = GetCookieAttenuation(ch, false, 0, uvs, cookiesBuffer KERNEL_VALIDATOR_BUFFERS);
    }

    return distFalloff * nDotL * light.col.xyz * cookieAttenuation;
}

static float3 ShadeBoxLight(
    const LightBuffer   light,
    const ray           surfaceToLightRay,
    float3              surfacePosition,
    __global const int*  restrict cookiesBuffer
    KERNEL_VALIDATOR_BUFFERS_DEF
)
{
    int    cookieIndex       = light.dataUnion.boxLightData.cookieIndex;
    float3 cookieAttenuation = (float3)(1.0f, 1.0f, 1.0f);

    if (cookieIndex >= 0)
    {
        float3 pos2 = surfacePosition + light.dir.xyz * dot(surfacePosition - light.pos.xyz, light.dir.xyz);
        float3 edge = pos2 - light.pos.xyz;
        float3 e0   = light.dataUnion.boxLightData.scaledTangent.xyz;
        float3 e1   = light.dataUnion.boxLightData.scaledBitangent.xyz;
        float2 uvs = (float2)(dot(edge, e0), dot(edge, e1));

        uvs = uvs * 0.5f + 0.5f;
        // pull header info first
        CookieHeader ch = LoadCookieHeader(cookieIndex, cookiesBuffer KERNEL_VALIDATOR_BUFFERS);
        cookieAttenuation = GetCookieAttenuation(ch, false, 0, uvs, cookiesBuffer KERNEL_VALIDATOR_BUFFERS);
    }

    return light.col.xyz * cookieAttenuation * surfaceToLightRay.d.w;
}

static float3 ShadeLight(
    const LightBuffer        light,
    const ray                surfaceToLightRay,
    float3                   surfacePosition,
    __global const float* restrict angularFalloffLUT_buffer,
    __global const float* restrict distanceFalloffs_buffer,
    __global const int*   restrict cookiesBuffer
    KERNEL_VALIDATOR_BUFFERS_DEF
)
{
    if (light.lightType == kPVRLightSpot)
    {
        return ShadeSpotLight(light, surfaceToLightRay, surfacePosition, angularFalloffLUT_buffer, distanceFalloffs_buffer, cookiesBuffer KERNEL_VALIDATOR_BUFFERS);
    }
    else if (light.lightType == kPVRLightDirectional)
    {
        return ShadeDirectionalLight(light, surfaceToLightRay, surfacePosition, cookiesBuffer KERNEL_VALIDATOR_BUFFERS);
    }
    else if (light.lightType == kPVRLightPoint)
    {
        return ShadePointLight(light, surfaceToLightRay, true, light.dataUnion.pointLightData.LightFalloffIndex, distanceFalloffs_buffer, cookiesBuffer KERNEL_VALIDATOR_BUFFERS);
    }
    else if (light.lightType == kPVRLightRectangle)
    {
        return ShadeRectangularAreaLight(light, surfaceToLightRay, surfacePosition, cookiesBuffer KERNEL_VALIDATOR_BUFFERS);
    }
    else if (light.lightType == kPVRLightPyramid)
    {
        return ShadePyramidLight(light, surfaceToLightRay, surfacePosition, (int)light.dataUnion.pyramidLightData.scaledBitangent.w, distanceFalloffs_buffer, cookiesBuffer KERNEL_VALIDATOR_BUFFERS);
    }
    else if (light.lightType == kPVRLightBox)
    {
        return ShadeBoxLight(light, surfaceToLightRay, surfacePosition, cookiesBuffer KERNEL_VALIDATOR_BUFFERS);
    }
    //else if (light.lightType == kPVRLightDisc)
    {
        return ShadeDiscLight(light, surfaceToLightRay, surfacePosition, cookiesBuffer KERNEL_VALIDATOR_BUFFERS);
    }
}

#endif
