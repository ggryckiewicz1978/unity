#ifndef EMISSIVE_LIGHTING_H
#define EMISSIVE_LIGHTING_H

#include "commonCL.h"
#include "textureFetch.h"

typedef struct _atlasInfo
{
    float4 color;
    float2 textureUVs;
} AtlasInfo;

static AtlasInfo FetchEmissionFromRayIntersection(
    const int                                compactedRayIndex,
    INPUT_BUFFER(, Intersection,              pathIntersectionsCompactedBuffer),
    INPUT_BUFFER(, MaterialTextureProperties, instanceIdToEmissiveTextureProperties),
    INPUT_BUFFER(, MeshDataOffsets,           instanceIdToMeshDataOffsets),
    INPUT_BUFFER(, float2,                    geometryUV1sBuffer),
    INPUT_BUFFER(, uint,                      geometryIndicesBuffer),
    INPUT_BUFFER(, float4,                    dynarg_texture_buffer)
    KERNEL_VALIDATOR_BUFFERS_DEF
)
{
    const int   instanceId = GetInstanceIdFromIntersection(GET_PTR_SAFE(pathIntersectionsCompactedBuffer, compactedRayIndex));
    const MaterialTextureProperties matProperty = INDEX_SAFE(instanceIdToEmissiveTextureProperties, instanceId);

    float2 textureUVs = GetUVsAtRayIntersection(compactedRayIndex,
        pathIntersectionsCompactedBuffer,
        instanceIdToMeshDataOffsets,
        geometryUV1sBuffer,
        geometryIndicesBuffer
        KERNEL_VALIDATOR_BUFFERS);

    AtlasInfo atlasInfo;
    atlasInfo.color = FetchTextureFromMaterialAndUVs(dynarg_texture_buffer, textureUVs, matProperty, true KERNEL_VALIDATOR_BUFFERS);
    atlasInfo.textureUVs = textureUVs;

    return atlasInfo;
}

static AtlasInfo FetchAlbedoFromRayIntersection(
    const int                                compactedRayIndex,
    INPUT_BUFFER(, Intersection,              pathIntersectionsCompactedBuffer),
    INPUT_BUFFER(, MaterialTextureProperties, instanceIdToAlbedoTextureProperties),
    INPUT_BUFFER(, MeshDataOffsets,           instanceIdToMeshDataOffsets),
    INPUT_BUFFER(, float2,                    geometryUV1sBuffer),
    INPUT_BUFFER(, int,                       geometryIndicesBuffer),
    INPUT_BUFFER(, uchar4,                    albedoTextures_buffer)
    KERNEL_VALIDATOR_BUFFERS_DEF
)
{
    const int   instanceId = GetInstanceIdFromIntersection(GET_PTR_SAFE(pathIntersectionsCompactedBuffer, compactedRayIndex));
    const MaterialTextureProperties matProperty = INDEX_SAFE(instanceIdToAlbedoTextureProperties, instanceId);

    float2 textureUVs = GetUVsAtRayIntersection(compactedRayIndex,
        pathIntersectionsCompactedBuffer,
        instanceIdToMeshDataOffsets,
        geometryUV1sBuffer,
        geometryIndicesBuffer
        KERNEL_VALIDATOR_BUFFERS);

    AtlasInfo atlasInfo;
    atlasInfo.color = FetchUChar4TextureFromMaterialAndUVs(albedoTextures_buffer, textureUVs, matProperty, true, true KERNEL_VALIDATOR_BUFFERS);
    atlasInfo.textureUVs = textureUVs;

    return atlasInfo;
}

#endif // EMISSIVE_LIGHTING_H
