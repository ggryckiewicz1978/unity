#include "commonCL.h"

__kernel void prepareExpandedRayIndices(
    //output
    OUTPUT_BUFFER(00, SampleDescription, sampleDescriptionsExpandedBuffer),
    OUTPUT_BUFFER(01, uint,              sampleDescriptionsExpandedCountBuffer),
    OUTPUT_BUFFER(02, ExpandedTexelInfo, expandedTexelsBuffer),
    OUTPUT_BUFFER(03, uint,              expandedTexelsCountBuffer),
    //input and output
    OUTPUT_BUFFER(04, int,               directSampleCountBuffer),
    OUTPUT_BUFFER(05, int,               environmentSampleCountBuffer),
    OUTPUT_BUFFER(06, int,               indirectSampleCountBuffer),
    //input
    INPUT_VALUE(  07, int,               radeonRaysExpansionPass),
    INPUT_VALUE(  08, int,               numRaysToShootPerTexel),
    INPUT_VALUE(  09, int,               maxSampleCount),
    INPUT_VALUE(  10, int,               maxOutputRayCount)
#ifndef PROBES
    ,
    INPUT_BUFFER( 11, unsigned char,     cullingMapBuffer),
    INPUT_BUFFER( 12, unsigned char,     occupancyBuffer),
    INPUT_VALUE(  13, int,               shouldUseCullingMap)
#endif
    KERNEL_VALIDATOR_BUFFERS_DEF
)
{
    // Initialize local memory
    __local int numExpandedTexelsForThreadGroup;
    __local int threadGroupExpandedTexelOffsetInGlobalMemory;
    __local int numRaysForThreadGroup;
    __local int threadGroupRaysOffsetInGlobalMemory;
    if (get_local_id(0) == 0)
    {
        numExpandedTexelsForThreadGroup = 0;
        threadGroupExpandedTexelOffsetInGlobalMemory = 0;
        numRaysForThreadGroup = 0;
        threadGroupRaysOffsetInGlobalMemory = 0;
    }
    barrier(CLK_LOCAL_MEM_FENCE);

    const uint idx = get_global_id(0);
    int numRaysToPrepare = numRaysToShootPerTexel;
#if DISALLOW_RAY_EXPANSION
    numRaysToPrepare = 1;
#endif

    // STEP 1 : Determine if the texel is active (i.e. occupied && visible).
#ifndef PROBES
    const int occupiedSamplesWithinTexel = INDEX_SAFE(occupancyBuffer, idx);
    if (occupiedSamplesWithinTexel == 0)
        numRaysToPrepare = 0;
    if (shouldUseCullingMap && numRaysToPrepare && IsCulled(INDEX_SAFE(cullingMapBuffer, idx)))
        numRaysToPrepare = 0;
#endif

    // STEP 2 : Compute how many rays we want to shoot for the active texels.
    int currentSampleCount;
    if (numRaysToPrepare)
    {
        if (radeonRaysExpansionPass == kRRExpansionPass_direct)
        {
            currentSampleCount = INDEX_SAFE(directSampleCountBuffer, idx);
        }
        else if (radeonRaysExpansionPass == kRRExpansionPass_environment)
        {
            currentSampleCount = INDEX_SAFE(environmentSampleCountBuffer, idx);
        }
        else if (radeonRaysExpansionPass == kRRExpansionPass_indirect)
        {
            currentSampleCount = INDEX_SAFE(indirectSampleCountBuffer, idx);
        }

        KERNEL_ASSERT(maxSampleCount >= currentSampleCount);
        int samplesLeftBeforeConvergence = max(maxSampleCount - currentSampleCount, 0);
        numRaysToPrepare = min(samplesLeftBeforeConvergence, numRaysToPrepare);
    }

    // STEP 3 : Compute rays write offsets and init the rays indices.
    int rayOffsetInThreadGroup = 0;
    if (numRaysToPrepare)
        rayOffsetInThreadGroup = atomic_add(&numRaysForThreadGroup, numRaysToPrepare);
    barrier(CLK_LOCAL_MEM_FENCE);
    if (get_local_id(0) == 0)
    {
#if DISALLOW_RAY_EXPANSION
        numRaysForThreadGroup = get_local_size(0);
#endif
        //Note: SampleDescriptionsExpandedCountBuffer will be potentially bigger than the size of SampleDescriptionsExpandedBuffer. However this is fine
        //as we will only dispatch the following kernel with numthread = ray buffer size.
        threadGroupRaysOffsetInGlobalMemory = atomic_add(GET_PTR_SAFE(sampleDescriptionsExpandedCountBuffer, 0), numRaysForThreadGroup);
    }
    barrier(CLK_LOCAL_MEM_FENCE);

    // STEP 4 : Write the rays texel index out (avoiding writing more rays than the buffer can hold).
    int threadGlobalRayOffset = threadGroupRaysOffsetInGlobalMemory + rayOffsetInThreadGroup;
    int maxNumRaysThisThreadCanPrepare = max(maxOutputRayCount - threadGlobalRayOffset, 0);
    numRaysToPrepare = min(maxNumRaysThisThreadCanPrepare, numRaysToPrepare);
#if DISALLOW_RAY_EXPANSION
    SampleDescription sampleDescription;
    sampleDescription.texelIndex = numRaysToPrepare ? idx : -1;//-1 marks a texel we should not cast a ray to (invalid or culled)
    sampleDescription.currentSampleCount = currentSampleCount;
    INDEX_SAFE(sampleDescriptionsExpandedBuffer, idx) = sampleDescription;
#else
    for (int i = 0; i < numRaysToPrepare; ++i)
    {
        SampleDescription sampleDescription;
        sampleDescription.texelIndex = idx;
        sampleDescription.currentSampleCount = currentSampleCount + i;
        INDEX_SAFE(sampleDescriptionsExpandedBuffer, threadGlobalRayOffset + i) = sampleDescription;
    }
#endif

    // STEP 5 : Register expanded texel info for the gather step.
    int expandedTexelOffsetInThreadGroup = 0;
    if (numRaysToPrepare)
        expandedTexelOffsetInThreadGroup = atomic_inc(&numExpandedTexelsForThreadGroup);
    barrier(CLK_LOCAL_MEM_FENCE);
    if (get_local_id(0) == 0)
    {
        KERNEL_ASSERT(numExpandedTexelsForThreadGroup <= get_local_size(0));
        KERNEL_ASSERT(numRaysForThreadGroup <= (numRaysToShootPerTexel * get_local_size(0)));
        KERNEL_ASSERT(numRaysForThreadGroup >= numExpandedTexelsForThreadGroup);
        KERNEL_ASSERT(numExpandedTexelsForThreadGroup <= get_local_size(0));
        threadGroupExpandedTexelOffsetInGlobalMemory = atomic_add(GET_PTR_SAFE(expandedTexelsCountBuffer, 0), numExpandedTexelsForThreadGroup);
    }
    barrier(CLK_LOCAL_MEM_FENCE);
    if (numRaysToPrepare)
    {
        ExpandedTexelInfo expandedTexelInfo;
#if DISALLOW_RAY_EXPANSION
        expandedTexelInfo.firstRaysOffset = idx;
#else
        expandedTexelInfo.firstRaysOffset = threadGlobalRayOffset;
#endif
        expandedTexelInfo.numRays = numRaysToPrepare;
        expandedTexelInfo.originalTexelIndex = idx;
        KERNEL_ASSERT(threadGroupExpandedTexelOffsetInGlobalMemory < get_global_size(0));
        KERNEL_ASSERT((threadGroupExpandedTexelOffsetInGlobalMemory + expandedTexelOffsetInThreadGroup)< get_global_size(0));
        KERNEL_ASSERT(expandedTexelOffsetInThreadGroup < get_local_size(0));
        INDEX_SAFE(expandedTexelsBuffer, threadGroupExpandedTexelOffsetInGlobalMemory + expandedTexelOffsetInThreadGroup) = expandedTexelInfo;

        //increment sample count
        if (radeonRaysExpansionPass == kRRExpansionPass_direct)
        {
            INDEX_SAFE(directSampleCountBuffer, idx) += numRaysToPrepare;
        }
        else if (radeonRaysExpansionPass == kRRExpansionPass_environment)
        {
            INDEX_SAFE(environmentSampleCountBuffer, idx) += numRaysToPrepare;
        }
        else if (radeonRaysExpansionPass == kRRExpansionPass_indirect)
        {
            INDEX_SAFE(indirectSampleCountBuffer, idx) += numRaysToPrepare;
        }
    }
}

__kernel void gatherProcessedExpandedRays(
    INPUT_BUFFER( 00, ExpandedTexelInfo, expandedTexelsBuffer),
    INPUT_BUFFER( 01, uint,              expandedTexelsCountBuffer),
    INPUT_VALUE(  02, int,               radeonRaysExpansionPass),
#ifdef PROBES
    INPUT_VALUE(  03, int,               numProbes),
    INPUT_VALUE(  04, int,               lightmappingSourceType),
    INPUT_BUFFER( 05, float4,            probeSHExpandedBuffer),
    INPUT_BUFFER( 06, float,             probeDepthOctahedronExpandedBuffer),
    INPUT_BUFFER( 07, float4,            probeOcclusionExpandedBuffer),
    INPUT_BUFFER( 08, float4,            shadowmaskExpandedBuffer), //when gathering indirect .x will contain AO and .y will contain Validity
    OUTPUT_BUFFER(09, float4,            outputProbeDirectSHDataBuffer),
    OUTPUT_BUFFER(10, float4,            outputProbeOcclusionBuffer),
    OUTPUT_BUFFER(11, float4,            outputProbeIndirectSHDataBuffer),
    OUTPUT_BUFFER(12, float,             outputProbeValidityBuffer),
    OUTPUT_BUFFER(13, float,             outputProbeDepthOctahedronBuffer)
#else
    INPUT_VALUE(  03, int,               lightmapMode),
    INPUT_BUFFER( 04, float3,            lightingExpandedBuffer),
    INPUT_BUFFER( 05, float4,            shadowmaskExpandedBuffer),//when gathering indirect .x will contain AO and .y will contain Validity
    INPUT_BUFFER( 06, float4,            directionalExpandedBuffer),
    OUTPUT_BUFFER(07, float4,            outputDirectLightingBuffer),
    OUTPUT_BUFFER(08, float4,            outputShadowmaskFromDirectBuffer),
    OUTPUT_BUFFER(09, float4,            outputDirectionalFromDirectBuffer),
    OUTPUT_BUFFER(10, float4,            outputIndirectLightingBuffer),
    OUTPUT_BUFFER(11, float4,            outputEnvironmentLightingBuffer),
    OUTPUT_BUFFER(12, float4,            outputDirectionalFromGiBuffer),
    OUTPUT_BUFFER(13, float,             outputAoBuffer),
    OUTPUT_BUFFER(14, float,             outputValidityBuffer)
#endif
    KERNEL_VALIDATOR_BUFFERS_DEF
)
{
    const uint expandedTexelInfoIdx = get_global_id(0);
    const uint numExpandedTexels = INDEX_SAFE(expandedTexelsCountBuffer, 0);
    if (expandedTexelInfoIdx < numExpandedTexels)
    {
        const ExpandedTexelInfo expandedTexelInfo = INDEX_SAFE(expandedTexelsBuffer, expandedTexelInfoIdx);
        const int numRays = expandedTexelInfo.numRays;
        const int raysOffset = expandedTexelInfo.firstRaysOffset;
        const uint originalTexelIndex = expandedTexelInfo.originalTexelIndex;

#ifdef PROBES
        float4 probeOcclusion = (float4)(0.0f, 0.0f, 0.0f, 0.0f);
        for (int i = 0; i < numRays; ++i)
            probeOcclusion += INDEX_SAFE(probeOcclusionExpandedBuffer, raysOffset + i);

        if (lightmappingSourceType == kLightmappingSourceType_Probe)
        {
            float4 outSH[SH_COEFF_COUNT];

            for (int coeff = 0; coeff < SH_COEFF_COUNT; ++coeff)
                outSH[coeff] = (float4)(0.0f, 0.0f, 0.0f, 0.0f);

            float outDepthOctahedron[OCTAHEDRON_TEXEL_COUNT];
            for (int texel = 0; texel < OCTAHEDRON_TEXEL_COUNT; ++texel)
                outDepthOctahedron[texel] = 0;

            float4 shadowMask = (float4)(0.0f, 0.0f, 0.0f, 0.0f);
            for (int i = 0; i < numRays; ++i)
            {
                int dataPositionCoeff = (raysOffset + i) * SH_COEFF_COUNT;
                for (int coeff = 0; coeff < SH_COEFF_COUNT; ++coeff)
                {
                    outSH[coeff] += INDEX_SAFE(probeSHExpandedBuffer, dataPositionCoeff + coeff);
                }

                shadowMask += INDEX_SAFE(shadowmaskExpandedBuffer, raysOffset + i);

                int dataPositionOctahedron = (raysOffset + i) * OCTAHEDRON_TEXEL_COUNT;
                for (int texel = 0; texel < OCTAHEDRON_TEXEL_COUNT; ++texel)
                {
                    outDepthOctahedron[texel] += INDEX_SAFE(probeDepthOctahedronExpandedBuffer, dataPositionOctahedron + texel);
                }
            }

            // TODO(RadeonRays): memory access is all over the place, make a struct ala SphericalHarmonicsL2 instead of loading/storing with a stride.
            if (radeonRaysExpansionPass == kRRExpansionPass_direct)
            {
                for (int coeff = 0; coeff < SH_COEFF_COUNT; ++coeff)
                {
                    INDEX_SAFE(outputProbeDirectSHDataBuffer, numProbes * coeff + originalTexelIndex) += outSH[coeff];
                }
                INDEX_SAFE(outputProbeOcclusionBuffer, originalTexelIndex) += probeOcclusion;
            }
            else
            {
                for (int coeff = 0; coeff < SH_COEFF_COUNT; ++coeff)
                {
                    INDEX_SAFE(outputProbeIndirectSHDataBuffer, numProbes * coeff + originalTexelIndex) += outSH[coeff];
                }
            }

            if (radeonRaysExpansionPass == kRRExpansionPass_indirect)
            {
                INDEX_SAFE(outputProbeValidityBuffer, originalTexelIndex) += shadowMask.y;

                for (int texel = 0; texel < OCTAHEDRON_TEXEL_COUNT; ++texel)
                {
                    INDEX_SAFE(outputProbeDepthOctahedronBuffer, originalTexelIndex * OCTAHEDRON_TEXEL_COUNT + texel) += outDepthOctahedron[texel];
                }
            }
        }
        else
        {
            if (radeonRaysExpansionPass == kRRExpansionPass_direct)
            {
                INDEX_SAFE(outputProbeOcclusionBuffer, originalTexelIndex) += probeOcclusion;
            }
        }
#else
        float4 shadowMask = (float4)(0.0f, 0.0f, 0.0f, 0.0f);
        float3 lighting = (float3)(0.0f, 0.0f, 0.0f);
        for (int i = 0; i < numRays; ++i)
        {
            // TODO(RadeonRays): only fetch and accumulate shadow mask when needed.
            shadowMask += shadowmaskExpandedBuffer[raysOffset + i];
            lighting += lightingExpandedBuffer[raysOffset + i];
        }

        if (radeonRaysExpansionPass == kRRExpansionPass_direct)
        {
            INDEX_SAFE(outputShadowmaskFromDirectBuffer, originalTexelIndex) += shadowMask;
            INDEX_SAFE(outputDirectLightingBuffer, originalTexelIndex).xyz += lighting;
        }
        else if (radeonRaysExpansionPass == kRRExpansionPass_environment)
        {
            INDEX_SAFE(outputEnvironmentLightingBuffer, originalTexelIndex).xyz += lighting;
        }
        else
        {
            KERNEL_ASSERT(radeonRaysExpansionPass == kRRExpansionPass_indirect);
            INDEX_SAFE(outputIndirectLightingBuffer, originalTexelIndex).xyz += lighting;
            INDEX_SAFE(outputAoBuffer, originalTexelIndex) += shadowMask.x;
            INDEX_SAFE(outputValidityBuffer, originalTexelIndex) += shadowMask.y;
        }

        if (lightmapMode == LIGHTMAPMODE_DIRECTIONAL)
        {
            float4 directionality = (float4)(0.0f, 0.0f, 0.0f, 0.0f);
            for (int i = 0; i < numRays; ++i)
            {
                directionality += directionalExpandedBuffer[raysOffset + i];
            }

            if (radeonRaysExpansionPass == kRRExpansionPass_direct)
            {
                INDEX_SAFE(outputDirectionalFromDirectBuffer, originalTexelIndex) += directionality;
            }
            else
            {
                INDEX_SAFE(outputDirectionalFromGiBuffer, originalTexelIndex) += directionality;
            }
        }
#endif
    }
}
