/**********************************************************************
Copyright (c) 2016 Advanced Micro Devices, Inc. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
********************************************************************/

//UNITY++
//Source: https://github.com/GPUOpen-LibrariesAndSDKs/RadeonRays_SDK/blob/master/RadeonRays/src/kernels/CL/intersect_bvh2_lds.cl

/*************************************************************************
EXTENSIONS
**************************************************************************/
#ifdef AMD_MEDIA_OPS
#pragma OPENCL EXTENSION cl_amd_media_ops2 : enable
#endif //! AMD_MEDIA_OPS
//UNITY--
/*************************************************************************
INCLUDES
**************************************************************************/
//UNITY++
#include "commonCL.h"
#include "textureFetch.h"
//UNITY--

/*************************************************************************
TYPE DEFINITIONS
**************************************************************************/

//UNITY++
#define MISS_MARKER -1
//UNITY--
#define INVALID_ADDR 0xffffffffu
#define INTERNAL_NODE(node) (GetAddrLeft(node) != INVALID_ADDR)

//UNITY++
#define GROUP_SIZE INTERSECT_BVH_WORKGROUPSIZE
//UNITY--
#define STACK_SIZE 32
#define LDS_STACK_SIZE 16

// BVH node
typedef struct
{
    float4 aabb_left_min_or_v0_and_addr_left;
    float4 aabb_left_max_or_v1_and_mesh_id;
    float4 aabb_right_min_or_v2_and_addr_right;
    float4 aabb_right_max_and_prim_id;

} bvh_node;

//UNITY++
/*************************************************************************
HELPER FUNCTIONS
**************************************************************************/
//UNITY--

#define GetAddrLeft(node)   as_uint((node).aabb_left_min_or_v0_and_addr_left.w)
#define GetAddrRight(node)  as_uint((node).aabb_right_min_or_v2_and_addr_right.w)
#define GetMeshId(node)     as_uint((node).aabb_left_max_or_v1_and_mesh_id.w)
#define GetPrimId(node)     as_uint((node).aabb_right_max_and_prim_id.w)

//UNITY++
inline float min3(float a, float b, float c)
{
#ifdef AMD_MEDIA_OPS
    return amd_min3(a, b, c);
#else //! AMD_MEDIA_OPS
    return min(min(a, b), c);
#endif //! AMD_MEDIA_OPS
}

inline float max3(float a, float b, float c)
{
#ifdef AMD_MEDIA_OPS
    return amd_max3(a, b, c);
#else //! AMD_MEDIA_OPS
    return max(max(a, b), c);
#endif //! AMD_MEDIA_OPS
}
//UNITY--

inline float2 fast_intersect_bbox2(float3 pmin, float3 pmax, float3 invdir, float3 oxinvdir, float t_max)
{
    const float3 f = mad(pmax.xyz, invdir, oxinvdir);
    const float3 n = mad(pmin.xyz, invdir, oxinvdir);
    const float3 tmax = max(f, n);
    const float3 tmin = min(f, n);
    const float t1 = min(min3(tmax.x, tmax.y, tmax.z), t_max);
    const float t0 = max(max3(tmin.x, tmin.y, tmin.z), 0.f);
    return (float2)(t0, t1);
}

//UNITY++
// Intersect ray against a triangle and return intersection interval value if it is in
// (0, t_max], return t_max otherwise.
inline float fast_intersect_triangle(ray r, float3 v1, float3 v2, float3 v3, float t_max)
{
    float3 const e1 = v2 - v1;
    float3 const e2 = v3 - v1;
    float3 const s1 = cross(r.d.xyz, e2);

#ifdef USE_SAFE_MATH
    float const invd = 1.f / dot(s1, e1);
#else //! USE_SAFE_MATH
    float const invd = native_recip(dot(s1, e1));
#endif //! USE_SAFE_MATH

    float3 const d = r.o.xyz - v1;
    float const b1 = dot(d, s1) * invd;
    float3 const s2 = cross(d, e1);
    float const b2 = dot(r.d.xyz, s2) * invd;
    float const temp = dot(e2, s2) * invd;

    if (b1 < 0.f || b1 > 1.f || b2 < 0.f || b1 + b2 > 1.f || temp < 0.f || temp > t_max)
    {
        return t_max;
    }
    else
    {
        return temp;
    }
}

inline int ray_is_active(ray const* r)
{
    return r->extra.y;
}

inline float3 safe_invdir(ray r)
{
    float const dirx = r.d.x;
    float const diry = r.d.y;
    float const dirz = r.d.z;
    float const ooeps = 1e-8;
    float3 invdir;
    invdir.x = 1.0f / (fabs(dirx) > ooeps ? dirx : copysign(ooeps, dirx));
    invdir.y = 1.0f / (fabs(diry) > ooeps ? diry : copysign(ooeps, diry));
    invdir.z = 1.0f / (fabs(dirz) > ooeps ? dirz : copysign(ooeps, dirz));
    return invdir;
}

// Given a point in triangle plane, calculate its barycentrics
inline float2 triangle_calculate_barycentrics(float3 p, float3 v1, float3 v2, float3 v3)
{
    float3 const e1 = v2 - v1;
    float3 const e2 = v3 - v1;
    float3 const e = p - v1;
    float const d00 = dot(e1, e1);
    float const d01 = dot(e1, e2);
    float const d11 = dot(e2, e2);
    float const d20 = dot(e, e1);
    float const d21 = dot(e, e2);

#ifdef USE_SAFE_MATH
    float const invdenom = 1.0f / (d00 * d11 - d01 * d01);
#else //! USE_SAFE_MATH
    float const invdenom = native_recip(d00 * d11 - d01 * d01);
#endif //! USE_SAFE_MATH

    float const b1 = (d11 * d20 - d01 * d21) * invdenom;
    float const b2 = (d00 * d21 - d01 * d20) * invdenom;

    return (float2)(b1, b2);
}

/*************************************************************************
KERNELS
**************************************************************************/

__kernel void clearIntersectionBuffer(
    OUTPUT_BUFFER(00, Intersection, pathIntersectionsCompactedBuffer)
    KERNEL_VALIDATOR_BUFFERS_DEF
)
{
    int idx = get_global_id(0);
    INDEX_SAFE(pathIntersectionsCompactedBuffer, idx).primid = MISS_MARKER;
    INDEX_SAFE(pathIntersectionsCompactedBuffer, idx).shapeid = MISS_MARKER;
    INDEX_SAFE(pathIntersectionsCompactedBuffer, idx).uvwt = (float4)(0.0f, 0.0f, 0.0f, 0.0f);
}

__kernel void clearOcclusionBuffer(
    OUTPUT_BUFFER(00,float4, lightOcclusionCompactedBuffer)
    KERNEL_VALIDATOR_BUFFERS_DEF
)
{
    int idx = get_global_id(0);
    INDEX_SAFE(lightOcclusionCompactedBuffer, idx) = (float4)(1.0f, 1.0f, 1.0f, 1.0f);
}
//UNITY--

__attribute__((reqd_work_group_size(GROUP_SIZE, 1, 1)))
__kernel void intersectWithTransmission(
    INPUT_BUFFER( 00, bvh_node,                  nodes),
    INPUT_BUFFER( 01, ray,                       pathRaysCompactedBuffer_0),
    INPUT_BUFFER( 02, uint,                      activePathCountBuffer_0),
    OUTPUT_BUFFER(03, uint,                      bvhStackBuffer),
    OUTPUT_BUFFER(04, Intersection,              pathIntersectionsCompactedBuffer),
//UNITY++
    OUTPUT_BUFFER(05, uint,                      transparentPathRayIndicesCompactedBuffer),
    OUTPUT_BUFFER(06, uint,                      transparentPathRayIndicesCompactedCountBuffer),
    OUTPUT_BUFFER(07, uint,                      totalRaysCastCountBuffer),
    INPUT_BUFFER( 08, MaterialTextureProperties, instanceIdToTransmissionTextureProperties),
    INPUT_BUFFER( 09, float4,                    instanceIdToTransmissionTextureSTs),
    INPUT_BUFFER( 10, MeshDataOffsets,           instanceIdToMeshDataOffsets),
    INPUT_BUFFER( 11, uchar4,                    dynarg_texture_buffer),
    INPUT_BUFFER( 12, uint,                      geometryIndicesBuffer),
    INPUT_BUFFER( 13, float2,                    geometryUV0sBuffer),
    INPUT_VALUE(  14, int,                       lightmapSize),
    INPUT_VALUE(  15, int,                       bounce),
    INPUT_VALUE(  16, int,                       superSamplingMultiplier),
    INPUT_BUFFER( 17, float,                     goldenSample_buffer),
    INPUT_BUFFER( 18, uint,                      sobol_buffer),
    INPUT_BUFFER( 19, SampleDescription,         sampleDescriptionsExpandedBuffer)
    KERNEL_VALIDATOR_BUFFERS_DEF
)
{
    __local uint numTransparentRaySharedMem;
    if (get_local_id(0) == 0)
        numTransparentRaySharedMem = 0;
    barrier(CLK_LOCAL_MEM_FENCE);

    bool atLeastATransparentMaterialWasHit = false;
    __local uint lds_stack[GROUP_SIZE * LDS_STACK_SIZE];
//UNITY--

    uint index = get_global_id(0);
    uint local_index = get_local_id(0);

    // Handle only working subset
    if (index < INDEX_SAFE(activePathCountBuffer_0, 0))
    {
        const ray my_ray = INDEX_SAFE(pathRaysCompactedBuffer_0, index);

        if (ray_is_active(&my_ray))
        {
            const float3 invDir = safe_invdir(my_ray);
            const float3 oxInvDir = -my_ray.o.xyz * invDir;

            // Intersection parametric distance
            float closest_t = my_ray.o.w;

            // Current node address
            uint addr = 0;
            // Current closest address
            uint closest_addr = INVALID_ADDR;

            uint stack_bottom = STACK_SIZE * index;
            uint sptr = stack_bottom;
            uint lds_stack_bottom = local_index * LDS_STACK_SIZE;
            uint lds_sptr = lds_stack_bottom;

            KERNEL_ASSERT(lds_sptr < GROUP_SIZE * LDS_STACK_SIZE);
            lds_stack[lds_sptr++] = INVALID_ADDR;

            //UNITY++
            int  sampleDimension = bounce * PLM_MAX_NUM_SOBOL_DIMENSIONS_PER_BOUNCE;
            const int rayLodLevel = UnpackLODMask(my_ray.extra.x);
            const int rayLodGroup = UnpackLODGroup(my_ray.extra.x);
            int hitLodParam = my_ray.extra.x;
            //UNITY--

            while (addr != INVALID_ADDR)
            {
                const bvh_node node = nodes[addr];

                if (INTERNAL_NODE(node))
                {
                    float2 s0 = fast_intersect_bbox2(
                        node.aabb_left_min_or_v0_and_addr_left.xyz,
                        node.aabb_left_max_or_v1_and_mesh_id.xyz,
                        invDir, oxInvDir, closest_t);
                    float2 s1 = fast_intersect_bbox2(
                        node.aabb_right_min_or_v2_and_addr_right.xyz,
                        node.aabb_right_max_and_prim_id.xyz,
                        invDir, oxInvDir, closest_t);

                    bool traverse_c0 = (s0.x <= s0.y);
                    bool traverse_c1 = (s1.x <= s1.y);
                    bool c1first = traverse_c1 && (s0.x > s1.x);

                    if (traverse_c0 || traverse_c1)
                    {
                        uint deferred = INVALID_ADDR;

                        if (c1first || !traverse_c0)
                        {
                            addr = GetAddrRight(node);
                            deferred = GetAddrLeft(node);
                        }
                        else
                        {
                            addr = GetAddrLeft(node);
                            deferred = GetAddrRight(node);
                        }

                        if (traverse_c0 && traverse_c1)
                        {
                            if (lds_sptr - lds_stack_bottom >= LDS_STACK_SIZE)
                            {
                                for (int i = 1; i < LDS_STACK_SIZE; ++i)
                                {
                                    KERNEL_ASSERT(lds_stack_bottom + i < GROUP_SIZE * LDS_STACK_SIZE);
                                    INDEX_SAFE(bvhStackBuffer, sptr + i) = lds_stack[lds_stack_bottom + i];
                                }

                                sptr += LDS_STACK_SIZE;
                                lds_sptr = lds_stack_bottom + 1;
                            }

                            KERNEL_ASSERT(lds_sptr < GROUP_SIZE * LDS_STACK_SIZE);
                            lds_stack[lds_sptr++] = deferred;
                        }

                        continue;
                    }
                }
                else
                {
                    float t = fast_intersect_triangle(
                        my_ray,
                        node.aabb_left_min_or_v0_and_addr_left.xyz,
                        node.aabb_left_max_or_v1_and_mesh_id.xyz,
                        node.aabb_right_min_or_v2_and_addr_right.xyz,
                        closest_t);

                    if (t < closest_t)
                    {
//UNITY++
                        const int instanceId = GetMeshId(node) - 1;
                        const MaterialTextureProperties matProperty = INDEX_SAFE(instanceIdToTransmissionTextureProperties, instanceId);
                        const int instanceLODMask = UnpackLODMask(matProperty.lodInfo);
                        const int instanceLODGroup = UnpackLODGroup(matProperty.lodInfo);
                        const bool isInstanceHit = IsInstanceHit(instanceLODMask, instanceLODGroup, rayLodLevel, rayLodGroup);

                        if (isInstanceHit)
                        {
                            hitLodParam = (instanceLODMask & 1) ? ((1 << 24) | (NO_LOD_GROUP & ((1<<24)-1))) : hitLodParam;
                            // Evaluate whether we've hit a transparent material
                            bool useTransmission = GetMaterialProperty(matProperty, kMaterialInstanceProperties_UseTransmission);
                            if (useTransmission)
                            {
                                const float3 p = my_ray.o.xyz + t * my_ray.d.xyz;
                                const float2 barycentricCoord = triangle_calculate_barycentrics(
                                    p,
                                    node.aabb_left_min_or_v0_and_addr_left.xyz,
                                    node.aabb_left_max_or_v1_and_mesh_id.xyz,
                                    node.aabb_right_min_or_v2_and_addr_right.xyz);

                                const int primIndex = GetPrimId(node);
                                const float2 geometryUVs = GetUVsAtPrimitiveIntersection(instanceId, primIndex, barycentricCoord, instanceIdToMeshDataOffsets, geometryUV0sBuffer, geometryIndicesBuffer KERNEL_VALIDATOR_BUFFERS);
                                const float2 textureUVs = geometryUVs * INDEX_SAFE(instanceIdToTransmissionTextureSTs, instanceId).xy + INDEX_SAFE(instanceIdToTransmissionTextureSTs, instanceId).zw;
                                const float4 transmission = FetchUChar4TextureFromMaterialAndUVs(dynarg_texture_buffer, textureUVs, matProperty, false, false KERNEL_VALIDATOR_BUFFERS);
                                const float averageTransmission = dot(transmission.xyz, kAverageFactors);
                                const int expandedRayIndex = Ray_GetSampleDescriptionIndex(GET_PTR_SAFE(pathRaysCompactedBuffer_0, index));
                                const SampleDescription sampleDescription = INDEX_SAFE(sampleDescriptionsExpandedBuffer, expandedRayIndex);

                                float rnd = SobolSample(sampleDescription.currentSampleCount, sampleDimension, sobol_buffer KERNEL_VALIDATOR_BUFFERS);

                                int texel_x = sampleDescription.texelIndex % lightmapSize;
                                int texel_y = sampleDescription.texelIndex / lightmapSize;
                                rnd = ApplyCranleyPattersonRotation1D(rnd, texel_x, texel_y, lightmapSize, sampleDimension, goldenSample_buffer KERNEL_VALIDATOR_BUFFERS);

                                // NOTE: This is wrong! The probability of either reflecting or refracting a ray
                                // should depend on the Fresnel of the material. However, since we do not support
                                // any specularity in PVR there is currently no way to query this value, so for now
                                // we use the transmission (texture) albedo.
                                if (rnd >= averageTransmission)
                                {
                                    //Bounce of the transparent material, the material is considered opaque.
                                    closest_t = t;
                                    closest_addr = addr;
                                }
                                else
                                {
                                    //Thought the transparent material, attenuation will need to be collected in an additional pass (specialized occlusion pass)
                                    atLeastATransparentMaterialWasHit = true;
                                    ++sampleDimension;
                                    sampleDimension %= SOBOL_MATRICES_COUNT;
                                }
                            }
                            else
                            {
    //UNITY--
                                closest_t = t;
                                closest_addr = addr;
                            }
                        }
                    }
                }

                KERNEL_ASSERT(lds_sptr - 1 < GROUP_SIZE * LDS_STACK_SIZE);
                addr = lds_stack[--lds_sptr];

                if (addr == INVALID_ADDR && sptr > stack_bottom)
                {
                    sptr -= LDS_STACK_SIZE;
                    for (int i = 1; i < LDS_STACK_SIZE; ++i)
                    {
                        KERNEL_ASSERT(lds_stack_bottom + i < GROUP_SIZE * LDS_STACK_SIZE);
                        lds_stack[lds_stack_bottom + i] = INDEX_SAFE(bvhStackBuffer, sptr + i);
                    }

                    lds_sptr = lds_stack_bottom + LDS_STACK_SIZE - 1;
                    KERNEL_ASSERT(lds_sptr < GROUP_SIZE * LDS_STACK_SIZE);
                    addr = lds_stack[lds_sptr];
                }
            }

            // Check if we have found an intersection
            if (closest_addr != INVALID_ADDR)
            {
                // Calculate hit position
                const bvh_node node = nodes[closest_addr];
                const float3 p = my_ray.o.xyz + closest_t * my_ray.d.xyz;

                // Calculate barycentric coordinates
                const float2 uv = triangle_calculate_barycentrics(
                    p,
                    node.aabb_left_min_or_v0_and_addr_left.xyz,
                    node.aabb_left_max_or_v1_and_mesh_id.xyz,
                    node.aabb_right_min_or_v2_and_addr_right.xyz);

                // Update hit information
                INDEX_SAFE(pathIntersectionsCompactedBuffer, index).primid = GetPrimId(node);
                INDEX_SAFE(pathIntersectionsCompactedBuffer, index).shapeid = GetMeshId(node);
                INDEX_SAFE(pathIntersectionsCompactedBuffer, index).uvwt = (float4)(uv.x, uv.y, 0.0f, closest_t);
                INDEX_SAFE(pathIntersectionsCompactedBuffer, index).padding0 = hitLodParam;
            }
            else
            {
                // Miss here
                INDEX_SAFE(pathIntersectionsCompactedBuffer, index).primid = MISS_MARKER;
                INDEX_SAFE(pathIntersectionsCompactedBuffer, index).shapeid = MISS_MARKER;
                INDEX_SAFE(pathIntersectionsCompactedBuffer, index).padding0 = hitLodParam;
            }
        }
    }

//UNITY++
    //Compact transparent ray that will be process further via the adjustPathThroughputFromIntersection kernel (see below)
    int compactedTransparentRayIndex = -1;
    if (atLeastATransparentMaterialWasHit)
        compactedTransparentRayIndex = atomic_inc(&numTransparentRaySharedMem);

    barrier(CLK_LOCAL_MEM_FENCE);
    if (get_local_id(0) == 0)
    {
        if (numTransparentRaySharedMem)
        {
            atomic_add(GET_PTR_SAFE(totalRaysCastCountBuffer, 0), numTransparentRaySharedMem);
        }
        numTransparentRaySharedMem = atomic_add(GET_PTR_SAFE(transparentPathRayIndicesCompactedCountBuffer, 0), numTransparentRaySharedMem);
    }
    barrier(CLK_LOCAL_MEM_FENCE);
    if (atLeastATransparentMaterialWasHit)
    {
        KERNEL_ASSERT(compactedTransparentRayIndex >= 0);
        INDEX_SAFE(transparentPathRayIndicesCompactedBuffer, numTransparentRaySharedMem + compactedTransparentRayIndex) = index;
    }
//UNITY--
}

//UNITY++
// This kernel is a copy of the occlusion one, but specialized to collect transmission in the ray path.
__attribute__((reqd_work_group_size(GROUP_SIZE, 1, 1)))
__kernel void adjustPathThroughputFromIntersection(
//UNITY--
    INPUT_BUFFER( 00, bvh_node,                  nodes),
    INPUT_BUFFER( 01, ray,                       pathRaysCompactedBuffer_0),
    INPUT_BUFFER( 02, uint,                      activePathCountBuffer_0),
    OUTPUT_BUFFER(03, uint,                      bvhStackBuffer),
    //UNITY++
    OUTPUT_BUFFER(04, float4,                    pathThroughputExpandedBuffer),
    INPUT_BUFFER( 05, Intersection,              pathIntersectionsCompactedBuffer),
    INPUT_BUFFER( 06, uint,                      transparentPathRayIndicesCompactedBuffer),
    INPUT_BUFFER( 07, uint,                      transparentPathRayIndicesCompactedCountBuffer),
    INPUT_BUFFER( 08, MaterialTextureProperties, instanceIdToTransmissionTextureProperties),
    INPUT_BUFFER( 09, float4,                    instanceIdToTransmissionTextureSTs),
    INPUT_BUFFER( 10, MeshDataOffsets,           instanceIdToMeshDataOffsets),
    INPUT_BUFFER( 11, uchar4,                    dynarg_texture_buffer),
    INPUT_BUFFER( 12, uint,                      geometryIndicesBuffer),
    INPUT_BUFFER( 13, float2,                    geometryUV0sBuffer)
    KERNEL_VALIDATOR_BUFFERS_DEF
    //UNITY--
)
{
    uint index = get_global_id(0);
    uint local_index = get_local_id(0);

    //UNITY++
    __local uint lds_stack[GROUP_SIZE * LDS_STACK_SIZE];
    //UNITY--

    // Handle only working subset
    if (index < INDEX_SAFE(transparentPathRayIndicesCompactedCountBuffer, 0))
    {
        const int compactedRayIndex = INDEX_SAFE(transparentPathRayIndicesCompactedBuffer, index);
        const ray my_ray = INDEX_SAFE(pathRaysCompactedBuffer_0, compactedRayIndex);
        KERNEL_ASSERT(ray_is_active(&my_ray));
        //UNITY++
        const Intersection my_intersection = INDEX_SAFE(pathIntersectionsCompactedBuffer, compactedRayIndex);

        const int rayLodLevel = UnpackLODMask(my_ray.extra.x);
        const int rayLodGroup = UnpackLODGroup(my_ray.extra.x);
        //UNITY--

        const float3 invDir = safe_invdir(my_ray);
        const float3 oxInvDir = -my_ray.o.xyz * invDir;

        // Current node address
        uint addr = 0;
        //UNITY++
        // Intersection distance or ray distance if the ray did not stop on a geometry.
        const float closest_t = (my_intersection.primid == MISS_MARKER)? my_ray.o.w : my_intersection.uvwt.w;
        //UNITY--

        uint stack_bottom = STACK_SIZE * index;
        uint sptr = stack_bottom;
        uint lds_stack_bottom = local_index * LDS_STACK_SIZE;
        uint lds_sptr = lds_stack_bottom;

        KERNEL_ASSERT(lds_sptr < GROUP_SIZE * LDS_STACK_SIZE);
        lds_stack[lds_sptr++] = INVALID_ADDR;

        while (addr != INVALID_ADDR)
        {
            const bvh_node node = nodes[addr];

            if (INTERNAL_NODE(node))
            {
                float2 s0 = fast_intersect_bbox2(
                    node.aabb_left_min_or_v0_and_addr_left.xyz,
                    node.aabb_left_max_or_v1_and_mesh_id.xyz,
                    invDir, oxInvDir, closest_t);
                float2 s1 = fast_intersect_bbox2(
                    node.aabb_right_min_or_v2_and_addr_right.xyz,
                    node.aabb_right_max_and_prim_id.xyz,
                    invDir, oxInvDir, closest_t);

                bool traverse_c0 = (s0.x <= s0.y);
                bool traverse_c1 = (s1.x <= s1.y);
                bool c1first = traverse_c1 && (s0.x > s1.x);

                if (traverse_c0 || traverse_c1)
                {
                    uint deferred = INVALID_ADDR;

                    if (c1first || !traverse_c0)
                    {
                        addr = GetAddrRight(node);
                        deferred = GetAddrLeft(node);
                    }
                    else
                    {
                        addr = GetAddrLeft(node);
                        deferred = GetAddrRight(node);
                    }

                    if (traverse_c0 && traverse_c1)
                    {
                        if (lds_sptr - lds_stack_bottom >= LDS_STACK_SIZE)
                        {
                            for (int i = 1; i < LDS_STACK_SIZE; ++i)
                            {
                                KERNEL_ASSERT(lds_stack_bottom + i < GROUP_SIZE * LDS_STACK_SIZE);
                                INDEX_SAFE(bvhStackBuffer, sptr + i) = lds_stack[lds_stack_bottom + i];
                            }

                            sptr += LDS_STACK_SIZE;
                            lds_sptr = lds_stack_bottom + 1;
                        }

                        KERNEL_ASSERT(lds_sptr < GROUP_SIZE * LDS_STACK_SIZE);
                        lds_stack[lds_sptr++] = deferred;
                    }

                    continue;
                }
            }
            else
            {
                float t = fast_intersect_triangle(
                    my_ray,
                    node.aabb_left_min_or_v0_and_addr_left.xyz,
                    node.aabb_left_max_or_v1_and_mesh_id.xyz,
                    node.aabb_right_min_or_v2_and_addr_right.xyz,
                    closest_t);

                if (t < closest_t)
                {
                    //UNITY++
                    const int instanceId = GetMeshId(node) - 1;
                    const MaterialTextureProperties matProperty = INDEX_SAFE(instanceIdToTransmissionTextureProperties, instanceId);
                    const int instanceLODMask = UnpackLODMask(matProperty.lodInfo);
                    const int instanceLODGroup = UnpackLODGroup(matProperty.lodInfo);
                    const bool isInstanceHit = IsInstanceHit(instanceLODMask, instanceLODGroup, rayLodLevel, rayLodGroup);
                    if (isInstanceHit)
                    {
                        // Evaluate transparent material attenuation
                        bool useTransmission = GetMaterialProperty(matProperty, kMaterialInstanceProperties_UseTransmission);
                        if (useTransmission)
                        {
                            const float3 p = my_ray.o.xyz + t * my_ray.d.xyz;
                            const float2 barycentricCoord = triangle_calculate_barycentrics(
                                p,
                                node.aabb_left_min_or_v0_and_addr_left.xyz,
                                node.aabb_left_max_or_v1_and_mesh_id.xyz,
                                node.aabb_right_min_or_v2_and_addr_right.xyz);

                            const int primIndex = GetPrimId(node);
                            const float2 geometryUVs = GetUVsAtPrimitiveIntersection(instanceId, primIndex, barycentricCoord, instanceIdToMeshDataOffsets, geometryUV0sBuffer, geometryIndicesBuffer KERNEL_VALIDATOR_BUFFERS);
                            const float2 textureUVs = geometryUVs * INDEX_SAFE(instanceIdToTransmissionTextureSTs, instanceId).xy + INDEX_SAFE(instanceIdToTransmissionTextureSTs, instanceId).zw;
                            const float4 transmission = FetchUChar4TextureFromMaterialAndUVs(dynarg_texture_buffer, textureUVs, matProperty, false, false KERNEL_VALIDATOR_BUFFERS);
                            const float averageTransmission = dot(transmission.xyz, kAverageFactors);
                            const int expandedRayIndex = Ray_GetSampleDescriptionIndex(GET_PTR_SAFE(pathRaysCompactedBuffer_0, compactedRayIndex));
                            INDEX_SAFE(pathThroughputExpandedBuffer, expandedRayIndex) *= (float4)(transmission.x, transmission.y, transmission.z, averageTransmission);
                        }
                    }
                    //UNITY--
                }
            }
            KERNEL_ASSERT(lds_sptr - 1 < GROUP_SIZE * LDS_STACK_SIZE);
            addr = lds_stack[--lds_sptr];

            if (addr == INVALID_ADDR && sptr > stack_bottom)
            {
                sptr -= LDS_STACK_SIZE;
                for (int i = 1; i < LDS_STACK_SIZE; ++i)
                {
                    KERNEL_ASSERT(lds_stack_bottom + i < GROUP_SIZE * LDS_STACK_SIZE);
                    lds_stack[lds_stack_bottom + i] = INDEX_SAFE(bvhStackBuffer, sptr + i);
                }

                lds_sptr = lds_stack_bottom + LDS_STACK_SIZE - 1;
                KERNEL_ASSERT(lds_sptr < GROUP_SIZE * LDS_STACK_SIZE);
                addr = lds_stack[lds_sptr];
            }
//UNITY++
        }
//UNITY--
    }
}

__attribute__((reqd_work_group_size(GROUP_SIZE, 1, 1)))
__kernel void occludedWithTransmission(
    INPUT_BUFFER( 00, bvh_node,                  nodes),
    INPUT_BUFFER( 01, ray,                       lightRaysCompactedBuffer),
    INPUT_BUFFER( 02, uint,                      lightRaysCountBuffer),
    OUTPUT_BUFFER(03, uint,                      bvhStackBuffer),
//UNITY++
    OUTPUT_BUFFER(04, float4,                    lightOcclusionCompactedBuffer),
    INPUT_BUFFER( 05, MaterialTextureProperties, instanceIdToTransmissionTextureProperties),
    INPUT_BUFFER( 06, float4,                    instanceIdToTransmissionTextureSTs),
    INPUT_BUFFER( 07, MeshDataOffsets,           instanceIdToMeshDataOffsets),
    INPUT_BUFFER( 08, uchar4,                    dynarg_texture_buffer),
    INPUT_BUFFER( 09, uint,                      geometryIndicesBuffer),
    INPUT_BUFFER( 10, float2,                    geometryUV0sBuffer)
    KERNEL_VALIDATOR_BUFFERS_DEF
//UNITY--
)
{
    uint index = get_global_id(0);
    uint local_index = get_local_id(0);
//UNITY++
    __local uint lds_stack[GROUP_SIZE * LDS_STACK_SIZE];
//UNITY--

    // Handle only working subset
    if (index < INDEX_SAFE(lightRaysCountBuffer, 0))
    {
//UNITY++
        // Initialize memory
        INDEX_SAFE(lightOcclusionCompactedBuffer, index) = (float4)(1.0f, 1.0f, 1.0f, 1.0f);
//UNITY--

        const ray my_ray = INDEX_SAFE(lightRaysCompactedBuffer, index);

        if (ray_is_active(&my_ray))
        {
            //UNITY++
            const int rayLodLevel = UnpackLODMask(my_ray.extra.x);
            const int rayLodGroup = UnpackLODGroup(my_ray.extra.x);
            //UNITY--
            const float3 invDir = safe_invdir(my_ray);
            const float3 oxInvDir = -my_ray.o.xyz * invDir;

            // Current node address
            uint addr = 0;
            // Intersection parametric distance
            const float closest_t = my_ray.o.w;

            uint stack_bottom = STACK_SIZE * index;
            uint sptr = stack_bottom;
            uint lds_stack_bottom = local_index * LDS_STACK_SIZE;
            uint lds_sptr = lds_stack_bottom;

            KERNEL_ASSERT(lds_sptr < GROUP_SIZE * LDS_STACK_SIZE);
            lds_stack[lds_sptr++] = INVALID_ADDR;

            while (addr != INVALID_ADDR)
            {
                const bvh_node node = nodes[addr];

                if (INTERNAL_NODE(node))
                {
                    float2 s0 = fast_intersect_bbox2(
                        node.aabb_left_min_or_v0_and_addr_left.xyz,
                        node.aabb_left_max_or_v1_and_mesh_id.xyz,
                        invDir, oxInvDir, closest_t);
                    float2 s1 = fast_intersect_bbox2(
                        node.aabb_right_min_or_v2_and_addr_right.xyz,
                        node.aabb_right_max_and_prim_id.xyz,
                        invDir, oxInvDir, closest_t);

                    bool traverse_c0 = (s0.x <= s0.y);
                    bool traverse_c1 = (s1.x <= s1.y);
                    bool c1first = traverse_c1 && (s0.x > s1.x);

                    if (traverse_c0 || traverse_c1)
                    {
                        uint deferred = INVALID_ADDR;

                        if (c1first || !traverse_c0)
                        {
                            addr = GetAddrRight(node);
                            deferred = GetAddrLeft(node);
                        }
                        else
                        {
                            addr = GetAddrLeft(node);
                            deferred = GetAddrRight(node);
                        }

                        if (traverse_c0 && traverse_c1)
                        {
                            if (lds_sptr - lds_stack_bottom >= LDS_STACK_SIZE)
                            {
                                for (int i = 1; i < LDS_STACK_SIZE; ++i)
                                {
                                    KERNEL_ASSERT(lds_stack_bottom + i < GROUP_SIZE * LDS_STACK_SIZE);
                                    INDEX_SAFE(bvhStackBuffer, sptr + i) = lds_stack[lds_stack_bottom + i];
                                }

                                sptr += LDS_STACK_SIZE;
                                lds_sptr = lds_stack_bottom + 1;
                            }

                            KERNEL_ASSERT(lds_sptr < GROUP_SIZE * LDS_STACK_SIZE);
                            lds_stack[lds_sptr++] = deferred;
                        }

                        continue;
                    }
                }
                else
                {
                    float t = fast_intersect_triangle(
                        my_ray,
                        node.aabb_left_min_or_v0_and_addr_left.xyz,
                        node.aabb_left_max_or_v1_and_mesh_id.xyz,
                        node.aabb_right_min_or_v2_and_addr_right.xyz,
                        closest_t);

                    if (t < closest_t)
                    {
//UNITY++
                        const int instanceId = GetMeshId(node) - 1;
                        const MaterialTextureProperties matProperty = INDEX_SAFE(instanceIdToTransmissionTextureProperties, instanceId);
                        const int instanceLODMask = UnpackLODMask(matProperty.lodInfo);
                        const int instanceLODGroup = UnpackLODGroup(matProperty.lodInfo);
                        const bool isInstanceHit = IsInstanceHit(instanceLODMask, instanceLODGroup, rayLodLevel, rayLodGroup);

                        if (isInstanceHit)
                        {
                            bool castShadows = GetMaterialProperty(matProperty, kMaterialInstanceProperties_CastShadows);
                            if (castShadows)
                            {
                                // Evaluate whether we've hit a transparent material
                                bool useTransmission = GetMaterialProperty(matProperty, kMaterialInstanceProperties_UseTransmission);
                                if (useTransmission)
                                {
                                    const float3 p = my_ray.o.xyz + t * my_ray.d.xyz;
                                    const float2 barycentricCoord = triangle_calculate_barycentrics(
                                        p,
                                        node.aabb_left_min_or_v0_and_addr_left.xyz,
                                        node.aabb_left_max_or_v1_and_mesh_id.xyz,
                                        node.aabb_right_min_or_v2_and_addr_right.xyz);

                                    const int primIndex = GetPrimId(node);
                                    const float2 geometryUVs = GetUVsAtPrimitiveIntersection(instanceId, primIndex, barycentricCoord, instanceIdToMeshDataOffsets, geometryUV0sBuffer, geometryIndicesBuffer KERNEL_VALIDATOR_BUFFERS);
                                    const float2 textureUVs = geometryUVs * INDEX_SAFE(instanceIdToTransmissionTextureSTs, instanceId).xy + INDEX_SAFE(instanceIdToTransmissionTextureSTs, instanceId).zw;
                                    const float4 transmission = FetchUChar4TextureFromMaterialAndUVs(dynarg_texture_buffer, textureUVs, matProperty, false, false KERNEL_VALIDATOR_BUFFERS);
                                    const float averageTransmission = dot(transmission.xyz, kAverageFactors);
                                    INDEX_SAFE(lightOcclusionCompactedBuffer, index) *= (float4)(transmission.x, transmission.y, transmission.z, averageTransmission);
                                    if (INDEX_SAFE(lightOcclusionCompactedBuffer, index).w < TRANSMISSION_THRESHOLD)
                                        return;// fully occluded
                                }
                                else
                                {
                                    INDEX_SAFE(lightOcclusionCompactedBuffer, index) = (float4)(0.0f, 0.0f, 0.0f, 0.0f);
                                    return;// fully occluded
                                }
                            }
                        }
//UNITY--
                    }
                }
                KERNEL_ASSERT(lds_sptr - 1 < GROUP_SIZE * LDS_STACK_SIZE);
                addr = lds_stack[--lds_sptr];

                if (addr == INVALID_ADDR && sptr > stack_bottom)
                {
                    sptr -= LDS_STACK_SIZE;
                    for (int i = 1; i < LDS_STACK_SIZE; ++i)
                    {
                        KERNEL_ASSERT(lds_stack_bottom + i < GROUP_SIZE * LDS_STACK_SIZE);
                        lds_stack[lds_stack_bottom + i] = INDEX_SAFE(bvhStackBuffer, sptr + i);
                    }

                    lds_sptr = lds_stack_bottom + LDS_STACK_SIZE - 1;
                    KERNEL_ASSERT(lds_sptr < GROUP_SIZE * LDS_STACK_SIZE);
                    addr = lds_stack[lds_sptr];
                }
            }
        }
    }
}
