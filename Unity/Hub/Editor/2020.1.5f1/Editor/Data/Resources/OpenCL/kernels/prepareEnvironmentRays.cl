#include "environmentLighting.h"


__kernel void prepareDirectEnvironmentRays(
    // *** output *** //
    OUTPUT_BUFFER( 0, ray,                 lightRaysCompactedBuffer),
    OUTPUT_BUFFER( 1, uint,                lightRaysCountBuffer),
    OUTPUT_BUFFER( 2, uint,                totalRaysCastCountBuffer),
    // *** input *** //
    INPUT_VALUE(   3, int,                 lightmapSize),
    INPUT_VALUE(   4, int,                 envFlags),
    INPUT_VALUE(   5, int,                 numEnvironmentSamples),
    INPUT_BUFFER(  6, PackedNormalOctQuad, envDirectionsBuffer),
    INPUT_BUFFER(  7, float4,              positionsWSBuffer),
    INPUT_BUFFER(  8, float,               goldenSample_buffer),
    INPUT_BUFFER( 9, uint,                 sobol_buffer),
    INPUT_BUFFER( 10, SampleDescription,   sampleDescriptionsExpandedBuffer),
    INPUT_BUFFER( 11, uint,                sampleDescriptionsExpandedCountBuffer),
    INPUT_BUFFER( 12, uint,                instanceIdToLodInfoBuffer)
#   ifndef PROBES
    ,
    INPUT_BUFFER( 13, PackedNormalOctQuad, interpNormalsWSBuffer),
    INPUT_BUFFER( 14, PackedNormalOctQuad, planeNormalsWSBuffer),
    INPUT_VALUE(  15, float,               pushOff),
    INPUT_VALUE(  16, int,                 superSamplingMultiplier)
#   endif
    KERNEL_VALIDATOR_BUFFERS_DEF)
{
    __local uint numRayPreparedSharedMem;
    if (get_local_id(0) == 0)
        numRayPreparedSharedMem = 0;
    barrier(CLK_LOCAL_MEM_FENCE);

    // Prepare ray in private memory
    ray r;
    Ray_SetInactive(&r);

    int expandedRayIdx = get_global_id(0), local_idx;
    const uint sampleDescriptionsExpandedCount = INDEX_SAFE(sampleDescriptionsExpandedCountBuffer, 0);
    if (expandedRayIdx < sampleDescriptionsExpandedCount)
    {
        const SampleDescription sampleDescription = INDEX_SAFE(sampleDescriptionsExpandedBuffer, expandedRayIdx);
#if DISALLOW_RAY_EXPANSION
        if (sampleDescription.texelIndex >= 0)
        {
#endif

#ifndef PROBES
        //From 0 To lightmap_width*lightmap_height * superSamplingMultiplier *superSamplingMultiplier
        const int ssIdx = GetRandomSuperSampledIndex(sampleDescription.texelIndex, lightmapSize, sampleDescription.currentSampleCount, superSamplingMultiplier, sobol_buffer, goldenSample_buffer KERNEL_VALIDATOR_BUFFERS);
#else
        const int ssIdx = sampleDescription.texelIndex;
#endif

        const float4 position = INDEX_SAFE(positionsWSBuffer, ssIdx);

        AssertPositionIsOccupied(position KERNEL_VALIDATOR_BUFFERS);

        //Random numbers
        float3 rand;
        rand.x = SobolSample(sampleDescription.currentSampleCount, 0, sobol_buffer KERNEL_VALIDATOR_BUFFERS);
        rand.y = SobolSample(sampleDescription.currentSampleCount, 1, sobol_buffer KERNEL_VALIDATOR_BUFFERS);
        rand.z = SobolSample(sampleDescription.currentSampleCount, 2, sobol_buffer KERNEL_VALIDATOR_BUFFERS);
        int texel_x = sampleDescription.texelIndex % lightmapSize;
        int texel_y = sampleDescription.texelIndex / lightmapSize;
        rand = ApplyCranleyPattersonRotation3D(rand, texel_x, texel_y, lightmapSize, 0, goldenSample_buffer KERNEL_VALIDATOR_BUFFERS);

#ifdef PROBES
        float3 P = position.xyz;
        float4 D;
        if (UseEnvironmentMIS(envFlags))
            D = GenerateVolumeEnvironmentRayMIS(numEnvironmentSamples, rand, envDirectionsBuffer KERNEL_VALIDATOR_BUFFERS);
        else
            D = GenerateVolumeEnvironmentRay(rand.xy);
        const int packedLODInfo = PackLODInfo(NO_LOD_MASK, NO_LOD_GROUP);
#else
        float3 interpNormal = DecodeNormal(INDEX_SAFE(interpNormalsWSBuffer, ssIdx));
        float3 planeNormal  = DecodeNormal(INDEX_SAFE(planeNormalsWSBuffer, ssIdx));

        float4 D;
        if (UseEnvironmentMIS(envFlags))
            D = GenerateSurfaceEnvironmentRayMIS(numEnvironmentSamples, interpNormal, planeNormal, rand, envDirectionsBuffer KERNEL_VALIDATOR_BUFFERS);
        else
            D = GenerateSurfaceEnvironmentRay(interpNormal, planeNormal, rand.xy);

        float3 P = position.xyz + planeNormal * pushOff;
        const int instanceId = (int)(floor(position.w));
        const int packedLODInfo = INDEX_SAFE(instanceIdToLodInfoBuffer, instanceId);
#endif
        if (D.w != 0.0f)
        {
            Ray_Init(&r, P, D.xyz, DEFAULT_RAY_LENGTH, D.w, packedLODInfo);

            // Set the index so we can map to the originating texel/probe
            Ray_SetSourceIndex(&r, sampleDescription.texelIndex);
        }
#if DISALLOW_RAY_EXPANSION
        }
#endif
    }

    // Threads synchronization for compaction
    if (Ray_IsActive_Private(&r))
    {
        local_idx = atomic_inc(&numRayPreparedSharedMem);
    }
    barrier(CLK_LOCAL_MEM_FENCE);
    if (get_local_id(0) == 0)
    {
        atomic_add(GET_PTR_SAFE(totalRaysCastCountBuffer, 0), numRayPreparedSharedMem);
        int numRayToAdd = numRayPreparedSharedMem;
#if DISALLOW_LIGHT_RAYS_COMPACTION
        numRayToAdd = get_local_size(0);
#endif
        numRayPreparedSharedMem = atomic_add(GET_PTR_SAFE(lightRaysCountBuffer, 0), numRayToAdd);
    }
    barrier(CLK_LOCAL_MEM_FENCE);

    int compactedIndex = numRayPreparedSharedMem + local_idx;
#if DISALLOW_LIGHT_RAYS_COMPACTION
    compactedIndex = expandedRayIdx;
#else
    // Write the ray out to memory
    if (Ray_IsActive_Private(&r))
#endif
    {
        Ray_SetSampleDescriptionIndex(&r, expandedRayIdx);
        INDEX_SAFE(lightRaysCompactedBuffer, compactedIndex) = r;
    }
}

__kernel void prepareIndirectEnvironmentRays(
    //*** output ***
    OUTPUT_BUFFER( 0, ray,                 lightRaysCompactedBuffer),
    OUTPUT_BUFFER( 1, uint,                lightRaysCountBuffer),
    OUTPUT_BUFFER( 2, uint,                totalRaysCastCountBuffer),
    OUTPUT_BUFFER( 3, uint,                lightRayIndexToPathRayIndexCompactedBuffer),
    //*** input ***
    INPUT_BUFFER(  4, ray,                 pathRaysCompactedBuffer_0),
    INPUT_BUFFER(  5, uint,                activePathCountBuffer_0),
    INPUT_BUFFER(  6, Intersection,        pathIntersectionsCompactedBuffer),
    INPUT_BUFFER(  7, PackedNormalOctQuad, pathLastPlaneNormalCompactedBuffer),
    INPUT_BUFFER(  8, unsigned char,       pathLastNormalFacingTheRayCompactedBuffer),
    INPUT_BUFFER(  9, PackedNormalOctQuad, pathLastInterpNormalCompactedBuffer),
    INPUT_BUFFER( 10, float,               goldenSample_buffer),
    INPUT_BUFFER( 11, uint,                sobol_buffer),
    INPUT_BUFFER( 12, PackedNormalOctQuad, envDirectionsBuffer),
    INPUT_BUFFER( 13, SampleDescription,   sampleDescriptionsExpandedBuffer),
    INPUT_VALUE(  14, int,                 envFlags),
    INPUT_VALUE(  15, int,                 numEnvironmentSamples),
    INPUT_VALUE(  16, int,                 lightmapSize),
    INPUT_VALUE(  17, int,                 bounce),
    INPUT_VALUE(  18, float,               pushOff),
    INPUT_VALUE(  19, int,                 superSamplingMultiplier)
    KERNEL_VALIDATOR_BUFFERS_DEF
)
{
    // Initialize local memory
    __local uint numRayPreparedSharedMem;
    if (get_local_id(0) == 0)
        numRayPreparedSharedMem = 0;
    barrier(CLK_LOCAL_MEM_FENCE);

    // Prepare ray in private memory
    ray r;
    Ray_SetInactive(&r);

    // Should we prepare a light ray?
    int compactedPathRayIdx = get_global_id(0), local_idx;
    bool shouldPrepareNewRay = compactedPathRayIdx < INDEX_SAFE(activePathCountBuffer_0, 0);

#if DISALLOW_PATH_RAYS_COMPACTION
    if (Ray_IsInactive(GET_PTR_SAFE(pathRaysCompactedBuffer_0, compactedPathRayIdx)))
    {
        shouldPrepareNewRay = false;
    }
#endif

    if (shouldPrepareNewRay)
    {
        KERNEL_ASSERT(Ray_IsActive(GET_PTR_SAFE(pathRaysCompactedBuffer_0, compactedPathRayIdx)));

        // We did not hit anything, no light ray.
        const bool pathRayHitSomething = INDEX_SAFE(pathIntersectionsCompactedBuffer, compactedPathRayIdx).shapeid > 0;
        shouldPrepareNewRay &= pathRayHitSomething;

        // We hit an invalid triangle (from the back, no double sided GI), no light ray.
        const bool isNormalFacingTheRay = INDEX_SAFE(pathLastNormalFacingTheRayCompactedBuffer, compactedPathRayIdx);
        shouldPrepareNewRay &= isNormalFacingTheRay;
    }

    // Prepare the shadow ray
    if (shouldPrepareNewRay)
    {
        int sampleDescriptionIdx = Ray_GetSampleDescriptionIndex(GET_PTR_SAFE(pathRaysCompactedBuffer_0, compactedPathRayIdx));
        const SampleDescription sampleDescription = INDEX_SAFE(sampleDescriptionsExpandedBuffer, sampleDescriptionIdx);

        // Get random numbers
        int baseDimension = bounce * PLM_MAX_NUM_SOBOL_DIMENSIONS_PER_BOUNCE;
        float3 sample3D;
        sample3D.x = SobolSample(sampleDescription.currentSampleCount, baseDimension + 0, sobol_buffer KERNEL_VALIDATOR_BUFFERS);
        sample3D.y = SobolSample(sampleDescription.currentSampleCount, baseDimension + 1, sobol_buffer KERNEL_VALIDATOR_BUFFERS);
        sample3D.z = SobolSample(sampleDescription.currentSampleCount, baseDimension + 2, sobol_buffer KERNEL_VALIDATOR_BUFFERS);

        int texel_x = sampleDescription.texelIndex % lightmapSize;
        int texel_y = sampleDescription.texelIndex / lightmapSize;
        sample3D = ApplyCranleyPattersonRotation3D(sample3D, texel_x, texel_y, lightmapSize, baseDimension, goldenSample_buffer KERNEL_VALIDATOR_BUFFERS);

        float3 planeNormal  = DecodeNormal(INDEX_SAFE(pathLastPlaneNormalCompactedBuffer, compactedPathRayIdx));
        float3 interpNormal = DecodeNormal(INDEX_SAFE(pathLastInterpNormalCompactedBuffer, compactedPathRayIdx));

        float4 D;
        if (UseEnvironmentMIS(envFlags))
            D = GenerateSurfaceEnvironmentRayMIS(numEnvironmentSamples, interpNormal, planeNormal, sample3D, envDirectionsBuffer KERNEL_VALIDATOR_BUFFERS);
        else
            D = GenerateSurfaceEnvironmentRay(interpNormal, planeNormal, sample3D.xy);

        // TODO(RadeonRays) gboisse: we're generating some NaN directions somehow, fix it!!
        if (D.w != 0.0f && !any(isnan(D)))
        {
            float  t  = INDEX_SAFE(pathIntersectionsCompactedBuffer, compactedPathRayIdx).uvwt.w;
            float3 P  = INDEX_SAFE(pathRaysCompactedBuffer_0, compactedPathRayIdx).o.xyz + INDEX_SAFE(pathRaysCompactedBuffer_0, compactedPathRayIdx).d.xyz * t;
                   P += planeNormal * pushOff;

            // propagate potentially fixed up lod param from the intersection
            const int instanceLodInfo = INDEX_SAFE(pathIntersectionsCompactedBuffer, compactedPathRayIdx).padding0;

            Ray_Init(&r, P, D.xyz, DEFAULT_RAY_LENGTH, D.w, instanceLodInfo);

            // Set the index so we can map to the originating texel/probe
            Ray_SetSourceIndex(&r, sampleDescription.texelIndex);
            Ray_SetSampleDescriptionIndex(&r, sampleDescriptionIdx);
        }
    }

    // Threads synchronization for compaction
    if (Ray_IsActive_Private(&r))
    {
        local_idx = atomic_inc(&numRayPreparedSharedMem);
    }
    barrier(CLK_LOCAL_MEM_FENCE);
    if (get_local_id(0) == 0)
    {
        atomic_add(GET_PTR_SAFE(totalRaysCastCountBuffer, 0), numRayPreparedSharedMem);
        int numRayToAdd = numRayPreparedSharedMem;
#if DISALLOW_LIGHT_RAYS_COMPACTION
        numRayToAdd = get_local_size(0);
#endif
        numRayPreparedSharedMem = atomic_add(GET_PTR_SAFE(lightRaysCountBuffer, 0), numRayToAdd);
    }
    barrier(CLK_LOCAL_MEM_FENCE);

    int compactedLightRayIndex = numRayPreparedSharedMem + local_idx;
#if DISALLOW_LIGHT_RAYS_COMPACTION
    compactedLightRayIndex = compactedPathRayIdx;
#else
    // Write the ray out to memory
    if (Ray_IsActive_Private(&r))
#endif
    {
        INDEX_SAFE(lightRaysCompactedBuffer, compactedLightRayIndex) = r;
        INDEX_SAFE(lightRayIndexToPathRayIndexCompactedBuffer, compactedLightRayIndex) = compactedPathRayIdx;
    }
}
