#include "commonCL.h"
#include "directLighting.h"

static int GetCellIndex(float3 position, float3 gridBias, float3 gridScale, int3 gridDims)
{
    const int3 cellPos = clamp(convert_int3(position * gridScale + gridBias), (int3)0, gridDims - 1);
    return cellPos.x + cellPos.y * gridDims.x + cellPos.z * gridDims.x * gridDims.y;
}

//Preparing shadowRays for direct lighting.
__kernel void prepareLightRays(
    //outputs
    OUTPUT_BUFFER(00, ray,                 lightRaysCompactedBuffer),
    OUTPUT_BUFFER(01, LightSample,         lightSamplesCompactedBuffer),
    OUTPUT_BUFFER(02, uint,                totalRaysCastCountBuffer),
    OUTPUT_BUFFER(03, uint,                lightRaysCountBuffer),
    //inputs
    INPUT_BUFFER( 04, float4,              positionsWSBuffer),
    INPUT_BUFFER( 05, int,                 directLightsIndexBuffer),
    INPUT_BUFFER( 06, LightBuffer,         directLightsBuffer),
    INPUT_BUFFER( 07, int,                 directLightsOffsetBuffer),
    INPUT_BUFFER( 08, int,                 directLightsCountPerCellBuffer),
    INPUT_VALUE(  09, float3,              lightGridBias),
    INPUT_VALUE(  10, float3,              lightGridScale),
    INPUT_VALUE(  11, int3,                lightGridDims),
    INPUT_VALUE(  12, int,                 lightmapSize),
    INPUT_BUFFER( 13, float,               goldenSample_buffer),
    INPUT_BUFFER( 14, uint,                sobol_buffer),
    INPUT_BUFFER( 15, uint,                sampleDescriptionsExpandedCountBuffer),
    INPUT_VALUE(  16, int,                 lightIndexInCell),
    INPUT_BUFFER( 17, SampleDescription,   sampleDescriptionsExpandedBuffer),
    INPUT_BUFFER( 18, uint,                instanceIdToLodInfoBuffer)
#ifndef PROBES
    ,
    INPUT_BUFFER( 19, PackedNormalOctQuad, interpNormalsWSBuffer),
    INPUT_VALUE(  20, float,               pushOff),
    INPUT_VALUE(  21, int,                 superSamplingMultiplier)
#endif
    KERNEL_VALIDATOR_BUFFERS_DEF
)
{
    __local uint numRayPreparedSharedMem;
    if (get_local_id(0) == 0)
        numRayPreparedSharedMem = 0;
    barrier(CLK_LOCAL_MEM_FENCE);

    // Prepare ray in private memory
    ray r;
    Ray_SetInactive(&r);
    LightSample lightSample;
    lightSample.lightIdx = -1;

    int expandedRayIdx = get_global_id(0), local_idx;
    const uint sampleDescriptionsExpandedCount = INDEX_SAFE(sampleDescriptionsExpandedCountBuffer, 0);
    if (expandedRayIdx < sampleDescriptionsExpandedCount)
    {
        const SampleDescription sampleDescription = INDEX_SAFE(sampleDescriptionsExpandedBuffer, expandedRayIdx);
#if DISALLOW_RAY_EXPANSION
        if (sampleDescription.texelIndex >= 0)
        {
#endif

#ifndef PROBES
        //From 0 To lightmap_width*lightmap_height * superSamplingMultiplier *superSamplingMultiplier
        const int ssIdx = GetRandomSuperSampledIndex(sampleDescription.texelIndex, lightmapSize, sampleDescription.currentSampleCount, superSamplingMultiplier, sobol_buffer, goldenSample_buffer KERNEL_VALIDATOR_BUFFERS);
#else
        const int ssIdx = sampleDescription.texelIndex;
#endif
        float4 position = INDEX_SAFE(positionsWSBuffer, ssIdx);
        AssertPositionIsOccupied(position KERNEL_VALIDATOR_BUFFERS);

        // Directional light pass
        // The direction light index is encoded in the lightIndexInCell in negative numbers offseted by 1
        // The offset of 1 allows us to always have a negative lightIndexInCell for the dir light pass, especially with dir_light_id == 0 which becomes -1
        // Before calling this kernel we then do:  lightIndexInCell = -directionalLightIndex - 1;
        if (lightIndexInCell < 0)
        {
            int directionalLightIndex = -lightIndexInCell - 1;

            lightSample.lightPdf = 1.0f;
            lightSample.lightIdx = directionalLightIndex;
        }
        else
        {
            const int cellIdx = GetCellIndex(position.xyz, lightGridBias, lightGridScale, lightGridDims);
            const int lightCountInCell = INDEX_SAFE(directLightsCountPerCellBuffer, cellIdx);

            // Lights in light grid pass
            // If we already did all the lights in the cell bail out
            if (lightIndexInCell < lightCountInCell)
            {
                // Select a light in a round robin fashion (no need for pdf)
                const int lightCellOffset = INDEX_SAFE(directLightsOffsetBuffer, cellIdx) + lightIndexInCell;
                lightSample.lightPdf = 1.0f;
                lightSample.lightIdx = INDEX_SAFE(directLightsIndexBuffer, lightCellOffset);
            }
        }

        if(lightSample.lightIdx >=0)
        {
            const LightBuffer light = INDEX_SAFE(directLightsBuffer, lightSample.lightIdx);

            // Get random numbers
            float2 sample2D;
            sample2D.x = SobolSample(sampleDescription.currentSampleCount, 0, sobol_buffer KERNEL_VALIDATOR_BUFFERS);
            sample2D.y = SobolSample(sampleDescription.currentSampleCount, 1, sobol_buffer KERNEL_VALIDATOR_BUFFERS);

            int texel_x = sampleDescription.texelIndex % lightmapSize;
            int texel_y = sampleDescription.texelIndex / lightmapSize;
            sample2D = ApplyCranleyPattersonRotation2D(sample2D, texel_x, texel_y, lightmapSize, 0, goldenSample_buffer KERNEL_VALIDATOR_BUFFERS);

            // Generate the shadow ray. This might be an inactive ray (in case of back facing surfaces or out of cone angle for spots).
#ifdef PROBES
            float3 notUsed3 = (float3)(0, 0, 0);
            const int packedNoLODInfo = PackLODInfo(NO_LOD_MASK, NO_LOD_GROUP);
            PrepareShadowRay(light, sample2D, position.xyz, notUsed3, 0, false, &r, packedNoLODInfo);
#else
            const int instanceId = (int)(floor(position.w));
            const int instanceLodInfo = INDEX_SAFE(instanceIdToLodInfoBuffer, instanceId);
            float3 normal = DecodeNormal(INDEX_SAFE(interpNormalsWSBuffer, ssIdx));
            PrepareShadowRay(light, sample2D, position.xyz, normal, pushOff, false, &r, instanceLodInfo);
#endif
            // Set the index so we can map to the originating texel/probe
            Ray_SetSourceIndex(&r, sampleDescription.texelIndex);
        }
#if DISALLOW_RAY_EXPANSION
        }
#endif
    }

    // Threads synchronization for compaction
    if (Ray_IsActive_Private(&r))
    {
        local_idx = atomic_inc(&numRayPreparedSharedMem);
    }
    barrier(CLK_LOCAL_MEM_FENCE);
    if (get_local_id(0) == 0)
    {
        atomic_add(GET_PTR_SAFE(totalRaysCastCountBuffer, 0), numRayPreparedSharedMem);
        int numRayToAdd = numRayPreparedSharedMem;
#if DISALLOW_LIGHT_RAYS_COMPACTION
        numRayToAdd = get_local_size(0);
#endif
        numRayPreparedSharedMem = atomic_add(GET_PTR_SAFE(lightRaysCountBuffer, 0), numRayToAdd);
    }
    barrier(CLK_LOCAL_MEM_FENCE);

    int compactedIndex = numRayPreparedSharedMem + local_idx;
#if DISALLOW_LIGHT_RAYS_COMPACTION
    compactedIndex = expandedRayIdx;
#else
    // Write the ray out to memory
    if (Ray_IsActive_Private(&r))
#endif
    {
        INDEX_SAFE(lightSamplesCompactedBuffer, compactedIndex) = lightSample;
        Ray_SetSampleDescriptionIndex(&r, expandedRayIdx);
        INDEX_SAFE(lightRaysCompactedBuffer, compactedIndex) = r;
    }
}

//Preparing shadowRays for indirect lighting.
__kernel void prepareLightRaysFromBounce(
    INPUT_BUFFER( 00, LightBuffer,         indirectLightsBuffer),
    INPUT_BUFFER( 01, int,                 indirectLightsOffsetBuffer),
    INPUT_BUFFER( 02, int,                 indirectLightsIndexBuffer),
    INPUT_BUFFER( 03, int,                 indirectLightsDistributionBuffer),
    INPUT_BUFFER( 04, int,                 indirectLightDistributionOffsetBuffer),
    INPUT_BUFFER( 05, bool,                usePowerSamplingBuffer),
    INPUT_VALUE(  06, float3,              lightGridBias),
    INPUT_VALUE(  07, float3,              lightGridScale),
    INPUT_VALUE(  08, int3,                lightGridDims),
    INPUT_BUFFER( 09, ray,                 pathRaysCompactedBuffer_0),
    INPUT_BUFFER( 10, Intersection,        pathIntersectionsCompactedBuffer),
    INPUT_BUFFER( 11, PackedNormalOctQuad, pathLastInterpNormalCompactedBuffer),
    INPUT_BUFFER( 12, unsigned char,       pathLastNormalFacingTheRayCompactedBuffer),
    INPUT_VALUE(  13, int,                 lightmapSize),
    INPUT_VALUE(  14, int,                 bounce),
    INPUT_BUFFER( 15, float,               goldenSample_buffer),
    INPUT_BUFFER( 16, uint,                sobol_buffer),
    INPUT_VALUE(  17, float,               pushOff),
    INPUT_BUFFER( 18, uint,                activePathCountBuffer_0),
    INPUT_BUFFER( 19, SampleDescription,   sampleDescriptionsExpandedBuffer),
    OUTPUT_BUFFER(20, ray,                 lightRaysCompactedBuffer),
    OUTPUT_BUFFER(21, LightSample,         lightSamplesCompactedBuffer),
    OUTPUT_BUFFER(22, uint,                lightRayIndexToPathRayIndexCompactedBuffer),
    OUTPUT_BUFFER(23, uint,                totalRaysCastCountBuffer),
    OUTPUT_BUFFER(24, uint,                lightRaysCountBuffer),
    INPUT_VALUE(  25, int,                 superSamplingMultiplier),
    INPUT_VALUE(  26, int,                 directionalLightIndex)
    KERNEL_VALIDATOR_BUFFERS_DEF
)
{
    // Initialize local memory
    __local uint numRayPreparedSharedMem;
    if (get_local_id(0) == 0)
        numRayPreparedSharedMem = 0;
    barrier(CLK_LOCAL_MEM_FENCE);

    // Prepare ray in private memory
    ray r;
    Ray_SetInactive(&r);
    LightSample lightSample;

    // Should we prepare a light ray?
    int compactedPathRayIdx = get_global_id(0), local_idx;
    bool shouldPrepareNewRay = compactedPathRayIdx < INDEX_SAFE(activePathCountBuffer_0, 0);

#if DISALLOW_PATH_RAYS_COMPACTION
    if (Ray_IsInactive(GET_PTR_SAFE(pathRaysCompactedBuffer_0, compactedPathRayIdx)))
    {
        shouldPrepareNewRay = false;
    }
#endif

    if (shouldPrepareNewRay)
    {
        KERNEL_ASSERT(Ray_IsActive(GET_PTR_SAFE(pathRaysCompactedBuffer_0, compactedPathRayIdx)));

        // We did not hit anything, no light ray.
        const bool pathRayHitSomething = INDEX_SAFE(pathIntersectionsCompactedBuffer, compactedPathRayIdx).shapeid > 0;
        shouldPrepareNewRay &= pathRayHitSomething;

        // We hit an invalid triangle (from the back, no double sided GI), no light ray.
        const bool isNormalFacingTheRay = INDEX_SAFE(pathLastNormalFacingTheRayCompactedBuffer, compactedPathRayIdx);
        shouldPrepareNewRay &= isNormalFacingTheRay;
    }

    // Prepare the shadow ray
    if (shouldPrepareNewRay)
    {
        const float3 surfaceNormal = DecodeNormal(INDEX_SAFE(pathLastInterpNormalCompactedBuffer, compactedPathRayIdx));
        const float t = INDEX_SAFE(pathIntersectionsCompactedBuffer, compactedPathRayIdx).uvwt.w;
        const float3 surfacePosition = INDEX_SAFE(pathRaysCompactedBuffer_0, compactedPathRayIdx).o.xyz + t * INDEX_SAFE(pathRaysCompactedBuffer_0, compactedPathRayIdx).d.xyz;

        // Retrieve the light distribution at the shading site
        const int cellIdx = GetCellIndex(surfacePosition, lightGridBias, lightGridScale, lightGridDims);
        __global const int *const restrict lightDistributionPtr = GET_PTR_SAFE(indirectLightsDistributionBuffer, INDEX_SAFE(indirectLightDistributionOffsetBuffer, cellIdx));
        const int lightDistribution = *lightDistributionPtr; // safe to dereference, as GET_PTR_SAFE above does the validation

        // If there is no light in the cell, or not doing the directional light pass bail out
        if (lightDistribution || directionalLightIndex>=0)
        {
            // propagate potentially fixed up lod param from the intersection
            const int instanceLodInfo = INDEX_SAFE(pathIntersectionsCompactedBuffer, compactedPathRayIdx).padding0;

            int expandedRayIdx = Ray_GetSampleDescriptionIndex(GET_PTR_SAFE(pathRaysCompactedBuffer_0, compactedPathRayIdx));
            const SampleDescription sampleDescription = INDEX_SAFE(sampleDescriptionsExpandedBuffer, expandedRayIdx);

            // Get random numbers
            int baseDimension = bounce * PLM_MAX_NUM_SOBOL_DIMENSIONS_PER_BOUNCE;
            float3 sample3D;
            sample3D.x = SobolSample(sampleDescription.currentSampleCount, baseDimension + 0, sobol_buffer KERNEL_VALIDATOR_BUFFERS);
            sample3D.y = SobolSample(sampleDescription.currentSampleCount, baseDimension + 1, sobol_buffer KERNEL_VALIDATOR_BUFFERS);
            sample3D.z = SobolSample(sampleDescription.currentSampleCount, baseDimension + 2, sobol_buffer KERNEL_VALIDATOR_BUFFERS);

            int texel_x = sampleDescription.texelIndex % lightmapSize;
            int texel_y = sampleDescription.texelIndex / lightmapSize;
            sample3D = ApplyCranleyPattersonRotation3D(sample3D, texel_x, texel_y, lightmapSize, baseDimension, goldenSample_buffer KERNEL_VALIDATOR_BUFFERS);


            // Directional light pass
            if (directionalLightIndex >= 0)
            {
                lightSample.lightIdx = directionalLightIndex;
                lightSample.lightPdf = 1.0f;
            }
            else
            {
                // Select a light
                if (INDEX_SAFE(usePowerSamplingBuffer, UsePowerSamplingBufferSlot_PowerSampleEnabled))
                {
                    float selectionPdf;
                    const int lightCellOffset = INDEX_SAFE(indirectLightsOffsetBuffer, cellIdx) + Distribution1D_SampleDiscrete(sample3D.z, lightDistributionPtr, &selectionPdf);
                    lightSample.lightIdx = INDEX_SAFE(indirectLightsIndexBuffer, lightCellOffset);
                    lightSample.lightPdf = selectionPdf;
                }
                else
                {
                    const int offset = min(lightDistribution - 1, (int)(sample3D.z * (float)lightDistribution));
                    const int lightCellOffset = INDEX_SAFE(indirectLightsOffsetBuffer, cellIdx) + offset;
                    lightSample.lightIdx = INDEX_SAFE(indirectLightsIndexBuffer, lightCellOffset);
                    lightSample.lightPdf = 1.0f / lightDistribution;
                }
            }

            // Generate the shadow ray
            const LightBuffer light = INDEX_SAFE(indirectLightsBuffer, lightSample.lightIdx);
            PrepareShadowRay(light, sample3D.xy, surfacePosition, surfaceNormal, pushOff, false, &r, instanceLodInfo);

            // Set the index so we can map to the originating texel/probe
            Ray_SetSourceIndex(&r, sampleDescription.texelIndex);
            Ray_SetSampleDescriptionIndex(&r, expandedRayIdx);
        }
    }

    // Threads synchronization for compaction
    if (Ray_IsActive_Private(&r))
    {
        local_idx = atomic_inc(&numRayPreparedSharedMem);
    }
    barrier(CLK_LOCAL_MEM_FENCE);
    if (get_local_id(0) == 0)
    {
        atomic_add(GET_PTR_SAFE(totalRaysCastCountBuffer, 0), numRayPreparedSharedMem);
        int numRayToAdd = numRayPreparedSharedMem;
#if DISALLOW_LIGHT_RAYS_COMPACTION
        numRayToAdd = get_local_size(0);
#endif
        numRayPreparedSharedMem = atomic_add(GET_PTR_SAFE(lightRaysCountBuffer, 0), numRayToAdd);
    }
    barrier(CLK_LOCAL_MEM_FENCE);

    int compactedLightRayIndex = numRayPreparedSharedMem + local_idx;
#if DISALLOW_LIGHT_RAYS_COMPACTION
    compactedLightRayIndex = compactedPathRayIdx;
#else
    // Write the ray out to memory
    if (Ray_IsActive_Private(&r))
#endif
    {
        INDEX_SAFE(lightSamplesCompactedBuffer, compactedLightRayIndex) = lightSample;
        INDEX_SAFE(lightRaysCompactedBuffer, compactedLightRayIndex) = r;
        INDEX_SAFE(lightRayIndexToPathRayIndexCompactedBuffer, compactedLightRayIndex) = compactedPathRayIdx;
    }
}
