#include "commonCL.h"

__kernel void preparePathRays(
    OUTPUT_BUFFER(00, ray,               pathRaysCompactedBuffer_0),
    OUTPUT_BUFFER(01, uint,              activePathCountBuffer_0),
    OUTPUT_BUFFER(02, uint,              totalRaysCastCountBuffer),
    OUTPUT_BUFFER(03, float4,            originalRaysExpandedBuffer),
    INPUT_BUFFER( 04, float4,            positionsWSBuffer),
    INPUT_VALUE(  05, int,               lightmapSize),
    INPUT_VALUE(  06, int,               bounce),
    INPUT_BUFFER( 07, uint,              sobol_buffer),
    INPUT_BUFFER( 08, float,             goldenSample_buffer),
    INPUT_BUFFER( 09, SampleDescription, sampleDescriptionsExpandedBuffer),
    INPUT_BUFFER( 10, uint,              sampleDescriptionsExpandedCountBuffer),
    INPUT_BUFFER( 11, uint,              instanceIdToLodInfoBuffer)
#ifndef PROBES
    ,
    INPUT_BUFFER( 12, PackedNormalOctQuad, interpNormalsWSBuffer),
    INPUT_BUFFER( 13, PackedNormalOctQuad, planeNormalsWSBuffer),
    INPUT_VALUE(  14, float,               pushOff),
    INPUT_VALUE(  15, int,                 superSamplingMultiplier)
#endif
    KERNEL_VALIDATOR_BUFFERS_DEF
)
{
    __local uint numRayPreparedSharedMem;
    if (get_local_id(0) == 0)
        numRayPreparedSharedMem = 0;
    barrier(CLK_LOCAL_MEM_FENCE);

    // Prepare ray in private memory
    ray r;
    Ray_SetInactive(&r);

    int expandedPathRayIdx = get_global_id(0), local_idx;
    const uint sampleDescriptionsExpandedCount = INDEX_SAFE(sampleDescriptionsExpandedCountBuffer, 0);
    if (expandedPathRayIdx < sampleDescriptionsExpandedCount)
    {
        const SampleDescription sampleDescription = INDEX_SAFE(sampleDescriptionsExpandedBuffer, expandedPathRayIdx);
#if DISALLOW_RAY_EXPANSION
        if (sampleDescription.texelIndex >= 0)
        {
#endif

#ifndef PROBES
        //From 0 To lightmap_width*lightmap_height * superSamplingMultiplier *superSamplingMultiplier
        const int ssIdx = GetRandomSuperSampledIndex(sampleDescription.texelIndex, lightmapSize, sampleDescription.currentSampleCount, superSamplingMultiplier, sobol_buffer, goldenSample_buffer KERNEL_VALIDATOR_BUFFERS);
#else
        const int ssIdx = sampleDescription.texelIndex;
#endif
        // Get random numbers
        float2 sample2D;

        //first bounce uses dimension 0 and 1
        sample2D.x = SobolSample(sampleDescription.currentSampleCount, 0, sobol_buffer KERNEL_VALIDATOR_BUFFERS);
        sample2D.y = SobolSample(sampleDescription.currentSampleCount, 1, sobol_buffer KERNEL_VALIDATOR_BUFFERS);

        //first bounce, the base dimension is 0
        int texel_x = sampleDescription.texelIndex % lightmapSize;
        int texel_y = sampleDescription.texelIndex / lightmapSize;
        sample2D = ApplyCranleyPattersonRotation2D(sample2D, texel_x, texel_y, lightmapSize, 0, goldenSample_buffer KERNEL_VALIDATOR_BUFFERS);

        float4 position = INDEX_SAFE(positionsWSBuffer, ssIdx);
        AssertPositionIsOccupied(position KERNEL_VALIDATOR_BUFFERS);

#ifdef PROBES
        float3 D = SphereSample(sample2D);
        const float3 P = position.xyz;
        const int instanceLodInfo = PackLODInfo(NO_LOD_MASK, NO_LOD_GROUP);
#else
        const float3 interpNormal = DecodeNormal(INDEX_SAFE(interpNormalsWSBuffer, ssIdx));
        //Map to cosine weighted hemisphere directed toward normal
        float3 b1;
        float3 b2;
        CreateOrthoNormalBasis(interpNormal, &b1, &b2);
        float3 hamDir = HemisphereCosineSample(sample2D);
        float3 D = hamDir.x*b1 + hamDir.y*b2 + hamDir.z*interpNormal;

        const float3 planeNormal = DecodeNormal(INDEX_SAFE(planeNormalsWSBuffer, ssIdx));
        const float3 P = position.xyz + planeNormal * pushOff;

        // if plane normal is too different from interpolated normal, the hemisphere orientation will be wrong and the sample could be under the surface.
        float dotVal = dot(D, planeNormal);
        const int instanceId = (int)(floor(position.w));
        const int instanceLodInfo = INDEX_SAFE(instanceIdToLodInfoBuffer, instanceId);
        if (dotVal > 0.0f && !isnan(dotVal))
#endif
        {
            Ray_Init(&r, P, D, DEFAULT_RAY_LENGTH, 0.f, instanceLodInfo);

            // Set the index so we can map to the originating texel/probe
            Ray_SetSourceIndex(&r, sampleDescription.texelIndex);
        }
#if DISALLOW_RAY_EXPANSION
        }
#endif
    }

    // Threads synchronization for compaction
    if (Ray_IsActive_Private(&r))
    {
        local_idx = atomic_inc(&numRayPreparedSharedMem);
    }
    barrier(CLK_LOCAL_MEM_FENCE);
    if (get_local_id(0) == 0)
    {
        atomic_add(GET_PTR_SAFE(totalRaysCastCountBuffer, 0), numRayPreparedSharedMem);
        int numRayToAdd = numRayPreparedSharedMem;
#if DISALLOW_PATH_RAYS_COMPACTION
        numRayToAdd = get_local_size(0);
#endif
        numRayPreparedSharedMem = atomic_add(GET_PTR_SAFE(activePathCountBuffer_0, 0), numRayToAdd);
    }
    barrier(CLK_LOCAL_MEM_FENCE);

    int compactedPathRayIndex = numRayPreparedSharedMem + local_idx;
#if DISALLOW_PATH_RAYS_COMPACTION
    compactedPathRayIndex = expandedPathRayIdx;
#else
    // Write the ray out to memory
    if (Ray_IsActive_Private(&r))
#endif
    {
        INDEX_SAFE(originalRaysExpandedBuffer, expandedPathRayIdx) = (float4)(r.d.x, r.d.y, r.d.z, 0);
        Ray_SetSampleDescriptionIndex(&r, expandedPathRayIdx);
        INDEX_SAFE(pathRaysCompactedBuffer_0, compactedPathRayIndex) = r;
    }
}

__kernel void preparePathRaysFromBounce(
    //*** input ***
    INPUT_BUFFER( 00, ray,                 pathRaysCompactedBuffer_0),
    INPUT_BUFFER( 01, Intersection,        pathIntersectionsCompactedBuffer),
    INPUT_BUFFER( 02, uint,                activePathCountBuffer_0),
    INPUT_BUFFER( 03, PackedNormalOctQuad, pathLastPlaneNormalCompactedBuffer),
    INPUT_BUFFER( 04, unsigned char,       pathLastNormalFacingTheRayCompactedBuffer),
    INPUT_BUFFER( 05, SampleDescription,   sampleDescriptionsExpandedBuffer),
    INPUT_VALUE(  06, int,                 lightmapSize),
    INPUT_VALUE(  07, int,                 bounce),
    INPUT_BUFFER( 08, uint,                sobol_buffer),
    INPUT_BUFFER( 09, float,               goldenSample_buffer),
    INPUT_VALUE(  10, float,               pushOff),
    INPUT_VALUE(  11, int,                 superSamplingMultiplier),
    INPUT_VALUE(  12, int,                 russianRouletteStartBounce),
    //*** output ***
    OUTPUT_BUFFER(13, ray,                 pathRaysCompactedBuffer_1),
    OUTPUT_BUFFER(14, uint,                totalRaysCastCountBuffer),
    OUTPUT_BUFFER(15, uint,                activePathCountBuffer_1),
    //*** in/output ***
    OUTPUT_BUFFER(16, float4,              pathThroughputExpandedBuffer)
    KERNEL_VALIDATOR_BUFFERS_DEF
)
{
    __local uint numRayPreparedSharedMem;
    if (get_local_id(0) == 0)
        numRayPreparedSharedMem = 0;
    barrier(CLK_LOCAL_MEM_FENCE);

    ray r;// Prepare ray in private memory
    Ray_SetInactive(&r);

    uint compactedPathRayIdx = get_global_id(0), local_idx;
    bool shouldPrepareNewRay = compactedPathRayIdx < INDEX_SAFE(activePathCountBuffer_0, 0);

#if DISALLOW_PATH_RAYS_COMPACTION
    if (Ray_IsInactive(GET_PTR_SAFE(pathRaysCompactedBuffer_0, compactedPathRayIdx)))
    {
        shouldPrepareNewRay = false;
    }
#endif

    SampleDescription sampleDescription;
    int expandedPathRayIdx;
    if (shouldPrepareNewRay)
    {
        KERNEL_ASSERT(Ray_IsActive(GET_PTR_SAFE(pathRaysCompactedBuffer_0, compactedPathRayIdx)));

        expandedPathRayIdx = Ray_GetSampleDescriptionIndex(GET_PTR_SAFE(pathRaysCompactedBuffer_0, compactedPathRayIdx));
        sampleDescription = INDEX_SAFE(sampleDescriptionsExpandedBuffer, expandedPathRayIdx);

        // We did not hit anything, no bounce path ray.
        const bool pathRayHitSomething = INDEX_SAFE(pathIntersectionsCompactedBuffer, compactedPathRayIdx).shapeid > 0;
        shouldPrepareNewRay &= pathRayHitSomething;

        // We hit an invalid triangle (from the back, no double sided GI), stop the path.
        const unsigned char isNormalFacingTheRay = INDEX_SAFE(pathLastNormalFacingTheRayCompactedBuffer, compactedPathRayIdx);
        shouldPrepareNewRay = (shouldPrepareNewRay && isNormalFacingTheRay);
    }

    // Russian roulette step can terminate the path
    float3 sample3D;
    if (shouldPrepareNewRay)
    {
        // Get random numbers
        int baseDimension = bounce * PLM_MAX_NUM_SOBOL_DIMENSIONS_PER_BOUNCE;

        sample3D.x = SobolSample(sampleDescription.currentSampleCount, baseDimension + 0, sobol_buffer KERNEL_VALIDATOR_BUFFERS);
        sample3D.y = SobolSample(sampleDescription.currentSampleCount, baseDimension + 1, sobol_buffer KERNEL_VALIDATOR_BUFFERS);
        sample3D.z = SobolSample(sampleDescription.currentSampleCount, baseDimension + 2, sobol_buffer KERNEL_VALIDATOR_BUFFERS);

        int texel_x = sampleDescription.texelIndex % lightmapSize;
        int texel_y = sampleDescription.texelIndex / lightmapSize;
        sample3D = ApplyCranleyPattersonRotation3D(sample3D, texel_x, texel_y, lightmapSize, baseDimension, goldenSample_buffer KERNEL_VALIDATOR_BUFFERS);

        //russianRouletteStartBounce<0 means deactivate russian roulette
        const bool doRussianRoulette = (russianRouletteStartBounce >= 0) && ( (bounce+1) >= russianRouletteStartBounce) && shouldPrepareNewRay;
        if (doRussianRoulette)
        {
            const float4 pathThroughput = INDEX_SAFE(pathThroughputExpandedBuffer, expandedPathRayIdx);
            float p = max(max(pathThroughput.x, pathThroughput.y), pathThroughput.z);
            if (p < sample3D.z)
                shouldPrepareNewRay = false;
            else
                INDEX_SAFE(pathThroughputExpandedBuffer, expandedPathRayIdx).xyz *= (1 / p);
        }
    }

    if (shouldPrepareNewRay)
    {
        const float3 planeNormal = DecodeNormal(INDEX_SAFE(pathLastPlaneNormalCompactedBuffer, compactedPathRayIdx));
        const float t = INDEX_SAFE(pathIntersectionsCompactedBuffer, compactedPathRayIdx).uvwt.w;
        const float3 position = INDEX_SAFE(pathRaysCompactedBuffer_0, compactedPathRayIdx).o.xyz + INDEX_SAFE(pathRaysCompactedBuffer_0, compactedPathRayIdx).d.xyz * t;

        // Map to cosine weighted hemisphere directed toward plane normal
        float3 b1;
        float3 b2;
        CreateOrthoNormalBasis(planeNormal, &b1, &b2);
        float3 hamDir = HemisphereCosineSample(sample3D.xy);
        float3 D = hamDir.x*b1 + hamDir.y*b2 + hamDir.z*planeNormal;

        // TODO(RadeonRays) gboisse: we're generating some NaN directions somehow, fix it!!
        if (!any(isnan(D)))
        {
            const float3 P = position.xyz + planeNormal * pushOff;

            // propagate potentially fixed up lod param from the intersection
            const int instanceLodInfo = INDEX_SAFE(pathIntersectionsCompactedBuffer, compactedPathRayIdx).padding0;

            Ray_Init(&r, P, D, DEFAULT_RAY_LENGTH, 0.f, instanceLodInfo);
            Ray_SetSourceIndex(&r, sampleDescription.texelIndex);
            Ray_SetSampleDescriptionIndex(&r, expandedPathRayIdx);
        }
    }

    // Threads synchronization for compaction
    if (Ray_IsActive_Private(&r))
    {
        local_idx = atomic_inc(&numRayPreparedSharedMem);
    }
    barrier(CLK_LOCAL_MEM_FENCE);
    if (get_local_id(0) == 0)
    {
        atomic_add(GET_PTR_SAFE(totalRaysCastCountBuffer, 0), numRayPreparedSharedMem);
        int numRayToAdd = numRayPreparedSharedMem;
#if DISALLOW_PATH_RAYS_COMPACTION
        numRayToAdd = get_local_size(0);
#endif
        numRayPreparedSharedMem = atomic_add(GET_PTR_SAFE(activePathCountBuffer_1, 0), numRayToAdd);
    }
    barrier(CLK_LOCAL_MEM_FENCE);

    int compactedBouncedPathRayIndex = numRayPreparedSharedMem + local_idx;
#if DISALLOW_PATH_RAYS_COMPACTION
    compactedBouncedPathRayIndex = compactedPathRayIdx;
#else
    // Write the ray out to memory
    if (Ray_IsActive_Private(&r))
#endif
    {
        INDEX_SAFE(pathRaysCompactedBuffer_1, compactedBouncedPathRayIndex) = r;
    }
}
