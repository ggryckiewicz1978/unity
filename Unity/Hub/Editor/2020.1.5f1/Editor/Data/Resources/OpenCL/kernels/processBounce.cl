#include "commonCL.h"
#include "colorSpace.h"
#include "directLighting.h"
#include "emissiveLighting.h"

__constant sampler_t linear2DSampler = CLK_NORMALIZED_COORDS_TRUE | CLK_ADDRESS_CLAMP_TO_EDGE | CLK_FILTER_LINEAR;

static void AccumulateLightFromBounce(float3 albedo, float3 directLightingAtHit, int expandedRayIdx, __global float3* lightingExpandedBuffer, int lightmapMode,
    __global float4* directionalExpandedBuffer, float3 direction KERNEL_VALIDATOR_BUFFERS_DEF)
{
    //Purely diffuse surface reflect the unabsorbed light evenly on the hemisphere.
    float3 energyFromHit = albedo * directLightingAtHit;
    INDEX_SAFE(lightingExpandedBuffer, expandedRayIdx).xyz += energyFromHit;

    //compute directionality from indirect
    if (lightmapMode == LIGHTMAPMODE_DIRECTIONAL)
    {
        float lum = Luminance(energyFromHit);

        INDEX_SAFE(directionalExpandedBuffer, expandedRayIdx).xyz += direction * lum;
        INDEX_SAFE(directionalExpandedBuffer, expandedRayIdx).w += lum;
    }
}

__kernel void processLightRaysFromBounce(
    INPUT_BUFFER( 00, LightBuffer,  indirectLightsBuffer),
    INPUT_BUFFER( 01, LightSample,  lightSamplesCompactedBuffer),
    OUTPUT_BUFFER(02, uint,         usePowerSamplingBuffer),
    INPUT_BUFFER( 03, float,        angularFalloffLUT_buffer),
    INPUT_BUFFER( 04, float,        distanceFalloffs_buffer),
    INPUT_BUFFER( 05, int,          cookiesBuffer),
    INPUT_BUFFER( 06, ray,          pathRaysCompactedBuffer_0),
    INPUT_BUFFER( 07, Intersection, pathIntersectionsCompactedBuffer),
    INPUT_BUFFER( 08, ray,          lightRaysCompactedBuffer),
    INPUT_BUFFER( 09, float4,       lightOcclusionCompactedBuffer),
    INPUT_BUFFER( 10, float4,       pathThroughputExpandedBuffer),
    INPUT_BUFFER( 11, uint,         lightRayIndexToPathRayIndexCompactedBuffer),
    INPUT_BUFFER( 12, uint,         lightRaysCountBuffer),
    INPUT_BUFFER( 13, float4,       originalRaysExpandedBuffer),
    INPUT_VALUE(  14, int,          updatePowerSamplingBuffer),
#ifdef PROBES
    INPUT_VALUE(  15, int,          totalSampleCount),
    OUTPUT_BUFFER(16, float4,       probeSHExpandedBuffer)
#else
    INPUT_VALUE(  15, int,          lightmapMode),
    OUTPUT_BUFFER(16, float3,       lightingExpandedBuffer),
    OUTPUT_BUFFER(17, float4,       directionalExpandedBuffer)
#endif
    KERNEL_VALIDATOR_BUFFERS_DEF
    )
{
    uint compactedLightRayIdx = get_global_id(0);
    __local int numLightHitCountSharedMem;
    __local int numLightRayCountSharedMem;

    if(updatePowerSamplingBuffer)
    {
        if (get_local_id(0) == 0)
        {
            numLightHitCountSharedMem = 0;
            numLightRayCountSharedMem = 0;
        }
        barrier(CLK_LOCAL_MEM_FENCE);
    }

    bool shouldProcessRay = true;
#if DISALLOW_LIGHT_RAYS_COMPACTION
    if (Ray_IsInactive(GET_PTR_SAFE(lightRaysCompactedBuffer, compactedLightRayIdx)))
    {
        shouldProcessRay = false;
    }
#endif

    if (shouldProcessRay && compactedLightRayIdx < INDEX_SAFE(lightRaysCountBuffer, 0))
    {
        KERNEL_ASSERT(Ray_IsActive(GET_PTR_SAFE(lightRaysCompactedBuffer, compactedLightRayIdx)));
        const int compactedPathRayIdx = INDEX_SAFE(lightRayIndexToPathRayIndexCompactedBuffer, compactedLightRayIdx);
        KERNEL_ASSERT(Ray_IsActive(GET_PTR_SAFE(pathRaysCompactedBuffer_0, compactedPathRayIdx)));
        const bool pathRayHitSomething = INDEX_SAFE(pathIntersectionsCompactedBuffer, compactedPathRayIdx).shapeid > 0;
        KERNEL_ASSERT(pathRayHitSomething);

        const int texelOrProbeIdx = Ray_GetSourceIndex(GET_PTR_SAFE(lightRaysCompactedBuffer, compactedLightRayIdx));
        const int expandedRayIdx = Ray_GetSampleDescriptionIndex(GET_PTR_SAFE(lightRaysCompactedBuffer, compactedLightRayIdx));
        LightSample lightSample = INDEX_SAFE(lightSamplesCompactedBuffer, compactedLightRayIdx);
        LightBuffer light = INDEX_SAFE(indirectLightsBuffer, lightSample.lightIdx);

        bool useShadows = light.castShadow;
        const float4 occlusions4 = useShadows ? INDEX_SAFE(lightOcclusionCompactedBuffer, compactedLightRayIdx) : make_float4(1.0f, 1.0f, 1.0f, 1.0f);
        const bool  isLightOccludedFromBounce = occlusions4.w < TRANSMISSION_THRESHOLD;

        if (!isLightOccludedFromBounce)
        {
            const float t = INDEX_SAFE(pathIntersectionsCompactedBuffer, compactedPathRayIdx).uvwt.w;
            //We need to compute direct lighting on the fly
            float3 surfacePosition = INDEX_SAFE(pathRaysCompactedBuffer_0, compactedPathRayIdx).o.xyz + INDEX_SAFE(pathRaysCompactedBuffer_0, compactedPathRayIdx).d.xyz * t;
            float3 albedoAttenuation = INDEX_SAFE(pathThroughputExpandedBuffer, expandedRayIdx).xyz;

            float3 directLightingAtHit = occlusions4.xyz * ShadeLight(light, INDEX_SAFE(lightRaysCompactedBuffer, compactedLightRayIdx), surfacePosition, angularFalloffLUT_buffer, distanceFalloffs_buffer, cookiesBuffer KERNEL_VALIDATOR_BUFFERS) / lightSample.lightPdf;

            // The original direction from which the rays was shot from the probe position
            float4 originalRayDirection = INDEX_SAFE(originalRaysExpandedBuffer, expandedRayIdx);
#ifdef PROBES
            float3 L = albedoAttenuation * directLightingAtHit;
            float weight = 4.0 / totalSampleCount;
            accumulateSHExpanded(L, originalRayDirection, weight, probeSHExpandedBuffer, expandedRayIdx KERNEL_VALIDATOR_BUFFERS);
#else
            AccumulateLightFromBounce(albedoAttenuation, directLightingAtHit, expandedRayIdx, lightingExpandedBuffer, lightmapMode, directionalExpandedBuffer, originalRayDirection.xyz KERNEL_VALIDATOR_BUFFERS);
#endif
            if (updatePowerSamplingBuffer)
            {
                atomic_inc(&numLightHitCountSharedMem);
            }
        }
        if (updatePowerSamplingBuffer)
        {
            atomic_inc(&numLightRayCountSharedMem);
        }
    }

    if (updatePowerSamplingBuffer)
    {
        // Collect stats to disable power sampling in pathological case.
        barrier(CLK_LOCAL_MEM_FENCE);
        if (get_local_id(0) == 0)
        {
            atomic_add(GET_PTR_SAFE(usePowerSamplingBuffer, UsePowerSamplingBufferSlot_LightHitCount), numLightHitCountSharedMem);
            atomic_add(GET_PTR_SAFE(usePowerSamplingBuffer, UsePowerSamplingBufferSlot_LightRayCount), numLightRayCountSharedMem);
        }
    }
}

__kernel void updatePowerSamplingBuffer(
    /*00*/ int                              resetPowerSamplingBuffer,
    /*01*/__global uint*                    usePowerSamplingBuffer
    KERNEL_VALIDATOR_BUFFERS_DEF
)
{
    if (resetPowerSamplingBuffer)
    {
        //Reset counter and re-enable power sampling
        INDEX_SAFE(usePowerSamplingBuffer, UsePowerSamplingBufferSlot_LightHitCount) = 0;
        INDEX_SAFE(usePowerSamplingBuffer, UsePowerSamplingBufferSlot_LightRayCount) = 0;
        INDEX_SAFE(usePowerSamplingBuffer, UsePowerSamplingBufferSlot_PowerSampleEnabled) = 0xFFFFFFFF;
        return;
    }

    const float kPowerSamplingMinimumRatio = 0.2f;
    const int   kPowerSamplingMinimumRaysCountBeforeDisabling = 100;

    uint totalLightHitCount = INDEX_SAFE(usePowerSamplingBuffer, UsePowerSamplingBufferSlot_LightHitCount);
    uint totalLightRayCount = INDEX_SAFE(usePowerSamplingBuffer, UsePowerSamplingBufferSlot_LightRayCount);
    if (totalLightRayCount > kPowerSamplingMinimumRaysCountBeforeDisabling)
    {
        float ratio = (float)totalLightHitCount / (float)totalLightRayCount;
        if (ratio < kPowerSamplingMinimumRatio)
        {
            //Disable power sampling
            INDEX_SAFE(usePowerSamplingBuffer, UsePowerSamplingBufferSlot_PowerSampleEnabled) = 0;
        }
    }
}

__kernel void processEmissiveAndAOFromBounce(
    INPUT_BUFFER( 00, ray,                       pathRaysCompactedBuffer_0),
    INPUT_BUFFER( 01, Intersection,              pathIntersectionsCompactedBuffer),
    INPUT_BUFFER( 02, MaterialTextureProperties, instanceIdToEmissiveTextureProperties),
    INPUT_BUFFER( 03, float2,                    geometryUV1sBuffer),
    INPUT_BUFFER( 04, float4,                    dynarg_texture_buffer),
    INPUT_BUFFER( 05, MeshDataOffsets,           instanceIdToMeshDataOffsets),
    INPUT_BUFFER( 06, uint,                      geometryIndicesBuffer),
    INPUT_BUFFER( 07, float4,                    pathThroughputExpandedBuffer),
    INPUT_BUFFER( 08, uint,                      activePathCountBuffer_0),
    INPUT_BUFFER( 09, unsigned char,             pathLastNormalFacingTheRayCompactedBuffer),
    INPUT_BUFFER( 10, float4,                    originalRaysExpandedBuffer),
#ifdef PROBES
    INPUT_VALUE(  11, int,                       totalSampleCount),
    OUTPUT_BUFFER(12, float4,                    probeSHExpandedBuffer)
#else
    INPUT_VALUE(  11, int,                       lightmapMode),
    INPUT_VALUE(  12, float,                     aoMaxDistance),
    INPUT_VALUE(  13, int,                       bounce),
    OUTPUT_BUFFER(14, float3,                    lightingExpandedBuffer),
    OUTPUT_BUFFER(15, float4,                    directionalExpandedBuffer),
    OUTPUT_BUFFER(16, float4,                    shadowmaskExpandedBuffer) //when gathering indirect .x will contain AO and .y will contain Validity
#endif
    KERNEL_VALIDATOR_BUFFERS_DEF
)
{
    uint compactedPathRayIdx = get_global_id(0);

    if (compactedPathRayIdx >= INDEX_SAFE(activePathCountBuffer_0, 0))
        return;

#if DISALLOW_PATH_RAYS_COMPACTION
    if (Ray_IsInactive(GET_PTR_SAFE(pathRaysCompactedBuffer_0, compactedPathRayIdx)))
        return;
#endif

    KERNEL_ASSERT(Ray_IsActive(GET_PTR_SAFE(pathRaysCompactedBuffer_0, compactedPathRayIdx)));
    const int texelOrProbeIdx = Ray_GetSourceIndex(GET_PTR_SAFE(pathRaysCompactedBuffer_0, compactedPathRayIdx));
    const int expandedPathRayIdx = Ray_GetSampleDescriptionIndex(GET_PTR_SAFE(pathRaysCompactedBuffer_0, compactedPathRayIdx));
    const bool  hit = INDEX_SAFE(pathIntersectionsCompactedBuffer, compactedPathRayIdx).shapeid > 0;

#ifndef PROBES
    const bool shouldAddOneToAOCount = (bounce == 0 && (!hit || INDEX_SAFE(pathIntersectionsCompactedBuffer, compactedPathRayIdx).uvwt.w > aoMaxDistance));
    if (shouldAddOneToAOCount)
    {
        INDEX_SAFE(shadowmaskExpandedBuffer, expandedPathRayIdx).x += 1.0f;
    }
#endif

    if (hit)
    {
        AtlasInfo emissiveContribution = FetchEmissionFromRayIntersection(compactedPathRayIdx,
            pathIntersectionsCompactedBuffer,
            instanceIdToEmissiveTextureProperties,
            instanceIdToMeshDataOffsets,
            geometryUV1sBuffer,
            geometryIndicesBuffer,
            dynarg_texture_buffer
            KERNEL_VALIDATOR_BUFFERS
        );

        // If hit an invalid triangle (from the back, no double sided GI) we do not apply emissive.
        const unsigned char isNormalFacingTheRay = INDEX_SAFE(pathLastNormalFacingTheRayCompactedBuffer, compactedPathRayIdx);

        // The original direction from which the rays was shot
        float4 originalRayDirection = INDEX_SAFE(originalRaysExpandedBuffer, expandedPathRayIdx);

#ifdef PROBES
        float3 L = emissiveContribution.color.xyz * INDEX_SAFE(pathThroughputExpandedBuffer, expandedPathRayIdx).xyz;
        float weight = 4.0 / totalSampleCount;
        accumulateSHExpanded(L, originalRayDirection, weight, probeSHExpandedBuffer, expandedPathRayIdx KERNEL_VALIDATOR_BUFFERS);
#else
        float3 output = isNormalFacingTheRay * emissiveContribution.color.xyz * INDEX_SAFE(pathThroughputExpandedBuffer, expandedPathRayIdx).xyz;

        // Compute directionality from indirect
        if (lightmapMode == LIGHTMAPMODE_DIRECTIONAL)
        {
            float lum = Luminance(output);
            float4 directionality;
            directionality.xyz = originalRayDirection.xyz * lum;
            directionality.w = lum;
            INDEX_SAFE(directionalExpandedBuffer, expandedPathRayIdx) += directionality;
        }


        // Write Result
        INDEX_SAFE(lightingExpandedBuffer, expandedPathRayIdx).xyz += output.xyz;
#endif
    }
}

__kernel void advanceInPathAndAdjustPathProperties(
    INPUT_BUFFER( 00, ray,                       pathRaysCompactedBuffer_0),
    INPUT_BUFFER( 01, Intersection,              pathIntersectionsCompactedBuffer),
    INPUT_BUFFER( 02, MaterialTextureProperties, instanceIdToAlbedoTextureProperties),
    INPUT_BUFFER( 03, MeshDataOffsets,           instanceIdToMeshDataOffsets),
    INPUT_BUFFER( 04, float2,                    geometryUV1sBuffer),
    INPUT_BUFFER( 05, uint,                      geometryIndicesBuffer),
    INPUT_BUFFER( 06, uchar4,                    albedoTextures_buffer),
    INPUT_BUFFER( 07, uint,                      activePathCountBuffer_0),
    OUTPUT_BUFFER(08, float4,                    pathThroughputExpandedBuffer) //in & output
    KERNEL_VALIDATOR_BUFFERS_DEF
)
{
    uint compactedPathRayIdx = get_global_id(0);

    if (compactedPathRayIdx >= INDEX_SAFE(activePathCountBuffer_0, 0))
        return;

#if DISALLOW_PATH_RAYS_COMPACTION
    if (Ray_IsInactive(GET_PTR_SAFE(pathRaysCompactedBuffer_0, compactedPathRayIdx)))
        return;
#endif

    KERNEL_ASSERT(Ray_IsActive(GET_PTR_SAFE(pathRaysCompactedBuffer_0, compactedPathRayIdx)));
    const int expandedPathRayIdx = Ray_GetSampleDescriptionIndex(GET_PTR_SAFE(pathRaysCompactedBuffer_0, compactedPathRayIdx));
    const bool  hit = INDEX_SAFE(pathIntersectionsCompactedBuffer, compactedPathRayIdx).shapeid > 0;
    if (!hit)
        return;

    AtlasInfo albedoAtHit = FetchAlbedoFromRayIntersection(compactedPathRayIdx,
        pathIntersectionsCompactedBuffer,
        instanceIdToAlbedoTextureProperties,
        instanceIdToMeshDataOffsets,
        geometryUV1sBuffer,
        geometryIndicesBuffer,
        albedoTextures_buffer
        KERNEL_VALIDATOR_BUFFERS);

    const float throughputAttenuation = dot(albedoAtHit.color.xyz, kAverageFactors);
    INDEX_SAFE(pathThroughputExpandedBuffer, expandedPathRayIdx) *= (float4)(albedoAtHit.color.x, albedoAtHit.color.y, albedoAtHit.color.z, throughputAttenuation);
}

__kernel void getNormalsFromLastBounceAndDoValidity(
    INPUT_BUFFER( 00, ray,                       pathRaysCompactedBuffer_0),              // rays from last to current hit
    INPUT_BUFFER( 01, Intersection,              pathIntersectionsCompactedBuffer),       // intersections from last to current hit
    INPUT_BUFFER( 02, MeshDataOffsets,           instanceIdToMeshDataOffsets),
    INPUT_BUFFER( 03, Matrix4x4,                 instanceIdToInvTransposedMatrices),
    INPUT_BUFFER( 04, Vector3f_storage,          geometryPositionsBuffer),
    INPUT_BUFFER( 05, PackedNormalOctQuad,       geometryNormalsBuffer),
    INPUT_BUFFER( 06, uint,                      geometryIndicesBuffer),
    INPUT_BUFFER( 07, uint,                      activePathCountBuffer_0),
    INPUT_BUFFER( 08, MaterialTextureProperties, instanceIdToTransmissionTextureProperties),
    INPUT_VALUE(  09, int,                       primaryBufferMode),
    //output
    OUTPUT_BUFFER(10, PackedNormalOctQuad,       pathLastPlaneNormalCompactedBuffer),
    OUTPUT_BUFFER(11, PackedNormalOctQuad,       pathLastInterpNormalCompactedBuffer),
    OUTPUT_BUFFER(12, unsigned char,             pathLastNormalFacingTheRayCompactedBuffer),
    OUTPUT_BUFFER(13, float4,                    shadowmaskExpandedBuffer), // Used to store validity in .y
    OUTPUT_BUFFER(14, float,                     probeDepthOctahedronExpandedBuffer)
    KERNEL_VALIDATOR_BUFFERS_DEF
)
{
    uint compactedPathRayIdx = get_global_id(0);

    if (compactedPathRayIdx >= INDEX_SAFE(activePathCountBuffer_0, 0))
        return;

#if DISALLOW_PATH_RAYS_COMPACTION
    if (Ray_IsInactive(GET_PTR_SAFE(pathRaysCompactedBuffer_0, compactedPathRayIdx)))
        return;
#endif

    KERNEL_ASSERT(Ray_IsActive(GET_PTR_SAFE(pathRaysCompactedBuffer_0, compactedPathRayIdx)));
    if (INDEX_SAFE(pathIntersectionsCompactedBuffer, compactedPathRayIdx).shapeid <= 0)
    {
        PackedNormalOctQuad zero;
        zero.x = 0xffffffff; // Will yield a decoded value of float3(0, 0, -1)
        INDEX_SAFE(pathLastPlaneNormalCompactedBuffer, compactedPathRayIdx) = zero;
        INDEX_SAFE(pathLastInterpNormalCompactedBuffer, compactedPathRayIdx) = zero;
        INDEX_SAFE(pathLastNormalFacingTheRayCompactedBuffer, compactedPathRayIdx) = 0;
        return;
    }

    const int instanceId = GetInstanceIdFromIntersection(GET_PTR_SAFE(pathIntersectionsCompactedBuffer, compactedPathRayIdx));
    float3 planeNormalWS;
    float3 interpVertexNormalWS;
    GetNormalsAtRayIntersection(compactedPathRayIdx,
        instanceId,
        pathIntersectionsCompactedBuffer,
        instanceIdToMeshDataOffsets,
        instanceIdToInvTransposedMatrices,
        geometryPositionsBuffer,
        geometryNormalsBuffer,
        geometryIndicesBuffer,
        &planeNormalWS,
        &interpVertexNormalWS
        KERNEL_VALIDATOR_BUFFERS);

    unsigned char isNormalFacingTheRay = 1;
    float3 rayDirection = INDEX_SAFE(pathRaysCompactedBuffer_0, compactedPathRayIdx).d.xyz;
    const bool frontFacing = dot(planeNormalWS, rayDirection) <= 0.0f;
    const int expandedPathRayIdx = Ray_GetSampleDescriptionIndex(GET_PTR_SAFE(pathRaysCompactedBuffer_0, compactedPathRayIdx));

    bool isRayValid = true;

    if (!frontFacing)
    {
        const MaterialTextureProperties matProperty = INDEX_SAFE(instanceIdToTransmissionTextureProperties, instanceId);
        const bool isDoubleSidedGI = GetMaterialProperty(matProperty, kMaterialInstanceProperties_DoubleSidedGI);
        planeNormalWS =        isDoubleSidedGI ? -planeNormalWS        : planeNormalWS;
        interpVertexNormalWS = isDoubleSidedGI ? -interpVertexNormalWS : interpVertexNormalWS;
        isNormalFacingTheRay = isDoubleSidedGI? 1 : 0;
        if (primaryBufferMode == PrimaryBufferMode_Generate && !isDoubleSidedGI)
        {
            const bool isTransparent = GetMaterialProperty(matProperty, kMaterialInstanceProperties_UseTransmission);
            if (!isTransparent)
            {
                //We use the shadowmaskExpandedBuffer.y to store validity to avoid having an additional expanded buffer.
                INDEX_SAFE(shadowmaskExpandedBuffer, expandedPathRayIdx).y = 1.0f;
                isRayValid = false;
            }
        }
    }

#ifdef PROBES
    if (primaryBufferMode == PrimaryBufferMode_Generate && isRayValid)
    {
        float t = INDEX_SAFE(pathIntersectionsCompactedBuffer, compactedPathRayIdx).uvwt.w;

        float2 normalizedOctCoord = PackNormalOctQuadEncoded(normalize(rayDirection));
        int texel = GetOctQuadEncodedTexelFromPackedNormal(normalizedOctCoord, OCTAHEDRON_SIZE);
        float3 texelDirection = UnpackNormalOctQuadEncoded(normalizedOctCoord);

        float weight = max(0.0f, dot(texelDirection, rayDirection));

        INDEX_SAFE(probeDepthOctahedronExpandedBuffer, expandedPathRayIdx * OCTAHEDRON_TEXEL_COUNT + texel) += t * weight;
    }
#endif

    // Store normals for various kernels to use later
    INDEX_SAFE(pathLastPlaneNormalCompactedBuffer, compactedPathRayIdx) = EncodeNormalToUint(planeNormalWS);
    INDEX_SAFE(pathLastInterpNormalCompactedBuffer, compactedPathRayIdx) = EncodeNormalToUint(interpVertexNormalWS);
    INDEX_SAFE(pathLastNormalFacingTheRayCompactedBuffer, compactedPathRayIdx) = isNormalFacingTheRay;
}
