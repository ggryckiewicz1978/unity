#include "environmentLighting.h"


__kernel void processDirectEnvironment(
    //*** input ***
    INPUT_BUFFER( 00, ray,                 lightRaysCompactedBuffer),
    INPUT_BUFFER( 01, uint,                lightRaysCountBuffer),
    INPUT_BUFFER( 02, float4,              lightOcclusionCompactedBuffer),
    INPUT_BUFFER( 03, float4,              env_mipped_cube_texels_buffer),
    INPUT_BUFFER( 04, int,                 env_mip_offsets_buffer),
    INPUT_VALUE(  05, Environment,         envData),
#ifdef PROBES
    INPUT_VALUE(  06, int,                 totalSampleCount),
    //*** output ***
    OUTPUT_BUFFER(07, float4,              probeSHExpandedBuffer)
#else
    INPUT_BUFFER( 06, PackedNormalOctQuad, interpNormalsWSBuffer),
    INPUT_VALUE(  07, int,                 lightmapMode),
    INPUT_VALUE(  08, int,                 superSamplingMultiplier),
    INPUT_VALUE(  09, int,                 lightmapSize),
    INPUT_BUFFER( 10, SampleDescription,   sampleDescriptionsExpandedBuffer),
    INPUT_BUFFER( 11, uint,                sobol_buffer),
    INPUT_BUFFER( 12, float,               goldenSample_buffer),
    //*** output ***
    OUTPUT_BUFFER(13, float4,              directionalExpandedBuffer),
    OUTPUT_BUFFER(14, float3,              lightingExpandedBuffer)

#endif
    KERNEL_VALIDATOR_BUFFERS_DEF)
{
    uint compactedRayIdx = get_global_id(0);
    if (compactedRayIdx >= INDEX_SAFE(lightRaysCountBuffer, 0))
        return;

#if DISALLOW_LIGHT_RAYS_COMPACTION
    if (Ray_IsInactive(GET_PTR_SAFE(lightRaysCompactedBuffer, compactedRayIdx)))
        return;
#endif

    KERNEL_ASSERT(Ray_IsActive(GET_PTR_SAFE(lightRaysCompactedBuffer, compactedRayIdx)));
    int sampleDescriptionIdx = Ray_GetSampleDescriptionIndex(GET_PTR_SAFE(lightRaysCompactedBuffer, compactedRayIdx));

    float4 occlusion = INDEX_SAFE(lightOcclusionCompactedBuffer, compactedRayIdx);
    bool   occluded  = occlusion.w < TRANSMISSION_THRESHOLD;

    if (!occluded)
    {
        // Environment intersection
        float4 dir          = INDEX_SAFE(lightRaysCompactedBuffer, compactedRayIdx).d;
        float3 color        = make_float3(0.0f, 0.0f, 0.0f);
#ifdef PROBES
        if (UseEnvironmentMIS(envData.flags))
            color = ProcessVolumeEnvironmentRayMIS(dir, envData.envDim, envData.numMips, envData.envmapIntegral, env_mipped_cube_texels_buffer, env_mip_offsets_buffer KERNEL_VALIDATOR_BUFFERS);
        else
            color = ProcessVolumeEnvironmentRay(dir, envData.envDim, envData.numMips, env_mipped_cube_texels_buffer, env_mip_offsets_buffer KERNEL_VALIDATOR_BUFFERS);

        // accumulate environment lighting
        color *= occlusion.xyz;
        KERNEL_ASSERT(totalSampleCount > 0);
        float weight = 1.0f / totalSampleCount;
        accumulateSHExpanded(color.xyz, dir, weight, probeSHExpandedBuffer, sampleDescriptionIdx KERNEL_VALIDATOR_BUFFERS);
#else
        const SampleDescription sampleDescription = INDEX_SAFE(sampleDescriptionsExpandedBuffer, sampleDescriptionIdx);
        const int ssIdx = GetRandomSuperSampledIndex(sampleDescription.texelIndex, lightmapSize, sampleDescription.currentSampleCount, superSamplingMultiplier, sobol_buffer, goldenSample_buffer KERNEL_VALIDATOR_BUFFERS);

        float3 interpNormal = DecodeNormal(INDEX_SAFE(interpNormalsWSBuffer, ssIdx));
        if (UseEnvironmentMIS(envData.flags))
            color = ProcessEnvironmentRayMIS(dir, interpNormal, envData.envDim, envData.numMips, envData.envmapIntegral, env_mipped_cube_texels_buffer, env_mip_offsets_buffer KERNEL_VALIDATOR_BUFFERS);
        else
            color = ProcessEnvironmentRay(dir, envData.envDim, envData.numMips, env_mipped_cube_texels_buffer, env_mip_offsets_buffer KERNEL_VALIDATOR_BUFFERS);

        // accumulate environment lighting
        INDEX_SAFE(lightingExpandedBuffer, sampleDescriptionIdx) += occlusion.xyz * color;

        //compute directionality from indirect
        if (lightmapMode == LIGHTMAPMODE_DIRECTIONAL)
        {
            float  luminance   = Luminance(color);
            float3 scaledDir   = dir.xyz * luminance;
            float4 directional = make_float4(scaledDir.x, scaledDir.y, scaledDir.z, luminance);
            INDEX_SAFE(directionalExpandedBuffer, sampleDescriptionIdx) += directional;
        }
#endif
    }
}

__kernel void processIndirectEnvironment(
    INPUT_BUFFER(  0, ray,                 lightRaysCompactedBuffer),
    INPUT_BUFFER(  1, uint,                lightRaysCountBuffer),
    INPUT_BUFFER(  2, ray,                 pathRaysCompactedBuffer_0),//Only for kernel assert purpose.
    INPUT_BUFFER(  3, uint,                activePathCountBuffer_0),//Only for kernel assert purpose.
    INPUT_BUFFER(  4, uint,                lightRayIndexToPathRayIndexCompactedBuffer),
    INPUT_BUFFER(  5, float4,              originalRaysExpandedBuffer),
    INPUT_BUFFER(  6, float4,              lightOcclusionCompactedBuffer),
    INPUT_BUFFER(  7, float4,              pathThroughputExpandedBuffer),
    INPUT_BUFFER(  8, PackedNormalOctQuad, pathLastInterpNormalCompactedBuffer),
    INPUT_BUFFER(  9, float4,              env_mipped_cube_texels_buffer),
    INPUT_BUFFER( 10, int,                 env_mip_offsets_buffer),
    INPUT_VALUE(  11, Environment,         envData),
#ifdef PROBES
    INPUT_VALUE(  12, int,                 totalSampleCount),
    OUTPUT_BUFFER(13, float4,              probeSHExpandedBuffer)
#else
    INPUT_VALUE(  12, int,                 lightmapMode),
    OUTPUT_BUFFER(13, float3,              lightingExpandedBuffer),
    OUTPUT_BUFFER(14, float4,              directionalExpandedBuffer)
#endif
    KERNEL_VALIDATOR_BUFFERS_DEF
)
{
    uint compactedLightRayIdx = get_global_id(0);
    bool shouldProcessRay = true;

#if DISALLOW_LIGHT_RAYS_COMPACTION
    if (Ray_IsInactive(GET_PTR_SAFE(lightRaysCompactedBuffer, compactedLightRayIdx)))
    {
        shouldProcessRay = false;
    }
#endif

    if (shouldProcessRay && compactedLightRayIdx < INDEX_SAFE(lightRaysCountBuffer, 0))
    {
        KERNEL_ASSERT(Ray_IsActive(GET_PTR_SAFE(lightRaysCompactedBuffer, compactedLightRayIdx)));
        const int compactedPathRayIdx = INDEX_SAFE(lightRayIndexToPathRayIndexCompactedBuffer, compactedLightRayIdx);
        KERNEL_ASSERT(compactedPathRayIdx < INDEX_SAFE(activePathCountBuffer_0, 0));
        KERNEL_ASSERT(Ray_IsActive(GET_PTR_SAFE(pathRaysCompactedBuffer_0, compactedPathRayIdx)));
        const int expandedRayIdx = Ray_GetSampleDescriptionIndex(GET_PTR_SAFE(lightRaysCompactedBuffer, compactedLightRayIdx));

        float4 occlusion = INDEX_SAFE(lightOcclusionCompactedBuffer, compactedLightRayIdx);
        bool   occluded = occlusion.w < TRANSMISSION_THRESHOLD;
        if (!occluded)
        {
            // Environment intersection
            float3 interpNormal = DecodeNormal(INDEX_SAFE(pathLastInterpNormalCompactedBuffer, compactedPathRayIdx));
            float4 dir = INDEX_SAFE(lightRaysCompactedBuffer, compactedLightRayIdx).d;
            float3 color;

            if (UseEnvironmentMIS(envData.flags))
                color = ProcessEnvironmentRayMIS(dir, interpNormal, envData.envDim, envData.numMips, envData.envmapIntegral, env_mipped_cube_texels_buffer, env_mip_offsets_buffer KERNEL_VALIDATOR_BUFFERS);
            else
                color = ProcessEnvironmentRay(dir, envData.envDim, envData.numMips, env_mipped_cube_texels_buffer, env_mip_offsets_buffer KERNEL_VALIDATOR_BUFFERS);

            color *= occlusion.xyz * INDEX_SAFE(pathThroughputExpandedBuffer, expandedRayIdx).xyz;
            float4 originalRayDirection = INDEX_SAFE(originalRaysExpandedBuffer, expandedRayIdx);
#ifdef PROBES
            float weight = 4.0f / totalSampleCount;
            accumulateSHExpanded(color.xyz, originalRayDirection, weight, probeSHExpandedBuffer, expandedRayIdx KERNEL_VALIDATOR_BUFFERS);
#else
            //compute directionality from indirect
            if (lightmapMode == LIGHTMAPMODE_DIRECTIONAL)
            {
                float luminance = Luminance(color);
                originalRayDirection.xyz *= luminance;
                originalRayDirection.w = luminance;
                INDEX_SAFE(directionalExpandedBuffer, expandedRayIdx) += originalRayDirection;
            }
            // accumulate environment lighting
            INDEX_SAFE(lightingExpandedBuffer, expandedRayIdx) += occlusion.xyz * color;
#endif
        }
    }
}
