#include "commonCL.h"
#include "directLighting.h"

__kernel void processLightRays(
    // Inputs
    INPUT_BUFFER( 00, ray,                 lightRaysCompactedBuffer),
    INPUT_BUFFER( 01, float4,              positionsWSBuffer),
    INPUT_BUFFER( 02, LightBuffer,         directLightsBuffer),
    INPUT_BUFFER( 03, LightSample,         lightSamplesCompactedBuffer),
    INPUT_BUFFER( 04, float4,              lightOcclusionCompactedBuffer),
    INPUT_BUFFER( 05, float,               angularFalloffLUT_buffer),
    INPUT_BUFFER( 06, float,               distanceFalloffs_buffer),
    INPUT_BUFFER( 07, int,                 cookiesBuffer),
    INPUT_BUFFER( 08, uint,                lightRaysCountBuffer),
#ifdef PROBES
    INPUT_VALUE(  09, int,                 numProbes),
    INPUT_VALUE(  10, int,                 totalSampleCount),
    INPUT_BUFFER( 11, float4,              inputLightIndicesBuffer),
    // Outputs
    OUTPUT_BUFFER(12, float4,              probeSHExpandedBuffer),
    OUTPUT_BUFFER(13, float4,              probeOcclusionExpandedBuffer)
#else
    INPUT_BUFFER( 09, SampleDescription,   sampleDescriptionsExpandedBuffer),
    INPUT_BUFFER( 10, PackedNormalOctQuad, planeNormalsWSBuffer),
    INPUT_VALUE(  11, float,               pushOff),
    INPUT_VALUE(  12, int,                 lightmapMode),
    INPUT_VALUE(  13, int,                 superSamplingMultiplier),
    INPUT_VALUE(  14, int,                 lightmapSize),
    INPUT_BUFFER( 15, unsigned char,       gbufferInstanceIdToReceiveShadowsBuffer),
    INPUT_BUFFER( 16, uint,              sobol_buffer),
    INPUT_BUFFER( 17, float,             goldenSample_buffer),
    // Outputs
    OUTPUT_BUFFER(18, float4,              shadowmaskExpandedBuffer),
    OUTPUT_BUFFER(19, float4,              directionalExpandedBuffer),
    OUTPUT_BUFFER(20, float3,              lightingExpandedBuffer)
#endif
    KERNEL_VALIDATOR_BUFFERS_DEF
)
{
    uint compactedRayIdx = get_global_id(0);
    if (compactedRayIdx >= INDEX_SAFE(lightRaysCountBuffer, 0))
        return;

#if DISALLOW_LIGHT_RAYS_COMPACTION
    if (Ray_IsInactive(GET_PTR_SAFE(lightRaysCompactedBuffer, compactedRayIdx)))
        return;
#endif

    KERNEL_ASSERT(Ray_IsActive(GET_PTR_SAFE(lightRaysCompactedBuffer, compactedRayIdx)));
    int sampleDescriptionIdx = Ray_GetSampleDescriptionIndex(GET_PTR_SAFE(lightRaysCompactedBuffer, compactedRayIdx));
    LightSample lightSample = INDEX_SAFE(lightSamplesCompactedBuffer, compactedRayIdx);
    LightBuffer light = INDEX_SAFE(directLightsBuffer, lightSample.lightIdx);

#ifndef PROBES
    const SampleDescription sampleDescription = INDEX_SAFE(sampleDescriptionsExpandedBuffer, sampleDescriptionIdx);
    const int texelIndex = sampleDescription.texelIndex;

    //From 0 To lightmap_width*lightmap_height * superSamplingMultiplier *superSamplingMultiplier
    const int ssIdx = GetRandomSuperSampledIndex(sampleDescription.texelIndex, lightmapSize, sampleDescription.currentSampleCount, superSamplingMultiplier, sobol_buffer, goldenSample_buffer KERNEL_VALIDATOR_BUFFERS);

    const float4 positionAndGbufferInstanceId = INDEX_SAFE(positionsWSBuffer, ssIdx);
    const int gBufferInstanceId = (int)(floor(positionAndGbufferInstanceId.w));
    const float3 P = positionAndGbufferInstanceId.xyz;
    const float3 planeNormal = DecodeNormal(INDEX_SAFE(planeNormalsWSBuffer, ssIdx));
    const float3 position = P + planeNormal * pushOff;
#else
    const int texelIndex = Ray_GetSourceIndex(GET_PTR_SAFE(lightRaysCompactedBuffer, compactedRayIdx));
    const int ssIdx = texelIndex;
    const float3 position = INDEX_SAFE(positionsWSBuffer, ssIdx).xyz;
#endif

    bool useShadows = light.castShadow;
#ifndef PROBES
    useShadows &= INDEX_SAFE(gbufferInstanceIdToReceiveShadowsBuffer, gBufferInstanceId);
#endif
    float4 occlusions4 = useShadows ? INDEX_SAFE(lightOcclusionCompactedBuffer, compactedRayIdx) : make_float4(1.0f, 1.0f, 1.0f, 1.0f);
    const bool hit = occlusions4.w < TRANSMISSION_THRESHOLD;
    if (!hit)
    {
#ifdef PROBES
        const float weight = 1.0 / totalSampleCount;
        if (light.directBakeMode >= kDirectBakeMode_Subtractive)
        {
            int lightIdx = light.probeOcclusionLightIndex;
            const float4 lightIndicesFloat = INDEX_SAFE(inputLightIndicesBuffer, texelIndex);
            int4 lightIndices = (int4)((int)(lightIndicesFloat.x), (int)(lightIndicesFloat.y), (int)(lightIndicesFloat.z), (int)(lightIndicesFloat.w));
            float4 channelSelector = (float4)((lightIndices.x == lightIdx) ? 1.0f : 0.0f, (lightIndices.y == lightIdx) ? 1.0f : 0.0f, (lightIndices.z == lightIdx) ? 1.0f : 0.0f, (lightIndices.w == lightIdx) ? 1.0f : 0.0f);
            INDEX_SAFE(probeOcclusionExpandedBuffer, sampleDescriptionIdx) += channelSelector * weight;
        }
        else if (light.directBakeMode != kDirectBakeMode_None)
        {
            float4 D = (float4)(INDEX_SAFE(lightRaysCompactedBuffer, compactedRayIdx).d.x, INDEX_SAFE(lightRaysCompactedBuffer, compactedRayIdx).d.y, INDEX_SAFE(lightRaysCompactedBuffer, compactedRayIdx).d.z, 0);
            float3 L = ShadeLight(light, INDEX_SAFE(lightRaysCompactedBuffer, compactedRayIdx), position, angularFalloffLUT_buffer, distanceFalloffs_buffer, cookiesBuffer KERNEL_VALIDATOR_BUFFERS);
            accumulateSHExpanded(L, D, weight, probeSHExpandedBuffer, sampleDescriptionIdx KERNEL_VALIDATOR_BUFFERS);
        }
#else
        if (light.directBakeMode >= kDirectBakeMode_OcclusionChannel0)
        {
            float4 channelSelector = (float4)(light.directBakeMode == kDirectBakeMode_OcclusionChannel0 ? 1.0f : 0.0f, light.directBakeMode == kDirectBakeMode_OcclusionChannel1 ? 1.0f : 0.0f, light.directBakeMode == kDirectBakeMode_OcclusionChannel2 ? 1.0f : 0.0f, light.directBakeMode == kDirectBakeMode_OcclusionChannel3 ? 1.0f : 0.0f);
            INDEX_SAFE(shadowmaskExpandedBuffer, sampleDescriptionIdx) += occlusions4.w * channelSelector;
        }
        else if (light.directBakeMode != kDirectBakeMode_None)
        {
            const float3 lighting = occlusions4.xyz * ShadeLight(light, INDEX_SAFE(lightRaysCompactedBuffer, compactedRayIdx), position, angularFalloffLUT_buffer, distanceFalloffs_buffer, cookiesBuffer KERNEL_VALIDATOR_BUFFERS);
            INDEX_SAFE(lightingExpandedBuffer, sampleDescriptionIdx).xyz += lighting;

            //compute directionality from direct lighting
            if (lightmapMode == LIGHTMAPMODE_DIRECTIONAL)
            {
                float lum = Luminance(lighting);
                float4 directionality;
                directionality.xyz = INDEX_SAFE(lightRaysCompactedBuffer, compactedRayIdx).d.xyz * lum;
                directionality.w = lum;
                INDEX_SAFE(directionalExpandedBuffer, sampleDescriptionIdx) += directionality;
            }
        }
#endif
    }
}
